// import chokidar from 'chokidar
const chokidar = require("chokidar");
const fs = require("fs");
const path = require("path");

const { readdirSync, renameSync } = require('fs');
const { join } = require('path');
const folderPath = "./client/sets/";


chokidar.watch(folderPath).on('all', (event, file) => {
  // console.log(event, file);
  var file_name = path.basename(file)

  const match_part = RegExp(/.part$/, 'g');
  const match_git = RegExp(/\.git(.*)/, 'g');
  const match_up = RegExp(/\/_up\//, 'g');
  
  if ( file.match(match_git) ) {

  } else if ( file.match(match_up) ) {
    
  } else if ( event == "addDir" ) {
    console.log("================================");
    console.log("================================");
    console.log("================================");
    console.log("================================");
    console.log("FOLDER == "+file);

  } else if ( file_name.match(match_part) ) {
    
    console.log("part")

  } else if ( event == "add" ) {

    const folder_path_only=file.split("/").slice(0, -1).join("/");
    console.log("fn -- add ---- " +file_name)

    const match_all = RegExp(/[`~!@#$%^&*()|\+\-=?;:'",<>\{\}\[\]\\\/\ ][^\.]/, 'gi');
    const repla_all = "_";
    if ( file_name.match(match_all) ) {
      const filePath = join(folder_path_only, file_name);
      const newFilePath = join(folder_path_only, file_name.replace(match_all, repla_all));
        renameSync(filePath, newFilePath);
        // console.log(file_name);
    }

    const match_extra_end = RegExp(/^([\W\w][^\.]{1,})\.(jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)_(.*)/, 'g');
    const repla_extra_end = "$1.$2";
    if (file_name.match(match_extra_end)) {
      const filePath = join(folder_path_only, file_name);
      const newFilePath = join(folder_path_only, file_name.replace(match_extra_end, repla_extra_end));
        renameSync(filePath, newFilePath);
        console.log("---- EXTRA")
    }

    accentsTidy = function(s){
        // var r=s.toLowerCase();
        r = s.replace(new RegExp("[àáâãäå]", 'g'),"a");
        r = r.replace(new RegExp("æ", 'g'),"ae");
        r = r.replace(new RegExp("ç", 'g'),"c");
        r = r.replace(new RegExp("[èéêë]", 'g'),"e");
        r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
        r = r.replace(new RegExp("ñ", 'g'),"n");                            
        r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
        r = r.replace(new RegExp("œ", 'g'),"oe");
        r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
        r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
        r = r.replace(new RegExp("[ÀÁÂÃÄÅ]", 'g'),"A");
        r = r.replace(new RegExp("[ÈÉÊË]", 'g'),"E");
        r = r.replace(new RegExp("[ÌÍÎÏ]", 'g'),"I");
        r = r.replace(new RegExp("[ÒÓÔÕÖ]", 'g'),"O");
        r = r.replace(new RegExp("[ÙÚÛÜ]", 'g'),"U");
        r = r.replace(new RegExp("[ÝŸ]", 'g'),"Y");
        return r;
    };

    let file_fixed=accentsTidy(file_name);
    const filePath = join(folder_path_only, file_name);
    const newFilePath = join(folder_path_only, file_fixed);
    renameSync(filePath, newFilePath);

    // https://regex101.com/r/9y2jid/1
    const match_num_end = RegExp(/^([\W\w][^\.]{1,})\.(jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)\.([0-9]{1,})$/, 'g');
    const repla_num_end = "$1__double_$3.$2";
    if ( file_name.match(match_num_end) ) {
      const filePath = join(folder_path_only, file_name);
      const newFilePath = join(folder_path_only, file_name.replace(match_num_end, repla_num_end));
      renameSync(filePath, newFilePath);
      console.log("---- name.ext.num -- DOUBLE")
      // const folder_path_only=file.split("/").slice(0, -1).join("/");
      // const filePath = join(folder_path_only, file_name);
      // fs.rmSync.filePath;
      // console.log("RMMMMM")
      // fs.rmSync(filePath, {
      //     force: true,
      // });    
    }

    const match_double_point = RegExp(/(.*)\.([^.]*)/, 'g');
    const match_check_double_point = RegExp(/\.([^.]*)\.(jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)/, 'g');
    const repla_name = "$1";
    // const repla_exte = "$2";

    if (file_name.match(match_check_double_point)) {
      const filePath = join(folder_path_only, file_name);
      const filename = file_name.replace(match_double_point, repla_name)
      const fileexte = file_name.split('.').pop()
      // const fileexte = file_name.replace(match_double_point, repla_exte)
      const file_nopoint = filename.replace(/\./g, '_');
      const file_ok = file_nopoint+"."+fileexte;
      const newFilePath = join(folder_path_only, file_ok);
        console.log("---- EXTRA POINT")
        console.log("file_ok ::::::::::: "+newFilePath)
        renameSync(filePath, newFilePath);
    }




  } else {
    console.log("event == "+event);
    console.log("file == "+file);
  }
});




// // const path="./client/sets/"
// const watcher = chokidar.watch('file, dir, or array', {
//   ignored: /(^|[\/\\])\../, // ignore dotfiles
//   persistent: true
// });

// const log = console.log.bind(console);
// // Add event listeners.
// watcher
//   .on('add', path => log(`File ${path} has been added`))
//   .on('change', path => log(`File ${path} has been changed`))
//   .on('unlink', path => log(`File ${path} has been removed`));
