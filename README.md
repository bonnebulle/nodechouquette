# NodeChouquette

Utilise [Socket.io](https://socket.io/) pour échanger entre plusieures fenêtres/pages de la Chat-Chouquette

_ /cc permet de voir (look) \
_ /ls de lister et envoyer des images à cc (list) \
_ /up d'uploader des images (upload) \
_ /text editor + gif creation



## DEMO (live)
[https://cc.bonnebulle.xyz/](https://cc.bonnebulle.xyz)

### SETS
Ajout des images dans (add images into) :   
EX/ ```/client/sets/01_Thematique/Sub_folders/```
[README.TXT](/client/README.md)   

### LS liste
![LS](https://bulle.vincent-bonnefille.fr/pj/Bulles/Bulle_9hoVlw_nodejs_chatchouquette/chat_chouquette_vincent_bonnefille__LS_Full-page__MD50.png)

### TEXTEDITOR (gif)
Creat texts -> img   
Then img -> Gifs   
[DEMO](https://cc.bonnebulle.xyz/text)   

### UP
Envoyer des images !  
[DEMO](https://cc.bonnebulle.xyz/up?set=09_Semis_artistes)   





# INSTALL 

NEED /[NodeJS](https://nodejs.org)/ installed... \
EXTRA dependencie : /[jp2a](https://github.com/cslarsen/jp2a)/ \
. . . \
THEN DO :  
`git clone https://gitlab.com/bonnebulle/nodechouquette.git`   
`npm update --save`   
`NODE_ENV=production node server.js`   
( <-- default, dévelopement mod : localIP  )   
. . .   
ADD Images in folders in to `./client/sets/`   
LOOK [README.TXT](/client/README.md)   
. . .   
`node server.js`   (to lanch) use "pm2" as well...   



## DWL IMGS
`cd /client/sets/00_Thematique/Exemple_sub_folder/`   
`cat _links.txt`   
```md
http://image.jpg
http://image_2.jpg
http://image_4.png
```
( same folder, do ) : \
```wget -i _links_1.txt```


## WATCHER.JS   
Cleanup des fichier durant l'upload/ajout/DWL   
( Rename/cleanup files added/droped into folders )   
: no spaces or strange caracters    









# Previous version (php)
( from PHP to Nodejs ) \
https://vincent-bonnefille.fr/chat-chouquette/



## Todo

_ Cleanup ! \
_ Big fix : search_folders pbl with sub ```/sets/parent/child/sub/``` \
_ ... some messy Regex, search "data-folder" from end ($) \
_ .. UP : allow dropdown Choose/Create folder name \
_ .. Home : dropdown select existing child-folders in sets => ?go=... \
~~_ Test w/out RANDOM at start~~ \
_ Test re.do SHOW_UPLOADS (yes : show \_up folder) \
_ change_img_interval => Change .css animation \
_ .. + Add func. to change img_chg interval \
~~_ add Rooms !~~
