// INFOS : https://liens.vincent-bonnefille.fr/?9hoVlw
// GIT : https://gitlab.com/bonnebulle/nodechouquette/
"use strict";
/// ------------------------
/// ------------------------
/// CLEANUP FILES ( use it carefully )
/// ------------------------
/// ------------------------
// var BATCH_rename_all="no" // <--- Do a mass rename files  ('' USE CAREFULLY '')                           //   FIX spaces in files, special characters --> getAllFiles_sets_auto
// var rename_all = "no"; // TODO verify use -- on _UP FILES ( double / triple check ) <--- MEMORY LEAK

// CURRENT CHOUQUETTE
let live_chouquette="12_Janniverssaire"

/// HOME exclude liste
let home_prod_domaines=["cc.bonnebulle.xyz", "ars.bonnebulle.xyz"] // IF prod domaine => will exclude [home_exclude_folders]
let home_exclude_folders=[".git","remotes","refs","logs","objects","papillonss", "00_defaut", "Z_tests", "00_errors"]  // excluded in home page liste
var memo_acceuil_dom='https://cc.bonnebulle.xyz'; // terminal wellcom message
/// ------------------------
/// ------------------------
/// CONNECTION SETTING -----
/// ------------------------
/// ------------------------
const nodeport=3036
const nodeport_worker=3039
var cors_limitation='*';
// var cors_limitation='ars.bonnebulle.xyz, cc.bonnebulle.xyz';

const end_scenario="demo_load_others" 
// reload /OR/ demo_load_others //TODO Random + mv cc.js

/// #################################
/// #################################
/// #################################

// /// SERVER CONFIG APP == Express
// import express from "express"; 
// import app from express(); 
// import http from "http"; ss
// import httpServer from http.Server(app);
// import { Server } from "socket.io"; 
// import io from new Server(httpServer, {
//   cors: {
//     origin: cors_limitation
//   }
// });


// /// DEPENCENCIES _ MODULES
// import shell from 'shelljs'
// import path from "path"
// import path_cc from "path"
// import fs from "fs";

// // Load a song and play it
// import BeatBeat from "beat-beat-js"

// const sound = new BeatBeat("/var/www/@Nodejs/nodechouquette/client/core/audio/Le_Chant_des_pistes.mp3")
// await sound.load()
// sound.play(() => {
// // The callback will execute at every beat
// }) 

/// EDITOR
import express from "express"; 
import http from "http"; 
import { Server } from "socket.io"; 
import shell from 'shelljs'; 
import path from "path"; 
const path_cc = path;

import fs from "fs"; 
// import BeatBeat from "beat-beat-js"; 
import bodyParser from 'body-parser'; 
import { exec } from 'child_process'; 
// import { JSDOM } from 'jsdom'; 
import DOMPurify from 'dompurify'; 

import { fileURLToPath } from 'url';
import { dirname } from 'path';

// Obtenez le chemin du fichier actuel
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);


// Créez une instance d'Express
const app = express(); // Assurez-vous que cette ligne est présente
const httpServer = http.createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: '*', // Ajustez cela selon vos besoins
  },
});



///////////////
/// VARS
var images_stock = [],
  rmvimages_stock = [],
  connections = [];

// /// IP
//   var os = require('os');
import { networkInterfaces } from 'os'; // Importation du module os


// #################################
// #################################

// #######################
// PROD / DEV / IP / DOM?
// #######################

function getIPAddress() {
  // var interfaces = require('os').networkInterfaces();
  var interfaces = networkInterfaces(); // Utilisation de networkInterfaces

  for (var devName in interfaces) {
    var iface = interfaces[devName];

    for (var i = 0; i < iface.length; i++) {
      var alias = iface[i];
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
        return alias.address;
    }
  }
  return '0.0.0.0';
}
const adresses_ip=getIPAddress();
// console.log(adresses_ip)


process.env.TERM=="xterm-256color"
/// DEV /VS/ PROD
if (process.env.NODE_ENV === 'production') {

  console.log("-- production")
  var clic_domaine_to_start=memo_acceuil_dom;
  console.log(clic_domaine_to_start)

  // shell.exec('TERM="xterm-256color" && hash notify-send.sh 2> /dev/null && notify-send.sh "NODE CHOUQUETTE" \"'+clic_domaine_to_start+'\" -t 3000 --icon="/usr/share/icons/Papirus/48x48/apps/grsync.svg" --action="FIREFOX:./logs_tools/firefox.sh '+clic_domaine_to_start+'" --action="CC:./logs_tools/firefox.sh '+clic_domaine_to_start+' cc" || echo "notify-send not installed" >&2')


} else if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'pm2_dev' || !process.env.NODE_ENV) {

  console.log("NODE_ENV default == ( not set / dev )")
  console.log("-- To change it : 'export NODE_ENV=production'")
  console.log("-- Or do : '> NODE_ENV=production node server.js'")
  console.log("\nNB : cors_limitation == "+ cors_limitation)

  let port_pls = process.env.PORT ? process.env.PORT : nodeport
  var clic_domaine_to_start="http://"+adresses_ip+":"+ port_pls;
  console.log("\nSTART HERE : "+clic_domaine_to_start)

  if (process.env.NODE_ENV === 'pm2_dev') {
    // TOREDO 
    // shell.exec('notify-send.sh "CHOUQUETTE" "'+clic_domaine_to_start+'" -o "Button Action: $(echo -n '+clic_domaine_to_start+' | xsel --clipboard)" -t 10000 --icon=\'/usr/share/icons/Papirus/48x48/apps/grsync.svg\'');
  }

}


// ###############
// TODO HOWTO
// https://copyprogramming.com/howto/socket-io-get-local-ip-address
// ###
// app.configure('development', function(){
//   app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
// });
// app.configure('production', function(){
//   app.use(express.errorHandler()); 
// });
// Routes
// app.get('/', routes.index);
// ###############

// var address = socket.handshake.address;
// console.log('New connection from ' + address.address + ':' + address.port);

// #######################
// ##### CHOUQUETTE ######
// #######################

shell.exec("jp2a "+__dirname+"/client/core/img/chouquettes.png")

console.log("\n\nSTART HERE : "+clic_domaine_to_start+"\n\n")

// shell.exec('notify-send.sh "CHOUQUETTE" "'+clic_domaine_to_start+'" -o "copy url: $(echo -n '+clic_domaine_to_start+' | xsel --clipboard)" -t 10000 --icon=\'/usr/share/icons/perso/cc.png\'');


////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////// BEAT ////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
// import { analyze } from 'web-audio-beat-detector';

// analyze(audioBuffer)
//     .then((tempo) => {
//         // the tempo could be analyzed
//     })
//     .catch((err) => {
//         // something went wrong
//     });

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
//////////////              ////////////////////
///////////// R O U T I N G ////////////////////
/////////////              /////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////  
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////




/////////// PAGES

app.get("/cc/", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/cc/cc.html");
});
app.get("/up/", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/up/up.html");
});
app.get("/ls/", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/ls/ls.html");
});
app.get("/lss/", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/lss/lss.html");
});
app.get("/", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/index.html");
});
app.get("/about", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/about.html");
});

app.get("/text/", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/text/text.html"); 
});


// CACHE
// app.use('/sets/*', express.static(path.join(__dirname, 'sets'), {
//   setHeaders: (res) => {
//       res.set('Cache-Control', 'public, max-age=31557600'); // 1 an
//   }
// }));

// Route dynamique pour servir tous les fichiers dans /client/sets/
app.get("/sets/*", (req, res) => {
  const filePath = path.join(__dirname, "client/sets", req.params[0]);
  return res.sendFile(filePath, (err) => {
    if (err) {
      res.status(err.status).end();
    }
  });
});


// # const cors = require('cors'); // Ajout de l'importation de cors
// # app.use(cors({ origin: '*' })); // Autoriser toutes les origines


// QFIX
app.get("/00_errors/*", (req, res) => {
  const filePath = path.join(__dirname, "client/sets/00_errors", req.params[0]);
  return res.sendFile(filePath, (err) => {
    if (err) {
      res.status(err.status).end();
    }
  });
});

// ############## SETS
io.on("connection", (socket) => {
  var set_is=socket.handshake.query.set_is
    let regex_set_is="/"+set_is+"/*"
    app.get(regex_set_is, (req, res) => {
      // console.log(set_is)
      return res.sendFile(__dirname + "/client/sets" + req.path);
    });

// ############## ~Same /Sets/ --> Forced
  var set_is=socket.handshake.query.set_is
    let regex_set_is_b="/sets/"+set_is+"/*"
    app.get(regex_set_is_b, (req, res) => {
      // console.log(set_is)
      return res.sendFile(__dirname + "/client/" + req.path);
    });


    

    // TEXT EDITOR
    socket.on('editor_to_cc', (filename) => {
      console.log(`Fichier ouvert: ${filename}`);
      nsp_cc.emit("serv-neww-file", data_folder, data_filename, fileurl); //EMIT
    });



});

//// TRY WITHOUT /is_set/
//// SAFER : ALL /sets/ -> redirect to images 
// app.get("/sets/*", (req, res) => {
//   // console.log(set_is)
//   return res.sendFile(__dirname + "/client" + req.path);
// });
// app.get("/sets/*", (req, res) => {
//   return res.sendFile(__dirname + "/client/sets/"+req[0]);
// });

// var address = socket.handshake.address;

/////////// CORE
app.get("/js/*", (req, res) => {
  return res.sendFile(__dirname + "/client/core/" + req.path);
});
app.get("/css/*", (req, res) => {
    return res.sendFile(__dirname + "/client/core/" + req.path);
});
app.get("/img/*", (req, res) => {
  return res.sendFile(__dirname + "/client/core/" + req.path);
});
app.get("favicon.ico", (req, res) => {
  return res.sendFile(__dirname + "/client/core/img/favicon.gif");
});
///
app.get("/siofu/client.js", (req, res) => {
  return res.sendFile(__dirname + "/node_modules/socketio-file-upload/client.js");
});
app.get("/font/*", (req, res) => {
  // Vérification de l'existence du fichier avant de l'envoyer
  const filePath = path.join(__dirname, "/client/core/fonts", req.params[0]);
  fs.access(filePath, fs.constants.F_OK, (err) => {
    if (err) {
      console.error(`Fichier non trouvé: ${filePath}`);
      return res.status(404).send('Fichier non trouvé.');
    }
    return res.sendFile(filePath);
  });
});


// Route pour servir les fichiers d'erreur
app.get("/errors/*", (req, res) => {
  return res.sendFile(__dirname + "/client/pages/errors/" + req.params[0]); // Utilisez req.params[0] pour obtenir le bon chemin
});

// // Middleware pour gérer les routes non trouvées
// app.use((req, res, next) => {
//   const imageExtensions = /\.(jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)$/i; // Extensions d'images à exclure
//   if (imageExtensions.test(req.path)) {
//     return next(); // Ignore les requêtes pour les images
//   }
//   res.status(404).sendFile(__dirname + "/client/pages/errors/404.html"); // Remplacez par le chemin correct
// });



///////////////////////////////////////////////
///////////////////////////////////////////////


const nsp_ls = io.of("/ls");
const nsp_home = io.of("/");
const nsp_cc = io.of("/cc");
const nsp_up = io.of("/up");
const nsp_ed = io.of("/text");


///////////////////////////////////////////////
///////////////////////////////////////////////


////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
//////////////              ////////////////////
////////////  ON CONNECTION io ON //////////////
//////////////              ////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////



// GENERAL (ERROR)
// https://socket.io/docs/v4/troubleshooting-connection-issues/
io.engine.on("connection_error", (err) => {
  console.log(err.req);      // the request object
  console.log(err.code);     // the error code, for example 1
  console.log(err.message);  // the error message, for example "Session ID unknown"
  console.log(err.context);  // some additional error context
});




////////////////////////////////////
////////////  ALL IO  //////////////
////////////////////////////////////
io.on("connection", (socket) => {
  var set_is=socket.handshake.query.set_is

  socket.broadcast.emit("say_hello", "coucoucou"); //EMIT
  
  if (connections.length === 1)
    console.log("Connected:", connections.length, "socket.");
  else console.log("Connected:", connections.length, "sockets.");

  // Disconnecting
  socket.on('disconnect', () => {
    console.log('User has disconnected');
  });

}); // io.on('connection', (socket) => {









////////////////////////////////////
///////////  ED/gif IO  ////////////
////////////////////////////////////
nsp_ed.on("connection", (socket) => {
  socket.on('text_liste', function (filename) {
    console.log('Editeur --- connect ' + filename);

    // Fonction pour lister les fichiers et sous-dossiers de manière récursive
    const getAllFiles = (dirPath, arrayOfFiles) => {
      let files = fs.readdirSync(dirPath);
      arrayOfFiles = arrayOfFiles || [];
      files.forEach(file => {
          const fullPath = path.join(dirPath, file);
          if (fs.statSync(fullPath).isDirectory()) {
              // Appel récursif pour les sous-dossiers
              getAllFiles(fullPath, arrayOfFiles);
          } else {
              arrayOfFiles.push(fullPath); // Ajoute le fichier à la liste
          }
      });
      return arrayOfFiles;
    };

    // Utilisation de la fonction pour lister les fichiers
    fs.readdir('./client/texteditor/', (err, files) => {
      if (err) {
          console.error(`Erreur lors de la lecture du répertoire: ${err.message}`);
          return res.status(500).send('Erreur lors de la lecture du répertoire.');
      }

      const combinedFileList = getAllFiles('./client/texteditor/'); // Récupère tous les fichiers et sous-dossiers
      // console.log(combinedFileList);

      let sortedlist = combinedFileList.sort((a, b) => {
          return fs.statSync(`${b}`).mtime - fs.statSync(`${a}`).mtime;
      });

      socket.emit('editor_go', sortedlist, live_chouquette); // Envoie les fichiers sous forme de tableau
    });



    socket.on('img_src_array', (img_src_array, vitesse) => {
      console.log("img_src_array reçu :", img_src_array); 
      
      const filename_plz = path.basename(img_src_array.img_src_array[0])
      const filename_plz_fix = filename_plz.replace(".png", "").replace(".gif", "");
      
      console.error("000 = "+img_src_array[0])
      console.error("filename_plz_fix = "+filename_plz_fix) 
      
      const gifOutputPath = `./client/texteditor/gifs/${filename_plz_fix}.gif`; // Chemin de sortie pour le GIF
      console.error(gifOutputPath)

      let vitesse_value = img_src_array.vitesse || "10"; // Renommage pour éviter la redéclaration
      console.error("vitesse_value = "+vitesse_value)
      
      // GIF oui
      let img_src_array_join=img_src_array.img_src_array.join(' ');
      console.warn(img_src_array_join)


      exec(`convert -delay ${vitesse_value} -loop 0 ${img_src_array_join} ${gifOutputPath}`, (error, stdout, stderr) => {
        if (error) {
            console.error(`Erreur: ${error.message}`);
            return; 
        } else if (stderr) {
            console.error(`Erreur: ${stderr}`);
            return; 
        } else {
            console.log("0K GIF == " + filename_plz_fix+".gif");

            nsp_ed.emit('editor_new_file', "/gifs/"+filename_plz_fix + ".gif", "gif");
            socket.emit('confirm_gif');
        }
      }); 
    });
    
  });

  
  socket.on("ls-add-neww", function (data_folder, data_filename, fileurl, context, action) {
    nsp_cc.emit("serv-neww-file", data_folder, data_filename, fileurl, context, action); //EMIT
    console.log("--- ed /clic/ --- "+data_folder+" \n-- "+data_filename+" \n-- "+fileurl);
  });

  socket.on("img-rm", function (fileurl) {
    nsp_cc.emit("serv-img-rm", fileurl); //EMIT
    console.log("--- ed /clic/ img-RM  ----- "+fileurl);
  });

  socket.on("clear_all_cc", function () {
    nsp_cc.emit("serv-clear_all_cc"); //EMIT
    console.log("--- ed /clic/ clear_all_cc  -----");
  });

  socket.on("img-rm_textfile", function (fileurl) {
    // nsp_cc.emit("serv-img-rm", fileurl); //EMIT
    fs.unlink(fileurl, (err) => { // Supprime le fichier
      if (err) {
        console.error(`Erreur lors de la suppression du fichier: ${err.message}`);
        return;
      }
      console.log(`Fichier supprimé: ${fileurl}`);
    });
    console.log("--- ed /clic/ img-RM_textfile  ----- "+fileurl);
    nsp_ed.emit("serv-img-rm", fileurl); //EMIT

  });
}); // EDitor


nsp_home.on("connection", (socket) => {
  // var set_is=socket.handshake.query.set_is
  console.log("HOME - "+socket.id);
  console.log(socket.request.connection.remoteAddress);
  // console.log(" SETs (HOME) ?")
  socket.on("ls-folders_plz", function (set_is, to_nsp, window_host) {
    var set_is=socket.handshake.query.set_is
    function svoid(){};
    const getAllFiles_sets = function(dirPath, arrayOfFiles) {
      let files = fs.readdirSync(dirPath)
      arrayOfFiles = arrayOfFiles || []
      files.forEach(function(file) {
        let regex_dir_path=dirPath + "/" + file;
        if (fs.statSync(regex_dir_path).isDirectory()) {
          arrayOfFiles = getAllFiles_sets(regex_dir_path, arrayOfFiles)
          
          let newitem=dirPath.replace(/^(.+)\/([^\/]+)$/, "$2");
          var myString = newitem;
          var myRegexp = /\/sets\/$/gi;
          var match = myRegexp.exec(myString);
          if (!match) {
            if ( ( home_prod_domaines.includes(window_host) ) && (home_exclude_folders.includes(newitem))){
              console.log("------ "+newitem)
            } else {
              arrayOfFiles.indexOf(newitem) === -1 ? arrayOfFiles.push(newitem) : svoid("void");
            } 
          }
        } 
      })
      return arrayOfFiles
    }

    console.log("SET ((S)) - ALL , to_nsp = "+to_nsp)
    var regex_not_ls_pls="/client/sets/"
    const directoryPath = path.join(__dirname, regex_not_ls_pls);
    const result = getAllFiles_sets(directoryPath);

    /// EMIT !New to_nsp ==> will get client origine '/ls' (ls test page) 
    socket.emit("ls-folders", "folders", result); //EMIT
    console.log(result)
    console.log("Excluded ====\n"+home_exclude_folders)

  }); // ON ls-folders_plz

  socket.emit('editor_go', "", live_chouquette); // Envoie les fichiers sous forme de tableau


}); // HOME









////////////////////////////////////
////////////  LS IO  ///////////////
////////////////////////////////////
nsp_ls.on("connection", (socket) => {
  
  var set_is=socket.handshake.query.set_is 

  socket.on("ls-folders_plz", function (set_is, to_nsp) {
    var set_is=socket.handshake.query.set_is

    const getAllFiles = 
     function(dirPath, arrayOfFiles) {
      let files = fs.readdirSync(dirPath)
      arrayOfFiles = arrayOfFiles || []
      files.forEach(function(file) {
        let regex_dir_path=dirPath + "/" + file;
        if (fs.statSync(regex_dir_path).isDirectory()) {
          arrayOfFiles = getAllFiles(regex_dir_path, arrayOfFiles)
        } else {
          var myString = file;
          var myRegexp = /\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)$/gi;
          var match = myRegexp.exec(myString);
          if (match) {
            arrayOfFiles.push(path.join(__dirname, "/sets/"+dirPath, "/", file))
          }
        }
      })
      return arrayOfFiles
    }

    
    console.log("SET IS IS IS IS IS "+set_is)
    var regex_not_ls_pls="/client/sets/"+set_is+"/"
    var directoryPath = path.join(__dirname, regex_not_ls_pls);

    let folder_exists = ( fs.existsSync( directoryPath ) ) 
                      ? "oui" 
                      : "non";

    var directoryPath_fix = ( folder_exists == "oui" ) 
               ? path.join(__dirname, regex_not_ls_pls) 
               : path.join(__dirname, "/client/sets/00_errors/");

    var directoryPath_error = ( folder_exists == "non" ) ? "oui" : "non";


    const result = getAllFiles(directoryPath_fix);

    /// EMIT !New to_nsp ==> will get client origine '/ls' (ls test page) 
    socket.emit("ls-folders", "file", result, directoryPath_error); //EMIT
    // console.log(result)

  }); // ls-folders_plz


  // socket.emit("nextprev_ls", "img", "prev", "force_change"); LS ==>
  socket.on("nextprev_ls", function (type, todo, option) {
    if (type=="img"){
      switch (todo) {
        case "next":
          nsp_cc.emit("ls-change_bg", todo, option); //EMIT
        break;
        case "prev":
          nsp_cc.emit("ls-change_bg", todo, option); //EMIT
        break;
      default:
      }
    }
    console.log("LS - nextprev_ls ==> "+ type +" - "+ todo +" - "+ option)
  });


  socket.on("ls-add-neww", function (data_folder, data_filename, fileurl, context, action) {
    if (context != "piano"){
      nsp_cc.emit("serv-neww-file", data_folder, data_filename, fileurl); //EMIT
      console.log("--- ls /clic/ --- "+data_folder+" \n-- "+data_filename+" \n-- "+fileurl);
    } else { // PIANO
      nsp_cc.emit("serv-neww-file", data_folder, data_filename, fileurl, context, action); //EMIT
      console.log("--- ls /piano/ --- "+data_folder+" \n-- "+data_filename+" \n-- action -- "+action+ " -- "+Date() );
    }
  });


  socket.on("ls-loadall", function (go_folder, how, set_folder, folder_number) {

    // EMIT cleanup
    nsp_cc.emit("cc-loadall_pre_cleanup", go_folder, how, set_folder); //EMIT

    console.log("\n\n\n--- LOAD ALL --- "+set_folder+" / " +go_folder);
    let regex_set_path="client/sets/"

    // GET /rootpath/path/SET/GO_specific_Folder/
    // REGEX
    const directoryPath_sub = path.join(__dirname, "/"+regex_set_path+"/"+set_folder+"/"+go_folder+"/");
    console.log("directoryPath_sub == "+directoryPath_sub)
    var t = 0;
    
    const getAllFiles = function(dirPath, arrayOfFiles) {
      let files = fs.readdirSync(dirPath) 
      arrayOfFiles = arrayOfFiles || []
      files.forEach(function(file) {
        t++;
        // fs.readdir(dirPath, (err, files) => {
        //   console.error("--- Count : "+files.length);
        //   var files_len = files.length;
        // });
        // console.error("--- Count : "+files.length+" / "+t);
        var myString = file;
        var myRegexp = /\.(jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF|svg|webp)$/gi;
        var match = myRegexp.exec(myString);
        if (match) {
          if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
          } else {
            arrayOfFiles.push(path.join("/"+set_folder, go_folder, file))
          }
        } else {
          console.log("not an image")
        }
      }) // files.forEach
      return arrayOfFiles
    } // getAllFiles
    
    const result = getAllFiles(directoryPath_sub);
    console.log("\n\n");
    console.log(result);
    console.error(result.length);
    let quantite=result.length;

    nsp_cc.emit("cc-subfolder", "file", result, how, folder_number, quantite); //EMIT
    
  }); // ls-loadall




  /// LOAD ALL (ls) GO_FOLDERS (loop1 : folders_list) -> all files (loop2 : getAllFiles --> result)
  //// LS load_all_folders -> Server : ls-load_all_folders ( here ) --> cc-load_all_folders
  socket.on("ls-load_all_folders", function(set_is, to_nsp, window_host, cond) {

    var u=0;
    console.log("===================")
    console.log("===================")
    console.log("\n\n\n\n-------\nls-load_all_folders\n set_is: "+set_is+", \n to_nsp: "+to_nsp+", \n window_host : "+window_host+"\n-------\n\n\n\n")

    let regex_set_is="client/sets/"+set_is
    var directoryPath = path_cc.join(__dirname, "/"+regex_set_is+"/");
    let folder_exists = ( fs.existsSync( directoryPath ) ) 
    ? "oui" 
    : "non";
  
    console.error(__dirname)
    var directoryPath_sub_cc = ( folder_exists == "oui" ) 
    ? directoryPath 
    : path_cc.join(__dirname+"/client/sets/00_errors/");
  
    console.log (" d---------------------------d ")
    console.error(directoryPath_sub_cc)
    console.log (" d---------------------------d ")
  
    const folders_list=getAllFolders_cc(directoryPath_sub_cc)
  
    let t=0;
    let set_folder = set_is;
    var countfolders=0;
    let count_total = folders_list.length


    console.log("====== FOLDERS ! ====== "+countfolders+ "/" +count_total)
    
    // loop1 -- LOAD ALL (ls)
    folders_list.forEach(function(name, current_num) {
    // folders_list.forEach(function (item, index, array) {
      countfolders++
      let regex_set_is="client/sets/"+set_is
      let fixname_folder=name.replace(__dirname+"/"+regex_set_is+"/", "");
      console.log("\n--- number -- "+current_num+ " --- folder -- "+fixname_folder+"\n--- name -- "+name)

      let go_folder = fixname_folder;
      
      // loop2 -- LOAD ALL (ls)
      const getAllFiles = function(dirPath, arrayOfFiles) {
        let files = fs.readdirSync(dirPath) 
        arrayOfFiles = arrayOfFiles || []
        files.forEach(function(file) {
          t++;
          var myString = file;
          var myRegexp = /\.(jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF|svg|webp)$/gi;
          var match = myRegexp.exec(myString);
          if (match) {
            if (fs.statSync(dirPath + "/" + file).isDirectory()) {
              arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
            } else {
              arrayOfFiles.push(path.join("/"+set_folder, go_folder, file))
            }
          } else {
            console.log("not an image")
          }
        }) // files.forEach
        return arrayOfFiles
      } // getAllFiles
      
      const directoryPath_sub = name;
      const result = getAllFiles(directoryPath_sub);
      
      console.log("\n\nFILES --- >");
      console.log(result);
      let get_folder_files = result;
      let folder_num = Number(current_num - 1);
      let folder_name = go_folder;
      let dirname_plz = __dirname;

      let isfinalone = (count_total == countfolders) ? "oui_finaleone" : "not_finaleone"
      nsp_cc.emit("cc-load_all_folders", get_folder_files, folder_name, folder_num, dirname_plz, set_folder, isfinalone, cond); // GET_FILES
      // nsp_cc.emit("cc-load_all_folders", name, result); //EMIT



    });
  
  });



  socket.on("ls-remove_all_plz", function (from) {
    console.log("---- RESETALL ! ! ! -- from: "+from)
    nsp_cc.emit("cc-reset_all"); //EMIT
  });
  
  socket.on("ls-rm_this_group", function (go_folder, how, set_folder) {
    console.log("---- REMOVE ! ! ! "+go_folder)
    nsp_cc.emit("cc-rm_this_group", go_folder, how, set_folder); //EMIT
    /// CC will search "go_folder" in [data-folder] 
  });

  socket.on("new_speed_socket", function (new_speed) {
    nsp_cc.emit("cc-new_speed_socket", new_speed); //EMIT
    console.log("---- NEW SPEED ! ! !")
  });  
  socket.on("ls-reload", function () {
    nsp_cc.emit("cc-reload"); //EMIT
    console.log("---- RELOAD ! ! !")
  });
  socket.on("ls-killme", function (what) {
    nsp_cc.emit("cc-killme", what); //EMIT
    console.log("---- KILLME "+what+" ! ! !")
  });
  socket.on("ls-randme", function () {
    nsp_cc.emit("cc-randme"); //EMIT
    console.log("---- randme X X X")
  });

  socket.on("ls-pause_me", function (what) {
    nsp_cc.emit("ls-pause_me", "forced"); //EMIT
    console.log("---- PAUSE from: LS P P P")
  });

  socket.on("ls-lock_me", function (what) {
    nsp_cc.emit("ls-lock_me"); //EMIT
    console.log("---- LOCK from: LS")
  });

  socket.on("ls-point", function (id) {
    nsp_cc.emit("ls-point", id); //EMIT
    console.log("---- POINT from: LS -- id = "+id)
  });


  socket.on("newstyle", (data) => {
    // Vérifiez si une destination est spécifiée
    if (data.to === "cc") {
        // Émettez l'événement "newstyle" vers l'espace de noms "/cc"
        nsp_cc.emit("newstyle");
        console.warn("newsyle !!!")
    }
  });

  socket.on("style_change", function (type) {
    nsp_cc.emit("cc_style_change", type); //EMIT
    console.error("type ? = "+type)
  });

  socket.on("extra_set", function () {
    nsp_cc.emit("cc-extra_set"); //EMIT
  });  

  socket.on("goeffect", function (type) {
    nsp_cc.emit("cc_goeffect", type); //EMIT
    console.error("type ? = "+type)
  });

  socket.on("ls-point_exist", function (askurl, action) {
    nsp_cc.emit("cc_point_exist_ask", askurl, action); //EMIT
  });

  socket.on("ls-restart", function (what) {
    console.log('_____________ ______________   L S  ___ restart_rand    -- 0')
    console.log('_____________ what    -- 0 what = ' +what)
    var set_is=socket.handshake.query.set_is
    let min=Number("0");
    function between(min, max) {  
      return Math.floor(
        Math.random() * (max - min) + min
      )
    }
    console.log('_____________ ______________   L S  ___ restart_rand    -- 1')
    console.log('_____________  set_is '+set_is)
    let regex_set_is="client/sets/"+set_is
    // let from="LINE --- 534";
    var directoryPath_sub_cc = path_cc.join(__dirname, "/"+regex_set_is+"/");
    const folders_list=getAllFolders_cc(directoryPath_sub_cc)
    let howmuch_list_re=folders_list.length;
    console.log("\n\n\n\n\n--------- howmuch_list_re == "+howmuch_list_re+"\n\n\n\n\n----------\n\n")
    
    switch (what) {
      case "restart_rand":
           let rand_numfix=between(min, howmuch_list_re) 
           console.log('_____________ ______________   L S  ___ restart_rand    -- 2')
          //  let rand_numfix=10;
           console.log("\n\n\n\n\n--------- rand_numfix == "+rand_numfix+"\n\n\n\n\n----------\n\n")
           // EMIT
           nsp_cc.emit("cc-restart", what, rand_numfix); //EMIT
        break;
        default:
          console.log('_____________ ______________   L S  ___ restart_rand    -- 3')
          nsp_cc.emit("cc-restart", what, 1); //EMIT
    } //SWITCH
    console.log("---- RESTART X X X")
  });


  socket.on("style_change", function (type) {
    nsp_cc.emit("style_change", type); //EMIT
  });

  

  nsp_ls.on('disconnect', () => {
    console.log('LS has disconnected');
  });


}); // nsp_ls ...?









/////////////////////////////
/////////// U P /////////////
/////////////////////////////
// TODO FIX WHY nsp_up NOT WORKS ? w/ /up
// var siofu = require("socketio-file-upload");
import siofu from "socketio-file-upload";
nsp_up.on("connection", function(socket){

  var set_is=socket.handshake.query.set_is
  var set_go=socket.handshake.query.set_go
  
  console.log("____ UP ___  _ SET IS == "+set_is+ " --- "+set_go)
  let regex="/client/sets/"+set_is+"/"+set_go+"/"
  const directoryPath_up = path.join(__dirname, regex);
  var uploader = new siofu();
  uploader.dir = directoryPath_up;


  // up_server_rm_img --- REMOVE
  socket.on("up_server_rm_img", function (data_old_name, data_filename, fileurl, context) {
    rm_file_function(data_old_name, data_filename, fileurl, context, set_is, set_go)
  });



  socket.on("up_server_rename_img", function (data_old_name, data_filename, fileurl, fileurl_newname, param_go_fix, param_set_fix, context) {

    console.log('\n_____\n up_server_rename_img \n')

    var myString1 = fileurl_newname;
    var myRegexp1 = /[a-zA-Z0-9À-Ÿà-ÿ\.\_\(\)]{1,}/gi;
    var match1 = myRegexp1.exec(myString1);
    
    if (!match1) {
      console.error("NOT SAFE FILE NAME")
      let message = "CARACTERES SEULEMENT : A-Z 0-9 +_-.()";
      nsp_up.emit('go_server_popup', message, 1000, 200, "yellowgreen", "cond", set_is, set_go);
      // nsp_up.emit('go_server_rm_img', "REGEX_MATCH", "CARACTERES SEULEMENT : A-Z 0-9 +_-.()");
      return;
    }


    let fileurl_newname_safe = accentsTidy(fileurl_newname)
    let data_old_name_fix = param_set_fix +"/"+ param_go_fix + "/" + path.basename(data_old_name)
    let fileurl_newname_fix = param_set_fix +"/"+ param_go_fix + "/" + path.basename(fileurl_newname_safe)
    
    console.warn("\n \
      data_old_name ==  \t"+  data_old_name+"\n \
      fileurl_newname_safe == \t"+fileurl_newname_safe+"\n \
      data_filename ==  \t"+  data_filename+"\n \
      fileurl ==  \t \t"+     fileurl+"\n \
      fileurl_newname_fix ==  \t"+fileurl_newname_fix+"\n \
      param_go_fix ==  \t \t"+param_go_fix+"\n \
      context ==  \t \t"+     context
    )

    if (context == "y"){
      
      if (data_old_name_fix != fileurl_newname_fix) {
        
        var myString = fileurl_newname_fix;
        var myRegexp = /\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)/gi;
        var match = myRegexp.exec(myString);

        if (match) {
          console.warn("match === " +match);
          console.warn("OK IMG for sure");
          // var add_ext = "."+match[1]
          var add_ext = ""
          console.warn("???? 1 "+add_ext)
        } else {
          console.log("match === NONE");
          console.log("NOT IMG for sure");
          var add_ext = ".JPG"
          console.log("???? 2 "+add_ext) 
        }

        console.warn("---- fileurl --------------\n \
                        "+fileurl+"\n")
        console.warn("---- fileurl_newname_fix ------\n \
                        "+fileurl_newname_fix+"\n")

        const dir_fileurl = path.join(__dirname, "/client/sets/", fileurl);
        const dir_fileurl_newname = path.join(__dirname, "/client/sets/" + fileurl_newname_fix + add_ext);
        console.warn("---- FINAL ---"+dir_fileurl + " --- " + dir_fileurl_newname)
        
        // EXEC
        fs.readFile(dir_fileurl_newname, (err, data) => {
          if (!err && data) {
            var rename_exists = "exists"
          } else {  
            shell.exec("mv "+ dir_fileurl + " " + dir_fileurl_newname)
            var rename_exists = "rename"
          }

          setTimeout(function () {
            // EMIT
            nsp_up.emit('go_server_rm_img', data_old_name, data_filename + add_ext, fileurl_newname_fix + add_ext, rename_exists); //EMIT
          },200);
        
        }); // fs.readFile


      }
    } else {
      // console.error("X (top) === "+fix_x_upPath)
      // shell.exec("mv \"" + fix_x_upPath + "\" \"" + fix_x_upPath_new + "\"")
    }
    
  });

  socket.on("up_re_namebatch", function (fileurl, param_set_fix, param_go_fix, data_name, fprefix, lname, fcount) {

    if (fprefix=="" && lname=="") {
      let message = "CHAMPS VIDES";
      nsp_up.emit('go_server_popup', message, 1000, 200, "yellowgreen", "cond", set_is, set_go);
      // nsp_up.emit('go_server_rm_img', "REGEX_MATCH", "CHAMPS VIDES");
      return;
    }

    if (fprefix!="") {
      var myString1 = fprefix;
      var myRegexp1 = /[a-zA-Z0-9À-Ÿà-ÿ\.\_\(\)]{1,}/gi;
      var match1 = myRegexp1.exec(myString1);
      
      if (!match1) {
        console.error("NOT SAFE FILE NAME -- fprefix")
        let message = "CARACTERES SEULEMENT : A-Z 0-9 +_-.()";
        nsp_up.emit('go_server_popup', message, 1000, 200, "yellowgreen", "cond", set_is, set_go);  
        // nsp_up.emit('go_server_rm_img', "REGEX_MATCH", "CARACTERES SEULEMENT : A-Z 0-9 +_-.()");
        return;
      }  
    }

    if (lname!="") {
      var myString2 = lname;
      var myRegexp2 = /[a-zA-Z0-9À-Ÿà-ÿ\.\_\(\)]{1,}/gi;
      var match2 = myRegexp2.exec(myString2);
      
      if (!match2) {
        console.error("NOT SAFE FILE NAME -- lname")
        let message = "CARACTERES SEULEMENT : A-Z 0-9 +_-.()";
        nsp_up.emit('go_server_popup', message, 1000, 200, "yellowgreen", "cond", set_is, set_go);  
        // nsp_up.emit('go_server_rm_img', "REGEX_MATCH", "CARACTERES SEULEMENT : A-Z 0-9 +_-.()");
        return;
      }
    }
    


    var extention_is = data_name.replace(/(.*)\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)$/, "$2")
    let extention_fix = (extention_is == "") ? ".jpg" : "."+extention_is;

    console.warn(
      "\n extention_is == "+extention_is, 
      "\n extention_fix == "+extention_fix, 
      "\n fileurl == \t"+fileurl, 
      "\n param_set_fix = "+param_set_fix, 
      "\n param_go_fix =  "+param_go_fix, 
      "\n data_name == \t"+data_name, 
      "\n fprefix == \t"+fprefix, 
      "\n lname == \t"+lname,
      "\n fcount == \t"+fcount
    )

    let fprefix_fix = (fprefix !== '')  ? fprefix + "_" + fcount + "_" : "";
    let lname_fix =   (lname !== '')    ? lname : data_name;
    let no_change_noname = (fprefix.length < 1) ? "__"+fcount : "";
    let no_change =   (((fprefix == '') && (lname == ''))) ? fcount + "_" : "";
    
    console.log("fprefix_fix == "+fprefix_fix)
    console.log("lname_fix == "+lname_fix)

    let new_name = param_set_fix+ "/" +param_go_fix+ "/" + no_change + fprefix_fix + lname_fix + no_change_noname + extention_fix;
    let new_url_final = __dirname + "/client/sets/"+new_name
    let fileurl_final = __dirname + "/client/sets/"+fileurl
    let finale_commande = "mv \"" + fileurl_final + "\" \""+ new_url_final+"\"";
    
    // console.log("\n\n"+finale_commande+"\n\n");
    shell.exec(finale_commande)
    nsp_up.emit("up_re_namebatch_done"); /// EMIT


  });

  socket.on("up_server_renamefolder", function (newfolder_fix, final_olddir) {
    console.warn("\n______\n up_server_renamefolder =\n"+newfolder_fix+"\n\n")
    let final_newdir = __dirname + "/client/sets" + newfolder_fix;
    let final_olddir_fix = __dirname + "/client/sets" + final_olddir;
    console.warn("mv "+final_olddir_fix+ " "+final_newdir)
    shell.exec("mv "+final_olddir_fix+ " "+final_newdir)
  }); /// up_server_renamefolder

  socket.on("up_server_newfolder_set", function (newfolder_fix) {
    console.warn("\n______\n up_server_newfolder_set =\n"+newfolder_fix+"\n\n")
    let final_newdir = __dirname + "/client/sets" + newfolder_fix;
    // let final_olddir_fix = __dirname + "/client/sets" + final_olddir;
    console.warn("mkdir "+final_newdir)
    shell.exec("mkdir "+final_newdir)
  }); /// up_server_newfolder_set


  socket.on("up_server_newfolder", function (newfolder_fix, final_olddir) {
    console.warn("\n______\n up_server_newfolder =\n"+newfolder_fix+"\n\n")
    let final_newdir = __dirname + "/client/sets" + newfolder_fix;
    // let final_olddir_fix = __dirname + "/client/sets" + final_olddir;
    console.warn("mkdir "+final_newdir)
    shell.exec("mkdir "+final_newdir)
  }); /// up_server_newfolder





  /// up_dwl_list -> upload_end
  socket.on("up_dwl_list", function (dwlContent, param_set, param_go) {
    let final_dest = __dirname + "/client/sets/" + param_set + "/" +param_go+ "/";
    
    console.log('              '); console.log('              ');
    console.log("\n\n\n______\n")
    console.warn("\n\n\n______\n up_dwl_list =\n"+ param_set + " / " + param_go+"\n==> "+final_dest+"\n\n")
    const lines = dwlContent.split('\n'); // Sépare le contenu en lignes
    
    /////////
    //// FILE SAVE
    // DATE - cur_date
    const formatDateISO = (date) => {
        // Convert the date to ISO string
        const isoString = date.toISOString();
        // Split at the "T" character to get the date part
        const formattedDate = isoString.split("T")[0];
        return formattedDate;
    };
    const currentDate = new Date();
    const cur_date = formatDateISO(currentDate)

    // TIME - timedate
    let timedate = currentDate.toLocaleString([], {
      hour: "2-digit",
      minute: "2-digit",
    });
    console.log(cur_date + " -- "+timedate); // Output: yyyy-mm-dd

    /// FILE
    let file_save_url = path.join(__dirname, "/client/sets/", param_set +"/"+param_go+"/dwl_liste____"+cur_date+".txt")
    console.log("file_save_url === "+ file_save_url)
    // ON crée le fichier
    shell.exec("touch "+file_save_url);
    // ON ajoute un double saut à la fin (separation)
    fs.appendFile(file_save_url, "\n\n"+cur_date+" -- "+timedate+"\n", (err) => {
    });

    //// FILE SAVE end
    /////////

    console.log('              ');
    
    let count_lines = lines.length 
    let count = 0;
    console.log("\ncount_lines === "+count_lines+"\n")

    lines.forEach(function(line) {
      // var count_lines = lines.length    
    
      if (line != "") { /// TODO cl normalement pas besoin, filtrage coté client (en amont)

        var myString = line;
        // var myRegexp = /\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)/gi;
        var myRegexp = /^(.*?)(?=.)(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)/; // Modifié pour capturer tout avant la virgule
        var match = myRegexp.exec(myString);


        if (match) {
          // console.warn("match === " +match);
          // console.warn("OK IMG for sure");
          var add_ext = "."+match[2]
          // console.warn(add_ext)
          var line_fix_ext = match[1];
        } else {
          // console.log("match === NONE");
          // console.log("NOT IMG for sure");
          var add_ext = ".JPG"
          // console.log(add_ext) 
          var line_fix_ext = line
        }

        var line_fixed = line_fix_ext.replace(/\&(.*)$/g,"")

        // var add_ext (match) 

        // let line_long = line.length;
        // let line_long = line.length;
        let line_max  = 65; // max 256
        let line_long = line_max;
        // let san_trunc = truncate(line_fixed, line_long);
        let sanitized = decodeURIComponent(line_fixed).replace(/[^\w\s_\?\=]/gi, '_').replace(/\?/gi, '_ASK_')
                  .replace(/[\n\t\r]/gi, '')
                  .replace(/\=/gi, '_EGAL_')
                  .replace(/\</g, '_LT_')
                  .replace(/\>/g, '_GT_')
                  .replace(/\&/g, '_AMP_')
                  .replace(/«/g, '_QUOT_')
                  .replace(/»/g, '_EQUOT_')
                  .replace(/\?/g, '_ASK_')
                  .replace(/\@/g, '_ATT_')
                  .replace(/\!/g, '_EXC_')
                  .replace(/\*/g, '_AST_')
                  .replace(/\#/g, '_HAS_')
                  .replace(/\(/g, '_PARA_')
                  .replace(/\)/g, '_EPARA_')
                  .replace(/\:/g, '_DPOINT_')
                  .replace(/ /g, '+')
                  .replace(/\'/g, "_APOS_")
                  .replace(/[`~@#$%^&()|+\-=;:'",.<>\{\}\[\]\\\/]/g, '+')
                  .replace(/(SELECT|INSERT|UPDATE|DELETE|DROP|CREATE|ALTER|TRUNCATE|EXEC|UNION|--|#|\/\*|\*\/)/gi, ''); // Filtre SQL;

        let final_file_name = sanitized +""+ add_ext
        let line_link = line.replace(/\&(.*)$/g,"").replace(/\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)(.*)$/gi,"\.$1")

        
        // let final_dest_counted = final_dest + count + "__" + param_set +"__"+ param_go
        // --show-progress
        let finale_commande = "cd "+final_dest+" && wget -O "+final_file_name+" -q --xattr --timeout=3 --tries=1 "+line_link;
        /// LOG
        // console.log("TEST ___long___ "+line_long+"\n")
        // console.log("TEST _sani fin_ "+final_file_name+"\n")
        // console.log("TEST ___----___ "+finale_commande+"\n")
        /// TEST
        // shell.exec("cd "+final_dest+" && echo 'LS TEST ----\n' && ls")
        // console.warn("T E S T \n" +final_dest+"\n+ "+final_file_name)

        if (!fs.existsSync(final_dest + final_file_name)) {

          // AJOUT des lignes
          fs.appendFile(file_save_url, line+"\n", (err) => {
          });

          /// GO
          // let data_fix_ok = "/" + param_set +"/"+ param_go +"/"+ final_file_name
          let message = "G0 --- : "+ final_file_name;
          nsp_up.emit('go_server_popup', message, 1000, 200, "yellowgreen", "dwl_liste", set_is, set_go);

          // shell.exec(finale_commande)

          execAsync(finale_commande)
          .then(output => {
              
              console.log('              ');
              count++;
              console.warn(count+" -- "+line)
    
              // console.log(`Sortie: ${output}`);
              shell.exec("jp2a --colors --height=15 "+final_dest + final_file_name);
              console.log("<----------------- "+final_file_name+"\n\n\n\n");
              console.log("")
              nsp_up.emit('go_server_popup', "OK -- "+final_file_name, 2000, 200, "yellow", "cond", set_is, set_go);  
              
              nsp_up.emit('upload_end', final_file_name, "dwl_file", set_is, set_go); //EMIT
              
              
              if (count_lines == count) {
                console.log('____ dwl_list __ END\n              '); console.log('              ');
                nsp_up.emit('go_server_popup', "TELECHARGEMENTS FINIS (liste)", 8000, 200, "blue", "dwl_end", set_is, set_go);  
                
              }


          })
          .catch(error => {
              console.error(`Erreur: ${error.message}`);
              nsp_up.emit('go_server_popup', "dwl_liste", 2000, 200, "red", "dwl_error", set_is, set_go);  
          });

          setTimeout(function () {
            // console.warn("final_file_name == "+final_file_name)
            // nsp_up.emit('upload_end', final_file_name, "dwl_liste", set_is, set_go); //EMIT

          },100);


        } else {
          console.warn("E X I S T S \n" +final_dest+"\n+ "+final_file_name)
          nsp_up.emit('go_server_popup', "FILE EXISTS -- "+final_file_name, 3000, 200, "red", "dwl_error", set_is, set_go);  

          fs.appendFile(file_save_url, "--- EXISTS --- "+line+"\n", (err) => {
          });

        }

      } // if (line != "") {

    }) // FOR each -- lines

    let message = "END";
    nsp_up.emit('go_server_popup', message, 1000, 200, "yellow", "CLOSE_MODAL", set_is, set_go);  

  }); /// up_dwl_list



  socket.on("up_server_move_imgs", function (fileUrl, name, selectedValue_destination, param_set_fix) {
    let original_dir = __dirname + "/client/sets/" + fileUrl;
    let final_newdir = __dirname + "/client/sets/" + param_set_fix + "/" + selectedValue_destination + "/" + name;
    // console.warn("\n______\n up_server_move_imgs =\n"+original_dir+" --> "+final_newdir+"\n\n")
    // let final_olddir_fix = __dirname + "/client/sets" + final_olddir;
    console.warn("mv "+original_dir+" "+final_newdir)
    shell.exec("mv "+original_dir+ " "+final_newdir)
  });
  



  let accentsTidy = function(s){
      var r=s.toLowerCase();
      r = s.replace(new RegExp("[àáâãäå]", 'g'),"a");
      r = r.replace(new RegExp("æ", 'g'),"ae");
      r = r.replace(new RegExp("ç", 'g'),"c");
      r = r.replace(new RegExp("[èéêë]", 'g'),"e");
      r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
      r = r.replace(new RegExp("ñ", 'g'),"n");                            
      r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
      r = r.replace(new RegExp("œ", 'g'),"oe");
      r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
      r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
      r = r.replace(new RegExp("[ÀÁÂÃÄÅ]", 'g'),"A");
      r = r.replace(new RegExp("[ÈÉÊË]", 'g'),"E");
      r = r.replace(new RegExp("[ÌÍÎÏ]", 'g'),"I");
      r = r.replace(new RegExp("[ÒÓÔÕÖ]", 'g'),"O");
      r = r.replace(new RegExp("[ÙÚÛÜ]", 'g'),"U");
      r = r.replace(new RegExp("[ÝŸ]", 'g'),"Y");
      return r;
  };



  uploader.listen(socket);
  var streams = {};

  //// START
  uploader.on("start", function(event){

      var data_fix = event.file.name.replace(/[^A-Za-z0-9\.]/gi, "_").toLowerCase();
      var file_extension = path.extname(event.file.name); // Récupérer l'extension
      var fileNameWithoutExt = path.basename(event.file.name, path.extname(event.file.name)); // Récupérer le nom sans extension
      let fileNameWithoutExt_accents = accentsTidy(fileNameWithoutExt)
      let fileNameWithoutExt_prefix = fileNameWithoutExt_accents.replace(/[^A-Za-z0-9\.]/gi, "_").toLowerCase()
      
      // TODO PREFIX_UP -- "up__"+ -- retiré
      var data_fix_ok = encodeURIComponent(fileNameWithoutExt_prefix) + file_extension;
      console.log("llllllllll :: "+event.file.name)
      console.log("iiiiiiiiii :: "+data_fix_ok)

      let regex_not_up="/client/sets/"+set_is+"/"+set_go+"/"
      const directoryPath = path.join(__dirname, regex_not_up, data_fix_ok);

      if (!fs.existsSync(directoryPath)) {
        streams[event.file.id] = fs.createWriteStream(directoryPath);

        socket.emit('upload.start', {
            data_name:data_fix,
            data_id:event.file.id
          },
          "do_no_existe_go"
        )
      } else {

        socket.emit('upload.start', {
            data_name:data_fix,
            data_id:event.file.id
          },
          "exist_already"
        )

      }

  }); //// START


  //// PROGRESS
  uploader.on('progress', function(event) {
    streams[event.file.id].write(event.buffer);
    console.log(event.file.bytesLoaded / event.file.size)
    var data_fix = event.file.name.replace(/[^A-Za-z0-9\.]/gi, "_").toLowerCase();

    socket.emit('upload.progress', {
      percentage:(event.file.bytesLoaded / event.file.size) * 100,
      data_name: data_fix,
      data_id:event.file.id
    })
  }); //// PROGRESS


  //// COMPLETE
  uploader.on("complete", function(event){
    
    streams[event.file.id].end();

    console.warn(" -- hand set : "+set_is+" -- hand go : "+set_go)

    console.warn("event.file.name - -- - - "+event.file.name);

    var data_fix = event.file.name.replace(/[^A-Za-z0-9\.]/gi, "_").toLowerCase();
    var file_extension = path.extname(event.file.name); // Récupérer l'extension
    var fileNameWithoutExt = path.basename(event.file.name, path.extname(event.file.name)); // Récupérer le nom sans extension
    /// RE_TODO Les changements de noms de fichiers devraient se faire  avant l'upload
    let fileNameWithoutExt_accents = accentsTidy(fileNameWithoutExt)
    let fileNameWithoutExt_prefix = fileNameWithoutExt_accents.replace(/[^A-Za-z0-9\.]/gi, "_").toLowerCase()

    // TODO PREFIX_UP -- "up__"+ -- retiré
    var data_fix_ok = encodeURIComponent(fileNameWithoutExt_prefix) + file_extension;
    console.log("llllllllll __ :: "+event.file.name)
    console.log("iiiiiiiiii __ :: "+data_fix_ok)

    let regex_not_up="/client/sets/"+set_is+"/"+set_go+"/"
    const directoryPath = path.join(__dirname, regex_not_up, data_fix_ok);
    const directoryOrig = path.join(__dirname, regex_not_up, event.file.name);

    console.log("directoryPath :: "+directoryPath)
    console.log("directoryOrig :: "+directoryOrig)

    // console.log(JSON.stringify(streams, null, 2))
    console.log(streams[event.file.id].path)
    // TODO WHY RM !
    // shell.exec("rm -rf \"" + directoryOrig + "\"")
    
    // socket.emit('upload_end', data_fix_ok, "solo"); //EMIT

    setTimeout(function () {
      //// DEPRECIATED context === data_filename, context, set, go
      nsp_up.emit('upload_end', data_fix_ok, "broardcast", set_is, set_go); //EMIT
      // TODO USE set+go => Dedicated ROOM (same has origine)
      nsp_ls.emit('upload_end', data_fix_ok, "broardcast", set_is, set_go); //EMIT
      delete streams[event.file.id];
    }, 400); //SETtimeout
    
    // nsp_cc.emit("up-add-neww_complete")
  }); //// COMPLETE

  uploader.on("error", function(event){
      console.log("Error from uploader", event);
  });




  socket.on("up-add-neww", function (data_folder, data_filename, filelink, context) {
    console.log("ccccccccccccccccccccccccccccc "+context)
    if(context=="from_up_page") {
      let regex_sets="/sets/"
      const data_filenamePath = path.join(__dirname, regex_sets, data_filename)
      nsp_cc.emit("serv-neww-file", data_folder, data_filenamePath, "/sets/"+filelink, context); //EMIT
      console.log("--- up /clic/ ( "+context+" ) -- "+data_filenamePath);
    } else {
      let regex_sets="/sets/"
      const data_filenamePath = path.join(__dirname, regex_sets, data_filename)
      nsp_cc.emit("serv-neww-file", data_folder, data_filenamePath, "/sets/"+filelink); //EMIT
      console.log("--- up /clic/ --- "+data_folder+" \n-- "+data_filenamePath+" \n-- "+filelink);
    }
  });



  
  socket.on("up-folders_pls", function () {
    var set_is=socket.handshake.query.set_is

    let regex_not_up="/client/sets/"+set_is+"/"+set_go+"/"
    const directoryPath = path.join(__dirname, regex_not_up);

    if (!fs.existsSync(directoryPath)) {
      shell.exec("mkdir "+__dirname + regex_not_up)
      console.log("_UP FOLDER CREATED")
    }

    console.log("\n==> up-folders_pls <==\n");
    const getAllFiles = function(dirPath, arrayOfFiles) {
      let files = fs.readdirSync(dirPath)
      arrayOfFiles = arrayOfFiles || []
      files.forEach(function(file) {
        
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
          // SUB DIRS TODO DISABLE
          arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {

          var myString = file;
          var myRegexp = /\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)$/gi;
          var match = myRegexp.exec(myString);
          if (match) {
            arrayOfFiles.push({
                path: path.join(dirPath, "/", file),
                mtime: fs.statSync(path.join(dirPath, "/", file)).mtime // Ajout de la date de modification
            });
          }
        }
      })
      // TRI mod
      arrayOfFiles.sort((a, b) => a.mtime - b.mtime); // Tri par date de modification croissante
      return arrayOfFiles
    } // getAllFiles

    
    /// RESULT
    const result = getAllFiles(directoryPath);
    result.forEach(function(result) {
      /// EMIT
      let data_filename = result.path;
      socket.emit("up-folders", "files", "/", data_filename, set_is); //EMIT  
    }); // forEach resultat --- result.forEach

  }); /// socket.on("up-folders_pls",



  /// TODO --- doublon  up-load_all_folders / up-folders_pls ?
  /// up-load_all_folders
  socket.on("up-load_all_folders", function(set_is) {

    var directoryPath_sub_cc = path.join(__dirname, "/client/sets/"+set_is+"/");
    const folders_list=getAllFolders_cc(directoryPath_sub_cc);
    let set_folder = set_is;
    let regex_set_is="client/sets/"+set_is

    console.log("====== FOLDERS UP ( up-load_all_folders ) ======")
    let result = []; // Initialiser un tableau pour stocker les résultats

    folders_list.forEach(function(name, index) {
      let fixname_folder=name.replace(__dirname+"/"+regex_set_is+"/", "");
      console.log(name+" -- "+index+" --- "+fixname_folder)
      result.push({ name: name, index: index, fixname_folder: fixname_folder }); // Ajouter un objet au tableau
    });

    // let folder_up = get_folder_up.sort((a, b) => a.mtime - b.mtime); // Tri par date de modification croissante
    socket.emit("load_all_folders", result, set_folder); // GET_F OLDERS

  }); /// up-load_all_folders


  // socket.on("up-error", function(errmess) {
  socket.on("up-error", function (errmess, fileurl) {
    let filename = fileurl.split('/').pop();

    console.error("-- up-error (errmess + filename) == \n"+errmess+"\n"+fileurl)
    console.warn("fileurl \t=== "+fileurl)
    console.warn("filename \t=== "+filename)
    
    let safe_fileurl = fileurl.replace(/(.*)\/sets\//g, "");
    rm_file_function(filename, filename, safe_fileurl, "SUPPRIME", set_is, set_go)

    let message = "ERROR === "+filename
    nsp_up.emit('go_server_popup', message, 1000, 200, "red", "up-error", set_is, set_go);

  }); /// up-load_all_folders


  
  
  socket.on("up-folders_files", function (set_is, set_go, to_nsp) {
    var set_is=socket.handshake.query.set_is
    var set_go=socket.handshake.query.set_go

    console.log("\n\n----------\nup-folders_files\n------------\n")

    const getAllFiles = 
     function(dirPath, arrayOfFiles) {
      let files = fs.readdirSync(dirPath)
      arrayOfFiles = arrayOfFiles || []
      files.forEach(function(file) {
        let regex_dir_path=dirPath + "/" + file;
        if (fs.statSync(regex_dir_path).isDirectory()) {
          arrayOfFiles = getAllFiles(regex_dir_path, arrayOfFiles)
        } else {
          var myString = file;
          var myRegexp = /\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)$/gi;
          var match = myRegexp.exec(myString);
          if (match) {
            arrayOfFiles.push(path.join(__dirname, "/sets/"+dirPath, "/", file))
          }
        }
      })
      return arrayOfFiles
    }

    
    console.log("SET IS IS IS IS IS "+set_is)
    var regex_not_ls_pls="/client/sets/"+set_is+"/"+set_go+"/"
    var directoryPath = path.join(__dirname, regex_not_ls_pls);

    let folder_exists = ( fs.existsSync( directoryPath ) ) 
                      ? "oui" 
                      : "non";

    var directoryPath_fix = ( folder_exists == "oui" ) 
               ? path.join(__dirname, regex_not_ls_pls) 
               : path.join(__dirname, "/client/sets/00_errors/");

    var directoryPath_error = ( folder_exists == "non" ) ? "oui" : "non";
    const result = getAllFiles(directoryPath_fix).reverse(); 


    console.warn("------- result.length ::: " +result.length)

    // ! à faire plutot coté client : ca permet d'afficher un feedback si length == 0 : Dossier vide
    // if (result.length > 0) {
      socket.emit("up_folder_files_data", "file", result, directoryPath_error); //EMIT
    // }

  }); // ls-folders_plz


}); /// nsp_up.on







////////////////////////////////////
////////////  CC IO  ///////////////
////////////////////////////////////
nsp_cc.on("connection", (socket) => {
  var set_is=socket.handshake.query.set_is
  socket.join("room1");

  socket.on("cc-get_folder_files_extra", function (set_is, folder) {

    let folder_path=__dirname+"/client/sets/"+set_is+"/"+folder;
    let folder_files_new = getAllFiles_cc(folder_path);
    socket.emit("cc-get_folder_files_extra_go", folder_files_new,set_is, folder);    //EMIT

  });


  let regex_set_is="client/sets/"+set_is
  var directoryPath = path_cc.join(__dirname, "/"+regex_set_is+"/");
  let folder_exists = ( fs.existsSync( directoryPath ) ) 
  ? "oui" 
  : "non";

  console.error(__dirname)
  var directoryPath_sub_cc = ( folder_exists == "oui" ) 
  ? directoryPath 
  : path_cc.join(__dirname+"/client/sets/00_errors/");

  console.log (" d---------------------------d ")
  console.error(directoryPath_sub_cc)
  console.log (" d---------------------------d ")

  const folders_list=getAllFolders_cc(directoryPath_sub_cc)

  console.log("====== FOLDERS ======")
  folders_list.forEach(function(name, current_num) {
    let regex_set_is="client/sets/"+set_is
    // let regex_set_is_dirname=__dirname+"/client/"set_is"/"
    let fixname_folder=name.replace(__dirname+"/"+regex_set_is+"/", "");
    console.log("\n--- number -- "+current_num+ " --- folder -- "+fixname_folder)
  });

  var u=0;
  
  const getAllFiles_cc = function(dirPath, arrayOfFiles) {
    let files = fs.readdirSync(dirPath)
    console.log("===================")
    console.log("====== FILES ======")
    
    arrayOfFiles = arrayOfFiles || []
    files.forEach(function(file) {
      if (fs.statSync(dirPath + "/" + file).isDirectory()) {
        console.log("getAllFiles_cc c1")
        arrayOfFiles = getAllFiles_cc(dirPath + "/" + file, arrayOfFiles)

      } else {
        var myString = file;
        var myRegexp = /\.(jpg|png|jpeg|webp|gif|jpg|png|jpeg|webp|gif|JPEG|JPG|PNG|GIF)$/gi;
        var match = myRegexp.exec(myString);
        
          if (match) {
            u++;
            arrayOfFiles.push(path_cc.join(dirPath, "/", file))
            if (u<2) {
              shell.exec("jp2a --height=15 "+dirPath+"/"+myString) 
            }
            console.log("file == "+myString)

          } else {
            console.log("XXXX -- "+myString)
          }
      }
    })
    return arrayOfFiles
  }
  console.log("===================")
  console.log("===================")
  var u=0;

  console.log("---------- is_up -----------------------")
  let isup_number=Number(folders_list.indexOf(__dirname+'/client/sets/'+set_is+'/_up'));
  console.log("isup_number ====== "+isup_number)
  if (isup_number != -1){
    nsp_cc.emit("cc-demorand", isup_number, "isup_number"); /// EMIT
    let isup_path=folders_list[isup_number];
    var name = isup_path.match(/([^\/]*)\/*$/)[1]
    console.log("isup_path ====== "+name)
  } else {
    console.log("isup_number == IS FIRST (-1)")
  }
  
  console.log("---------- list_first_path_name -----------------------")
  let list_first_path=folders_list[0];
  // var list_first_path_name = list_first_path.match(/([^\/]*)\/*$/)[1] // REGEX last folder only <- //TODO use it not replace __dirname
  var list_first_path_name = path.dirname(list_first_path);
  nsp_cc.emit("cc-demorand", list_first_path_name, "list_first_path_name"); /// EMIT
  console.log("------ (( "+list_first_path);
  console.log("------ (( "+list_first_path_name);

  console.log("---------- howmuch_number -----------------------")
  // QFIX -1
  const howmuch_list=Number(folders_list.length-1);
  nsp_cc.emit("cc-demorand", howmuch_list, "howmuch_list"); /// EMIT
  console.log(howmuch_list);



  socket.on("cc-get_folder_files", function (current_fld, current_num, show_uploads, condition) {
    console.log("--------------------------------- GOOOO cc-get_folder_files")
    if (current_num=="params") {
      let params=current_fld;
      // REGEX
      let folder_name=params.replace(__dirname+"/client/sets/"+set_is+"/", "");
      folders_list.forEach(function (item, index, array) {
        // REGEX
        let item_fix=item.replace(__dirname+"/client/sets/"+set_is+"/", "");
        if (item_fix==folder_name) {
          console.log("PARAMMMMMMMMM "+folder_name, item_fix, index);
          let folder_files_new = getAllFiles_cc(directoryPath_sub_cc + "/" + folder_name);
          var folder_num=Number(index - 1)
          /// HERE PARAMS
          nsp_cc.emit("cc-folder_files", folder_files_new, folder_name, folder_num, __dirname);    //EMIT
        }
      });

    } else { // PARAMS

      // console.log('_____________ ______________ cc-get_folder_files -- 0')
      if (show_uploads) { 
        //QFix exclude for UP (crashing when UP.html is connected... because dont have var show_uploads)
        // console.log('_____________ ______________ cc-get_folder_files -- 1')

        console.log("\n\n\n\n\n-------------------------\n\n GET ====> "+current_fld+" \n( "+current_num+" sur "+howmuch_list+" )\n")
        console.log("-- howmuch_list -- "+howmuch_list+" --- show_uploads : "+show_uploads)

        if (current_fld=="_up" && show_uploads=="no"){ 
          // TODO show_uploads not everywhere + Bypassed cc.js ==> if(current_num_next==is_up) {
          
          var current_num_next=Number(current_num) + 1
          console.log("NEXT (c1 is _up) -- current_num_next == "+current_num_next+" )) ) )) ) ))) ))))))))")
          // console.log('_____________ ______________ cc-get_folder_files -- 3')
        } else { // current_fld=="_up" && show_uploads=="no"){ 
          
          if (current_num=="rand"){   
            // console.log('_____________ ______________ cc-get_folder_files -- 4')
            let min=Number("0");
            function between(min, max) {  
              return Math.floor(
                Math.random() * (max - min) + min
              )
            }  
            let rand_numfix=between(min, howmuch_list)
            // let rand_numfix=10;

            console.log("---------------------------------")
            console.log("---------------------------------")
            console.log("isup_number )))) "+isup_number+" == "+rand_numfix)
            console.log("RAND_NUM == "+rand_numfix)
            console.log("---------------------------------")
            console.log("---------------------------------")

            if (isup_number == rand_numfix) {
              var current_num_next=rand_numfix + 1;
              nsp_cc.emit("cc-demorand", current_num_next, "isup_number"); /// EMIT
            } else {
              var current_num_next=Number(rand_numfix);
              nsp_cc.emit("cc-demorand", current_num_next); /// EMIT
            }
            

          } else {

            console.log("___________________ current_num == " + current_num)

            if (condition=='cc_restart') {
              
              var current_num_next=-1
              console.log('_____________ ______________ cc-get_folder_files -- 5 condition '+condition+' ----- FIX')

            } else if (condition=='default_first_folder') {
              
              var current_num_next=-1
              console.log('_____________ ______________ cc-get_folder_files -- 5 condition '+condition+' ----- FIRST')

            } else {
              
              var current_num_next=Number(current_num) + 1 
              console.log('_____________ ______________ cc-get_folder_files -- 5 condition '+condition+' ----- notFIX')

            }
            
            console.log("NEXT (c2 is no _up) -- current_num_next == "+current_num_next+" )) ) )) ) ))) ))))))))")      
            
          }

        } // current_fld=="_up" && show_uploads=="no"){ 

        // console.log("\n\n ---- current_num_now ----\n"+current_num_now+"\n")
        // console.log("\n\n ---- howmuch_list ----\n"+howmuch_list+"\n")
        let folders_list_next0 = (folders_list[current_num_next]) 
                                ? folders_list[current_num_next] 
                                : folders_list[1]
        
        // console.log('_____________ ______________ cc-get_folder_files -- 6 ')
        console.log("___________________ folders_list_next0 == " + folders_list_next0)
        console.log("DEBUG == current_num_next == "+current_num_next)
        console.log("DEBUG == howmuch_list == "+howmuch_list)
        
        if (current_num_next > howmuch_list) {  // END SCENARIO // NOT OK == LAST ( NEXT > LISTE )    
          //TODO Make a function
          if (end_scenario == "reload") {       // SC RELOAD
            // console.log('_____________ ______________ cc-get_folder_files -- 7')
            nsp_cc.emit("cc-reload");           //EMIT
          } else {                              // SC DEFAULT = LOAD_MORE
            // console.log('_____________ ______________ cc-get_folder_files -- 8')
            console.log("\n\n\n\nE N D - 01\n\n\n\n\n")
            nsp_cc.emit("cc-demo_load_others", "reload_from_zero"); //EMIT
            shell.exec("jp2a "+__dirname+"/client/core/img/chouquettes.png")
          }


        } else {         
          // NORMAL
          // console.log('_____________ ______________ cc-get_folder_files -- 7')
          // console.log("\n\n\nfolders_list_next0 == " + folders_list_next0)
          // console.log(__dirname+"/client/uploads/")
          if (folders_list_next0) { // Si pas de dossier suivant (1dossier) => crash
            
            // let folders_list_next = folders_list_next0.replace(__dirname+"/client/sets/"+set_is+"/", "")
            // let folders_list_next = folders_list_next0.replace(__dirname+"/client/sets/"+set_is+"/", "");

            let folder_exists = ( fs.existsSync( __dirname+"/client/sets/"+set_is+"/" ) )
            ? "oui" 
            : "non";
          
            let folders_list_next = ( folder_exists == "oui" )  
                                  ? folders_list_next0.replace(__dirname+"/client/sets/"+set_is+"/", "")
                                  : path.dirname(folders_list_next0)
            
            console.log("\n==> folders_list_next == " + folders_list_next)
            
            if (folders_list_next) {                      // NEXT exists
              
              // console.log('_____________ ______________ cc-get_folder_files -- 8')
              let folder_name=folders_list_next;
              // let folder_files_new = getAllFiles_cc(directoryPath_sub_cc + "/" + folder_name);

              let folder_files_new = ( folder_exists == "oui" )  
              ? getAllFiles_cc(directoryPath_sub_cc + "/" + folder_name)
              : getAllFiles_cc(folder_name)


              console.log("\n==> folder_name 1 == " + folder_name);
              
              if (folder_name=="_up" && show_uploads=="yes" || folder_name!="_up") {          // notUP / UP?ok
              
                console.log('_____________ ______________ cc-get_folder_files -- 9 <-- ' +folder_name+ " -- current_num : "+current_num+" --- current_num_next : "+current_num_next)
                // let qf_current_num_next = Number(current_num);
                //// HERE NO PARAMS NO UP or OK
                //// DEBUG log files
                // console.log('emit("cc-folder_files", '+folder_files_new+', '+folder_name+', '+current_num_next+', '+__dirname+')')
                nsp_cc.emit("cc-folder_files", folder_files_new, folder_name, current_num_next, __dirname);    //EMIT

              } else {       
                console.log('_____________ ______________ cc-get_folder_files -- 10')
                let current_num_next=Number(current_num) + 1
                nsp_cc.emit("cc-folder_files", folder_files_new, folder_name, current_num_next, __dirname); //EMIT

              }
              
            } else {                                      // NEXT NOT exists   == END SCENARIO / PBL ( NEXT > LISTE )
              console.log('_____________ ______________ cc-get_folder_files -- 11')
              console.log("\n\n\n\nE N D - 03\n\n\n\n\n")
              shell.exec("jp2a "+__dirname+"/client/core/img/chouquettes.png")
              nsp_cc.emit("cc-reload"); //EMIT
              
              // TODO SCENARIO ALT == RANDOM X TIME (long) THEN REBOOT
              // TODO / OR /
              // TODO END SC : load previous sets not loaded when demo start not at 0
              // TODO var is_demo_start = 6 -> END --> load 1-5 -> reEND --> Loop (without reloading)
            }
          }  // folders_list_next
        } // folders_list_next0
      } // if (show_uploads) { 
    } // PARAMS
  }); // cc-get_folder_files

  // next_folder_socket --> start_folder -> cc-loadall
  // -> cc-loadall -> nsp_cc.emit("cc-subfolder"...
  socket.on("cc-loadall", function (ls_state, dirname, next_num, next) {
    if (ls_state=="first") {
      console.log("\n\n\n--- LOAD ALL cc-loadall first /// next : "+dirname+" - " +ls_state +" /// FIRST");
      //REGEX
      var directoryPath_sub_cc = path_cc.join(__dirname, "/client/sets/"+set_is+"/"+next+"/");
    } else {
      console.log("\n\n\n--- LOAD ALL cc-loadall /// next : "+next+" - " +next_num +" /// prev : "+dirname+" - "+ls_state);
       
      //REGEX
      var directoryPath_sub_cc = path_cc.join(__dirname, "/client/sets/"+set_is+"/"+next+"/");
    }
    
    console.log("directoryPath_sub_cc == "+directoryPath_sub_cc)
    const getAllFiles_cc = function(dirPath, arrayOfFiles) {
      let files = fs.readdirSync(dirPath)
      arrayOfFiles = arrayOfFiles || []
      files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
          arrayOfFiles = getAllFiles_cc(dirPath + "/" + file, arrayOfFiles)
        } else {
          console.log("getAllFiles_cc c3")
          arrayOfFiles.push(path_cc.join(__dirname, "/", file))
        }
      })
      return arrayOfFiles
    } // getAllFiles_cc
    
    const result_cc = getAllFiles_cc(directoryPath_sub_cc);
    console.log("\n\ncc-loadall\n\n");
    
  }); // END cc-loadall



  socket.on("cc-point_response", function (askurl, response) {
    nsp_ls.emit("cc-point_response", askurl, response); //EMIT
    console.log("cc-point_response == "+askurl)
  });

}); // END nsp_cc.on 











// GET FILES (before cc)
const getAllFolders_cc = function(dirPath, arrayOfFiles) {
  let files = fs.readdirSync(dirPath)
  arrayOfFiles = arrayOfFiles || []
  files.forEach(function(file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      // console.log("getAllFiles_cc c0")
      arrayOfFiles = getAllFolders_cc(dirPath + "/" + file, arrayOfFiles)
      arrayOfFiles.push(path_cc.join(dirPath, "/", file))
    // } else {
    //   arrayOfFiles.push(path_cc.join(__dirname, dirPath, "/", file))
    }
  })
  return arrayOfFiles
}






















app.use(bodyParser.urlencoded({ extended: true }));

app.get("/socket.io/socket.io.js", (req, res) => {
    return res.sendFile(__dirname + "/node_modules/socket.io/client-dist/socket.io.js");
  });

app.get("/client/texteditor/*", (req, res) => { // Changement ici pour capturer le chemin
    const filePath = path.join(__dirname, "client/texteditor", req.params[0]); // Utilisation de req.params[0]
    return res.sendFile(filePath, (err) => {
        if (err) {
            console.error(`Erreur lors de l'envoi du fichier: ${err.message}`);
            res.status(err.status).end();
        }
    });
});


app.post('/submit', (req, res) => {

    const sanitizeInput = (input) => {
    return input.replace(/\n/g, "_BR_")
                .replace(/\</g, '_LT_')
                .replace(/\>/g, '_GT_')
                .replace(/\&/g, '_AMP_')
                .replace(/«/g, '_QUOT_')
                .replace(/»/g, '_EQUOT_')
                .replace(/\?/g, '_ASK_')
                .replace(/\@/g, '_ATT_')
                .replace(/\!/g, '_EXC_')
                .replace(/\*/g, '_AST_')
                .replace(/\#/g, '_HAS_')
                .replace(/\(/g, '_PARA_')
                .replace(/\)/g, '_EPARA_')
                .replace(/\:/g, '_DPOINT_')
                .replace(/ /g, '+')
                .replace(/\'/g, "_APOS_")
                .replace(/\'/g, '\'')
                .replace(/\ /g, '+');
    };


    const userInput =  req.body.text
    const userInputsave =  req.body.text.replace(/ /, "+").replace(/\n/g, "_BR_").replace(/\r/g, ""); // Échappe les caractères spéciaux
    // const userInput =  DOMPurify.sanitize(userInputsave).replace("_BR_", "\n").toUpperCase(); // Échappe les caractères spéciaux
    const userInput_prefinal = userInputsave.replace(/\`/g, '\\$1');
    const userInput_final = userInput_prefinal.replace(/_BR_/g, '\n').replace(/\&lt;/g, '<').replace(/&gt;/g, '>').replace(/\+/g, '\ ').replace(/\'/g, '\'').toUpperCase();
    
    console.warn("userInput_final == " +userInput_final)

    const textColor = DOMPurify.sanitize(req.body.textColor); // Récupère la couleur du texte
    const bgColor = DOMPurify.sanitize(req.body.bgColor); // Récupère la couleur du fond
    const fontSize = DOMPurify.sanitize(req.body.fontSize); // Récupère la taille de la police
    
    const filename =  DOMPurify.sanitize(req.body.filename).replace(/\ /, "+").replace(/\n/g, "_BR_"); // Échappe les caractères spéciaux
    let filename_plz = (filename) ? filename.replace(/\n/g, "_BR_").replace(/\r/g, "") : userInputsave; // Remplacement des sauts de ligne
    let filename_plz_fix = sanitizeInput(filename_plz);
    

    console.log("\n\n"+userInputsave+"\n\n"); // Affiche le texte dans le terminal
    console.log("\n\n"+filename_plz_fix+"\n\n"); // Affiche le texte dans le terminal

    // GIF non (normal)
    // Exécute une commande ImageMagick avec les couleurs et la taille de police spécifiées
    exec(`convert -size 1280x720 xc:'${bgColor}' -gravity Center -pointsize ${fontSize} -fill '${textColor}' -font 'keepcalm' -draw "text 0,0 '${userInput_final}'" ./client/texteditor/${filename_plz_fix}.png`, (error, stdout, stderr) => {
      // Ajout de vérifications pour gérer les erreurs lors de l'exécution de la commande
      if (error) {
          console.error(`Erreur: ${error.message}`);
          return res.status(500).send('<div style="font-family: sans-serif;">Erreur lors de l\'exécution de la commande. <br> DEBUG = <br>text =<br>'+userInput+'<br>Sans doute un caractère spécial mal passé...<br><br><a href="../text">Retour</a><br></div><iframe style="width: 540px;height: 300px;border: 0;margin-top: 30px;" src="https://cc.bonnebulle.xyz/cc?set=error&live=oui"> ');

      }
      else if (stderr) {
          console.error(`Erreur: ${stderr}`);
          return res.status(500).send('<div style="font-family: sans-serif;">Erreur lors de l\'exécution de la commande. <br> DEBUG = <br>text =<br>'+userInput+'<br>Sans doute un caractère spécial mal passé...<br><br><a href="../text">Retour</a><br></div><iframe style="width: 540px;height: 300px;border: 0;margin-top: 30px;" src="https://cc.bonnebulle.xyz/cc?set=error&live=oui"> ');

      } else {
        nsp_ed.emit('editor_new_file', filename_plz_fix+".png");
      }

      // res.send('Commande exécutée avec succès !');
      let textColor_fix = textColor.replace('#', '') 
      let bgColor_fix = bgColor.replace('#', '') 
      // let filename_plz = (filename) ? filename : userInput;
      
      const sanitizeInput = (input) => {
        return input.replace("\n", "_BR_")
                    .replace(/\</g, '_LT_')
                    .replace(/\>/g, '_GT_')
                    .replace(/\&/g, '_AMP_')
                    .replace(/«/g, '_QUOT_')
                    .replace(/»/g, '_EQUOT_')
                    .replace(/\?/g, '_ASK_')
                    .replace(/\@/g, '_ATT_')
                    .replace(/\!/g, '_EXC_')
                    .replace(/\*/g, '_AST_')
                    .replace(/\#/g, '_HAS_')
                    .replace(/\(/g, '_PARA_')
                    .replace(/\)/g, '_EPARA_')
                    .replace(/\:/g, '_DPOINT_')
                    .replace(/ /g, '+')
                    .replace(/\'/g, "_APOS_")
                    .replace(/[`~@#$%^&()|+\-=;:'",.<>\{\}\[\]\\\/]/g, '+');
      };

      let filename_fix = filename ? sanitizeInput(filename) : '';
      let userInput_fix = sanitizeInput(userInputsave);
                    
      res.redirect(`/text?tx=${userInput_fix}&fn=${filename_fix}&tc=${textColor_fix}&bg=${bgColor_fix}&fs=${fontSize}&confirm=oui`);
    }); // exec
}); // app.post('/submit',







// Disconnecting
nsp_cc.on('disconnect', () => {
  console.log('CC has disconnected');
});

// console.log(process.env.PORT +" || "+ nodeport)
httpServer.listen(process.env.PORT || nodeport);

// httpServer.listen(3000, () => {
// 	console.log('Server listening on port 3000');
// 	console.log("\n\n\n\n\n\n\n\n")
// });
















