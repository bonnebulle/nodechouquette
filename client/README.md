# SETS in separed folder (and git reporepositorie/repo)   
   
Sets contains images (large ammon of), so I separated it in an other repo   
For easy git upload/update, I use symilinks to "sets" (alias)   
   
So the "real"/"origine" "sets" folder is not here, in this direcctory :   
(root)/nodechouquette/clients/sets/   
but :   
../../cc_sets == (root)/cc_sets   
   
## IN PRODUCTION    
you can replace the linked folder (alias) with a real "sets" folder (put here)   
   
### RESUME FOLDERS   
(root)/sets_cc/ (original)   
(root)/nodechouquette/clients/sets/ (alias named set) (or original in production)   
   
# SETS STRUCTURES   
(root)/nodechouquette/clients/sets/01_thematique/   
(root)/nodechouquette/clients/sets/01_thematique/_up <--- needed (todo, optional)   
(root)/nodechouquette/clients/sets/01_thematique/AAA_Sub_thema/image.png, ....   
(root)/nodechouquette/clients/sets/01_thematique/BBB_Sub_thema/image.png, ....   