// var server_adresse = "http://192.168.1.21:3030";

const urlParams = new URLSearchParams(window.location.search);
const param_set = urlParams.get("set")
if (param_set){
  var param_set_fix=param_set.toString().replace("set=","")
  console.warn("!!!!! PARAM SET (param_set_fix) == "+param_set_fix)  
//   alert(param_set_fix)
} else {
  var param_set_fix="00_defaut"
//   alert(param_set_fix)
}

let set_is = param_set;
// Vérifiez si 'param_set' est un objet et extrayez la valeur souhaitée
if (typeof param_set === 'object' && param_set !== null) {
    // Remplacez 'key' par la clé appropriée de l'objet que vous souhaitez utiliser
    set_is = param_set.key; // Exemple : param_set.key
} else {
    set_is = param_set.toString(); // Convertir en chaîne si ce n'est pas un objet
}
// alert(set_is);


const window_host=window.location.host;

// ioo PARAMS etc, MV into .html
let ioo = io({ query: { set_is: param_set_fix } })
var socket = io.connect("/ls");

/// ASK SERVER to WATCHER/LISTEN FOLDER
// socket.emit("ls-stalker", "from_ls");

const last_url_segment=window.location.href.substring(window.location.href.lastIndexOf('/') + 1).replace(/\?.*/,"")
const to_nsp="/"+last_url_segment // --> "/ls" / "/"" / "/cc"

$( document ).ready(function() {
    /// AUTOSTART Liste 
    list_and_all(to_nsp) // TODO USE last_url_segment

    $('#listpls').click(function() {
        list_and_all(to_nsp)
    });
    $('#listrefresh').click(function() {
        liste_refresh("refresh"); // TODO TESTS
    });

    $('#load_all_folders').click(function() {
        // alert("ls-load_all_folders");
        // alert(to_nsp);
        // alert(window_host)

        console.log("CLIC --- ls-load_all_folders");
        socket.emit("ls-load_all_folders", set_is, to_nsp, window_host, "button");
    });
    
    

    $('#rm_iframe').click(function() {
         
        if ($(this).hasClass("removed")) {
            $(this).removeClass("removed")
            $("body").removeClass("removed_iframe")
            $("#here_folders").before("<div id=\"demo_cc\"><iframe src=\"/cc?set="+param_set_fix+"&live=oui\"></iframe></div>")
            // $("iframe").removeClass("removed")
        }
        else {
            $(this).addClass("removed")
            $("body").addClass("removed_iframe")
            // $("iframe").addClass("removed")
            $("iframe").remove();
        }
    });

    $('#piano').click(function() {
        $("html").toggleClass("piano")
    });

    $('#newstyle').click(function() {
        newstyle();
    });

    const param_ok=param_set_fix
    $("#cc_link").attr("href", "/cc?set="+param_ok)
    $("#cc_up").attr("href", "/up?set="+param_ok)

    $('#remove_all').click(function() {
        // alert("lspls port = "+socket);
        // $("#here_folders > div").remove();
        setTimeout(function() { 
            socket.emit("ls-remove_all_plz");
        }, 100);
    });

    $('#reload').click(function() {
        // alert("lspls port = "+socket);
        // $("#here_folders > div").remove();
        setTimeout(function() { 
            socket.emit("ls-reload");
        }, 200);
    });

    $('#killme_next').click(function() {
        setTimeout(function() { 
            socket.emit("ls-killme", "next");
        }, 200);
    });

    $('#killme_prev').click(function() {
        // alert("lspls port = "+socket);
        // $("#here_folders > div").remove();
        setTimeout(function() { 
            socket.emit("ls-killme", "prev");
        }, 200);
    });

    $('#extra_set').click(function() {
        extra_set() 
    });
    $('#randme').click(function() {
        // alert("lspls port = "+socket);
        // $("#here_folders > div").remove();
        setTimeout(function() { 
            socket.emit("ls-randme");
        }, 100);
    });

    $('#restart').click(function() {
        socket.emit("ls-remove_all_plz", "restart");
        setTimeout(function() { 
            socket.emit("ls-restart", "default_first_folder");
            // ==> CC => cc-restart
            // alert("test");
        }, 100);
    });

    $('#restart_rand').click(function() {
        socket.emit("ls-remove_all_plz", "restart_rand");
        setTimeout(function() { 
            socket.emit("ls-restart", "restart_rand");
        }, 0);
    });

    $('#next_img').click(function() {
        socket.emit("nextprev_ls", "img", "next", "force_change");
    });

    $('#prev_img').click(function() {
        socket.emit("nextprev_ls", "img", "prev", "force_change");
    });

    $('#pause').click(function() {
        socket.emit("ls-pause_me", "forced"); //EMIT
    });

    $('#lock').click(function() {
        socket.emit("ls-lock_me", "forced"); //EMIT
        $("body").toggleClass("lock");
    });

    $('#point, #back, #rereturn').click(function() {
        let id = $(this).attr("id");
        socket.emit("ls-point", id); //EMIT
        // alert(id)
        $("body").addClass(id);
    });

    $('#revert_style').click(function() {
        setTimeout(function() { 
            socket.emit("style_change", "revert");
        }, 200);
    });
    $('#gray_style').click(function() {
        setTimeout(function() { 
            socket.emit("style_change", "gray");
        }, 200);
    });
    $('#blue_style').click(function() {
        setTimeout(function() { 
            socket.emit("style_change", "blue");
        }, 200);
    });
    $('#vert_style').click(function() {
        setTimeout(function() { 
            socket.emit("style_change", "vert");
        }, 200);
    });
    $('#rouge_style').click(function() {
        setTimeout(function() { 
            socket.emit("style_change", "rouge");
        }, 200);
    });
    $('#reset_style').click(function() {
        setTimeout(function() { 
            socket.emit("style_change", "reset");
        }, 200);
    });
    $('#multi_style').click(function() {
        // alert("hellp")
        setTimeout(function() { 
            socket.emit("style_change", "multi");
        }, 200);
    });
    $('#goeffect').click(function() {
        // alert("hellp")
        // setTimeout(function() { 
            socket.emit("goeffect", "go");
        // }, 200);
    });

    var number_ms = 10;
    $('#plusvite').click(function() {
        var original_speed = localStorage.getItem('original_speed');
        var new_speed = Number(original_speed) - Number(number_ms);
        console.log("\nNew_Speed +\n" + new_speed + "\n");
        localStorage.setItem('original_speed', new_speed);

        $(".image").each(function () {        
        $(this).css("animation-duration", new_speed + "ms").addClass("manual_speed");
        });
        socket.emit("new_speed_socket", new_speed);
        // alert("newspeed = "+new_speed);
    });

    $('#moinsvite').click(function() {
        var original_speed = localStorage.getItem('original_speed');
        var new_speed = Number(original_speed) + Number(number_ms);
        
        // if (new_speed < min_speed) {
        console.log("\nNew_Speed +\n" + new_speed + "\n");
        localStorage.setItem('original_speed', new_speed);

        $(".image").each(function () {        
            $(this).css("animation-duration", new_speed + "ms").addClass("manual_speed");
        });
        socket.emit("new_speed_socket", new_speed);
        //   alert("newspeed = "+new_speed);
    });

    setTimeout(() => {
        socket.emit("ls-load_all_folders", set_is, to_nsp, window_host);
    }, 1500); // Délai de 500 ms



}); // Doc.READY




// buttall_click -> ls-loadall
function buttall_click (go_folder, how, set_folder, folder_number) {
    socket.emit("ls-loadall", go_folder, how, set_folder, folder_number);
    // ___> SERVER --> cc-subfolder -> CC
    // alert(count)
};

// $('.buttall_rm').each(function() {
function buttall_click_rm (go_folder, how, set_folder) { // TODO Server will rm all data-folder==go_folder (not use set_folder)
    socket.emit("ls-rm_this_group", go_folder, how, set_folder);
    // alert("rm")
    // alert("___> SERVER --> cc-subfolder -> CC")
}
// });


function extra_set () { 
        socket.emit("extra_set");
}


function newstyle () { 
    // socket.emit("newstyle");
    socket.emit("newstyle", { to: "cc" });
    $("html").toggleClass("newstyle_ls")
}
    

var folder_number=-1

function list_and_all(to_nsp) {
    var o=0
    setTimeout(function() {
        /// EMIT
        socket.emit("ls-folders_plz", param_set_fix, to_nsp);
        setTimeout(function() { 
            const protocol = window.location.protocol;
            const host = window.location.host;
            const current_host=protocol+"//"+host;
            console.log(current_host);
            // alert("ss")
            $('.column:not(.ok)').each(function() {
                folder_number++;
                // alert(count)
                // let datafu=$(this).attr("data-folder");
                let datafu_set_folder=$(this).attr("data-folder").replace(/^(.+)\/([^\/]+)$/, "$1");
                let datafu_go_folder=$(this).attr("data-folder").replace(/^(.+)\/([^\/]+)$/, "$2");
                // alert("df == "+datafu)
                $(this).find("div").first().before("<div class='grp_buttons' data-count='"+folder_number+"'> \
                    <div id='cc_links' data-set-folder='"+datafu_set_folder+"'> \
                        <a class='go_url_href' href='"+current_host+"/cc?set="+datafu_set_folder+"&go="+datafu_go_folder+"&live=oui' target='chouquette'>"+datafu_go_folder+"</a> \
                        <a class='go_url_href href_loop' href='"+current_host+"/cc?set="+datafu_set_folder+"&go="+datafu_go_folder+"&loop=yes&live=oui' target='chouquette'>( loop )</a> \
                        <a class='go_url_href href_up' href='"+current_host+"/up?set="+datafu_set_folder+"&go="+datafu_go_folder+"'>up ( load )</a> \
                    </div> \
                    <div id='cc_buttons'> \
                        <button data-set_folder='"+datafu_set_folder+"' data-go_folder='"+datafu_go_folder+"' class='disabled buttall first' data-how='default_next' onclick='buttall_click(\""+datafu_go_folder+"\", \"default_next\", \""+datafu_set_folder+"\", \""+folder_number+"\")'>"+datafu_go_folder+"</button> \
                        <button data-set_folder='"+datafu_set_folder+"' data-go_folder='"+datafu_go_folder+"' class='buttall next' data-how='default_next_next' onclick='buttall_click(\""+datafu_go_folder+"\", \"default_next_next\", \""+datafu_set_folder+"\", \""+folder_number+"\")'>"+datafu_go_folder+"</button> \
                        <button data-set_folder='"+datafu_set_folder+"' data-go_folder='"+datafu_go_folder+"' class='buttall half' data-how='at_the_end' onclick='buttall_click(\""+datafu_go_folder+"\", \"at_the_end\", \""+datafu_set_folder+"\", \""+folder_number+"\")'>End</button> \
                        <button data-set_folder='"+datafu_set_folder+"' data-go_folder='"+datafu_go_folder+"' class='buttall_rm ok half' onclick='buttall_click_rm(\""+datafu_go_folder+"\", \"remove_all\", \""+datafu_set_folder+"\", \""+folder_number+"\")'>X</button> \
                        <input type='text' name='name' value='ln -sf ../../cc_sets/"+datafu_set_folder+"/"+datafu_go_folder+" "+datafu_go_folder+"__ln' /><br> \
                    </div> \
                </div>");
                if (o<1) { // LOCKER , redo load one time, corrige socket emit destination
                    o++;
                }
            })
        }, 800);
        
        $("#demo_cc").remove()
        $("#here_folders").before("<div id=\"demo_cc\"><iframe src=\"/cc?set="+param_set_fix+"&live=oui\"></iframe></div>")


    }, 500);
}

/// TODONOTWORKS
// socket.on("upload_end", function (data_filename) {
//     let fileurl ="/uploads/_up/"+data_filename;
//     $("#here_folders").find("div._up").first().before("<div class='_up'><button data-folder='_up'  data-filename='"+data_filename+"' data-fileurl='"+fileurl+"' ><img src='"+fileurl+"' alt="+data_filename+"></button></div>");
// });







// String.prototype.hashCode = function () {
// if (Array.prototype.reduce) {
//     return this.split("").reduce(function (a, b) {
//     a = (a << 5) - a + b.charCodeAt(0);
//     return a & a;
//     }, 0);
// }
// var hash = 0;
// if (this.length === 0) return hash;
// for (var i = 0; i < this.length; i++) {
//     var character = this.charCodeAt(i);
//     hash = (hash << 5) - hash + character;
//     hash = hash & hash; // Convert to 32bit integer
// }
// return hash;
// };

var u=0;
socket.on('ls-folders', function (datatype, data, directoryPath_error) {
    
    let datafix=data.toString().split(',');
    
    if ( $(".column").length == 0 ) {
        var compteur = 1;
        var datafix_length = datafix.length;
        datafix.forEach(ls_folder_function_each);
        // alert(datafix_length)

        // alert("1")
    } else {
        $("#here_folders").find("div").remove();
        datafix.forEach(ls_folder_function_each);
    }

    // alert(directoryPath_error)

    if (directoryPath_error=="oui") {
        $("html").addClass("error");
        $("#title_ls").append("<div>( ERROR, set n'existe pas )</div>")
    }

    
    function ls_folder_function_each(item, index, arr) {
        let data_tofix=arr[index];
        // RM /Sets/
        let datafix=data_tofix.toString()
           .replace(/^(.+)sets\//g, "");
        let data_filename=datafix.replace(/^(.+)\/([^\/]+)$/, "$2");
        let data_folder=datafix.replace(/^(.+)\/([^\/]+)$/, "$1"); 
        // alert(data_filename)
        // let hashme=data_filename.hashCode();
        // let fileurl ="/sets/"+data_folder+"/"+data_filename;     
        let fileurl = datafix
        // console.log("ls files == "+fileurl)   
        // console.log("before filtring "+hashme);
        // if ($("div[data-hash='"+hashme+"']").length==0) {
        // if ($("div[data-hash='']").length==0) {
            const elem_column = "<div data-hash='' class='"+data_folder+"' > \
                                    <button class='point_me'>\
                                    Pointer</button> \
                                    <button class='img' \
                                        data-folder='"+data_folder+"' \
                                        data-filename='"+data_filename+"' \
                                        data-fileurl='"+fileurl+"' > \
                                        <img class='ghost' src='"+fileurl+"'> \
                                        <div data-src='"+fileurl+"' alt='"+data_filename+"' title='"+data_filename+"' style='background:url("+fileurl+")'></div> \
                                        <span class='number_compteur'>"+compteur+"</span>  <input class='lsinput' value='"+data_filename+"' /> \
                                    </button> \
                                </div>"
            $("#here_folders").append(elem_column);
            

            
            // let thishash=$(this).data("hash");
            // console.log(thishash);
            // console.log(hashme);
        // }


    
        compteur++;
        // console.log(compteur)
        // alert(datafix_length)

        // if (compteur == datafix_length) {
            //            // alert("Fin de la boucle");
            // //
            //            // $('button[data-fileurl]').each(function() {
            //                // var fileUrl = $(this).data('fileurl');
            //                // $(this).find("img").attr('src', fileUrl);
            //            // });
            // var images = $('button[data-fileurl]');
            // var index = 0;

            // function traitementImage() {
            // if (index < images.length) {
            //     var fileUrl = $(images[index]).data('fileurl');
            //     var img = $(images[index]).find("img");
            //     img.attr('src', fileUrl);
            //     img.on('load', function() {
            //     index++;
            //     traitementImage();
            //     });
            // }
            // }

            // traitementImage();
                    
            
        // }



    } // function -- ls_folder_function_each


    /// REGROUPER PAR CLASS (de sous dossier) ... TODO, data/attr =/= class !
    // $(document).ready(function() {
    var $children = $("#here_folders").children("div").attr("data-noclass","wrapwrap");
    var classNames = $children.map(function() {
        return this.className;
    });
    classNames = $.unique(classNames.get());
    $.each(classNames, function(i, className) {
        $children.filter(function() {
            return $(this).hasClass(className);
        }).wrapAll("<div class='column' data-folder='"+className+"' />");
    });

    


    liste_refresh("start");


    setTimeout(function() { 
        $("button[data-folder]").each(function(e) {
            $(this).click(function() {

                if ($("html").hasClass("piano")) {

                    let data_folder = $(this).data("folder");
                    let data_filename = $(this).data("filename");
                    let fileurl = $(this).data("fileurl");
                    // alert("piano")

                    $(this).on('mousedown touchstart', function() {
                        socket.emit("ls-add-neww", data_folder, data_filename, fileurl, "piano", "press"); // EMIT
                    }).on('mouseup touchend', function() {
                        socket.emit("ls-add-neww", data_folder, data_filename, fileurl, "piano", "unpress"); // EMIT
                    });

                } else {
                    let data_folder = $(this).data("folder");
                    let data_filename = $(this).data("filename");
                    let fileurl = $(this).data("fileurl");
                    // alert(data_folder);
                    socket.emit("ls-add-neww", data_folder, data_filename, fileurl); // EMIT
                }
            });
        });

        $('.column[data-folder="column"]').first().addClass("ok");
        //// MOVE _UP IN FIRST depreciated --- why bypass order (now you can rename/access all folders)
        // $("#here_folders").find( '.column[data-folder$="/_up"]').first().insertBefore($("#here_folders").children().first());

        // var $children = $('.column[data-folder="column"]').children("[data-noclass='wrapwrap']");
        // $children.wrapAll("<div class='wrapwrap'></div>"); // Envelopper les éléments dans une div
    

    }, 1000);

    after_load_ls();

    // });
    
});


function liste_refresh(state) {
    // alert(state)
    
    // BASE64
    // $(".column").each(function(e) {
    //     var images = $(this).find('button div[data-fileurl]');
    //     var index = 0;

    //     function traitementImage() {
    //         if (index < images.length) {
    //             var fileUrl = $(images[index]).data('fileurl');
    //             var img = $(images[index]).find("div");

    //             // Convertir l'image en base32 avant de la charger
    //             fetch(fileUrl)
    //                 .then(response => {
    //                     if (!response.ok) {
    //                         throw new Error('Erreur de chargement'); // Vérifier si la réponse est correcte
    //                     }
    //                     return response.blob();
    //                 })
    //                 .then(blob => {
    //                     var imgElement = document.createElement('div');
    //                     imgElement.src = URL.createObjectURL(blob); // Créer un URL temporaire pour le blob

    //                     // Redimensionner l'image
    //                     return new Promise((resolve) => {
    //                         imgElement.onload = function() {
    //                             var canvas = document.createElement('canvas');
    //                             var ctx = canvas.getContext('2d');
    //                             var maxWidth = 200; // Largeur maximale
    //                             var maxHeight = 200; // Hauteur maximale
    //                             var ratio = Math.min(maxWidth / imgElement.width, maxHeight / imgElement.height);
    //                             canvas.width = imgElement.width * ratio;
    //                             canvas.height = imgElement.height * ratio;
    //                             ctx.drawImage(imgElement, 0, 0, canvas.width, canvas.height);
    //                             canvas.toBlob(function(resizedBlob) {
    //                                 var reader = new FileReader();
    //                                 reader.onloadend = function() {
    //                                     var base64data = reader.result;
    //                                     alert(canvas.height)
    //                                     img.css("background", "url("+base64data+")").css("height", canvas.height); // Définir la source de l'image en base64
    //                                     index++; // Incrémenter l'index
    //                                     traitementImage(); // Appeler pour l'image suivante
    //                                     resolve(); // Résoudre la promesse lorsque l'image est chargée
    //                                 };
    //                                 reader.readAsDataURL(resizedBlob); // Lire le blob redimensionné en tant que Data URL
    //                             }, 'image/jpeg', 0.5); // Format JPEG avec qualité 70%
    //                         };

    //                         imgElement.onerror = function() {
    //                             index++; // Passer à l'image suivante si l'image est corrompue
    //                             traitementImage(); // Appeler pour l'image suivante
    //                             resolve(); // Résoudre la promesse
    //                         };
    //                     });
    //                 })
    //                 .catch(() => {
    //                     index++; // Passer à l'image suivante en cas d'erreur
    //                     traitementImage(); // Appeler pour l'image suivante
    //                 });
    //         }
    //     }
    //     traitementImage();
        
    // });

    // SANS BASE64
    $(".column").each(function(e) {
        var images = $(this).find('button.img');
        var index = 0;

        function traitementImage() {
            if (index < images.length) {
                var fileurl = $(images[index]).data('fileurl');
                var img = $(images[index]).find("div");
                // alert(fileurl)
                img.css("background","url("+fileurl+")");


                // img.on('load', function() {
                //     index++; // Incrémenter l'index
                //     traitementImage(); // Appeler pour l'image suivante
                // });

                // img.on('error', function() {
                //     index++; // Passer à l'image suivante si l'image est corrompue
                //     traitementImage(); // Appeler pour l'image suivante
                // });

            }
        }

        if ( state == "refresh" ) {
            images.each(function() {
                var fileurl = $(this).data('fileurl');
                $(this).find("div").css( "background", "url("+fileurl+"?v=" + new Date().getTime() );
            });
        }
        traitementImage();
    });

}


function after_load_ls() {
    $('button.point_me').each(function() {
        let thisis = $(this);
        $(this).click(function() {
            if ( $(this).hasClass("exists_clicked") ) {
                let askurl = $(thisis).next("button").attr("data-fileurl");
                // alert(askurl)
                socket.emit("ls-point_exist", askurl, "back");
                console.log("Point == "+askurl)
                // alert("1")
           } else {
                let askurl = $(thisis).next("button").attr("data-fileurl");
                // alert(askurl)
                socket.emit("ls-point_exist", askurl, "point");
                console.log("GoPoint == "+askurl)
                // alert("2")
           }
       });
    });
}

socket.on('cc-point_response', function (askurl, response) {
    if (response == "exists") {

        $(".clicked_pointer").removeClass("clicked_pointer")
        $("[data-fileurl='"+askurl+"']").parent().addClass("exists_clicked").addClass("clicked_pointer");
        
        if ( !$("[data-fileurl='"+askurl+"']").parent().hasClass("clicked_go") ) {
            $("[data-fileurl='"+askurl+"']").prev("button").text("Pointée !")
        }

        setTimeout(() => {

            if ( $("[data-fileurl='"+askurl+"']").parent().hasClass("clicked_pointer")
             && !$("[data-fileurl='"+askurl+"']").parent().hasClass("clicked_go") ) {
                $("[data-fileurl='"+askurl+"']").parent().find(".point_me").text("Go ?")
                $("[data-fileurl='"+askurl+"']").parent().addClass("clicked_go") // LOCK
            } else {
                $("[data-fileurl='"+askurl+"']").prev("button").text("GO  !")
            }

        }, 500);


        // alert($("[data-fileurl='"+askurl+"']").parent(".exists_clicked").length)
        $("[data-fileurl='"+askurl+"']").parent(".exists_clicked").find(".point_me").on("click", function() {
            // alert("kkk")
            // EMIT
            socket.emit("ls-point_exist", askurl, "backback");

            
            $("#here_folders").find(".clicked_last").addClass("clicked_re clicked_rere")
            $("#here_folders").find(".clicked_re:not(.clicked_rere)").removeClass("clicked_re");
            $("#here_folders").find(".clicked_rere").removeClass("clicked_go").find(".point_me").text("Pointer")
            $("#here_folders").find(".clicked_rere").removeClass("clicked_rere");

            $("#here_folders").find(".clicked_last").removeClass("clicked_last");
            
            $(this).parent().addClass("clicked_last")

            

        })
        


    } else {
        
        $("[data-fileurl='"+askurl+"']").parent().addClass("red_not_exists");
        $("[data-fileurl='"+askurl+"']").prev("button").text("Pas chargée :(")
    }
});


socket.on('upload_end', function (data_fix_ok, context) {
    if (context == "broardcast") {
        // alert(data_fix_ok
        const data_folder = "/_up/";
        const data_filename = data_fix_ok;
        const fileurl = "/"+param_set_fix + data_folder + data_filename;
        // alert(fileurl);
        const elem_column = "<div data-hash='' class='"+data_folder+"'> \
            <button data-folder='"+data_folder+"' \
                data-filename='"+data_filename+"' \
                data-fileurl='"+fileurl+"' > \
                <img src='"+fileurl+"' alt='"+data_filename+"' title='"+data_filename+"'> \
                <input class='lsinput' value='"+data_filename+"' /> \
            </button> \
        </div>"
        // alert($("#here_folders").find('.column[data-folder$="/_up"]').length);
        setTimeout(() => {
            $("#here_folders").find('.column[data-folder$="/_up"]').find(".grp_buttons").after(elem_column)
        }, 500); // Délai de 500 ms

    }
});


socket.on('go_server_rm_img', function (data_filename, fileurl) {
    // alert(fileurl)
    $("button.img[data-fileurl='"+fileurl+"']").addClass("removed")
});