// var server_adresse = "http://192.168.1.21:3030";

const urlParams = new URLSearchParams(window.location.search);
const param_set = urlParams.get("set")
// const param_go = urlParams.get("go")

if (param_set){
  var param_set_fix=param_set.toString().replace("set=","")
  console.warn("!!!!! PARAM SET (param_set_fix) == "+param_set_fix)  
//   alert(param_set_fix)
} else {
  var param_set_fix="00_defaut"
  // alert(param_set_fix)
}


const param_go = urlParams.get("go");
var param_go_fix= (param_go) ? param_go : "_up";

if (!param_go) {
  const currentUrl = new URL(window.location.href.split('#')[0]);
  currentUrl.searchParams.set('go', "_up"); // Ajoute ou met à jour le paramètre 'param'
  window.location.href = currentUrl.href.replace(); // Recharge la page avec la nouvelle URL
}

$('#actual_folder').html(
  "<a href='/ls?set="+ param_set_fix + "'><span class='txt_clr set'>"+ param_set_fix + "</span></a> / <span class='txt_clr go'>"+param_go_fix+"</span> /"
)


let accentsTidy = function(s){
  var r=s.toLowerCase();
  r = s.replace(new RegExp("[àáâãäå]", 'g'),"a");
  r = r.replace(new RegExp("æ", 'g'),"ae");
  r = r.replace(new RegExp("ç", 'g'),"c");
  r = r.replace(new RegExp("[èéêë]", 'g'),"e");
  r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
  r = r.replace(new RegExp("ñ", 'g'),"n");                            
  r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
  r = r.replace(new RegExp("œ", 'g'),"oe");
  r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
  r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
  r = r.replace(new RegExp("[ÀÁÂÃÄÅ]", 'g'),"A");
  r = r.replace(new RegExp("[ÈÉÊË]", 'g'),"E");
  r = r.replace(new RegExp("[ÌÍÎÏ]", 'g'),"I");
  r = r.replace(new RegExp("[ÒÓÔÕÖ]", 'g'),"O");
  r = r.replace(new RegExp("[ÙÚÛÜ]", 'g'),"U");
  r = r.replace(new RegExp("[ÝŸ]", 'g'),"Y");
  return r;
};


$("html").attr("data-set_param", param_set_fix);

var ioo = io({ query: { set_is: param_set_fix, set_go: param_go_fix } })
// var socket = ioo.connect("/up");
var socket_default = io.connect("/up");


  ///
  /// FUNCTIONS
  /// 

  function pushit(ele){
    let elm_file=$(ele).attr("data-filename")
    // alert(elm_file)
    $("img[src='"+elm_file+"']").addClass("pop");
    setTimeout(function () {
      $("img[src='"+elm_file+"']").removeClass("pop");
    },1500);
  }
  
  function onerrorfunction(data_filename_fix) {
    // alert($(this).attr('src'))
    // alert(data_filename_fix)
    $('[data-filename="'+data_filename_fix+'"]').addClass("error_img")
    .find("button.add_button").first().append("<span class='img_error_mess'>ERROR :<br>"+data_filename_fix+"</span>")
    // var images = document.images;
    // for (var i=0; i<images.length; i++) {
    //     images[i].src = images[i].src.replace(/\btime=[^&]*/, 'time=' + new Date().getTime());
    // }
  };
  

const last_url_segment=window.location.href.substring(window.location.href.lastIndexOf('/') + 1).replace(/\?.*/,"")
const to_nsp="/"+last_url_segment // --> "/ls" / "/"" / "/cc"





function SortData(cible, attributs, orders) {


  setTimeout(function () {
    tinysort(cible,{attr: attributs},{order:orders});
  },200);

  // setTimeout(function () {

    
  //   const imgs_buttons_re = $("button.img");
  //   imgs_buttons_re.each(function() {

  //     $(".input_checkbox_re").prop("checked", false);
  //     $(this).on("click", function() {
  //       const input_checkbox_re = $(this).parent().find(".img_up_chooser");
        
  //       //// BUG QFIX after tinysort
  //       if ( ($(input_checkbox_re).prop("checked") >= 0) && (!$(input_checkbox_re).hasClass("toggle_fix_checked")) ) {
  //         // alert("-0")
  //         $(input_checkbox_re).prop("checked", true);
  //         $(input_checkbox_re).addClass("toggle_fix_checked");
  //       } else {
  //         $(input_checkbox_re).removeClass("toggle_fix_checked");
  //         $(input_checkbox_re).prop("checked", false);
  //       }

  //       if ($(input_checkbox_re).prop("checked")) {
  //           $(input_checkbox_re).prop("checked", true);
  //           // alert( $(input_checkbox_re).data("filename") + "is unchecked" );

  //         }
  //       else {
  //         // alert( $(input_checkbox_re).data("filename") + "is checked" );

  //         $(input_checkbox_re).prop("checked", false);

  //       }
  //     });
  //   });
  
    
  // },700);
  
} // SortData

function popup(message,time,ctime,bkg) {
    const popupElement = $("<div class='popup-message' style='background: "+bkg+"'>"+message+"</div>");
    $("body").prepend(popupElement);
    popupElement.hide().fadeIn(ctime).delay(time).fadeOut(ctime, function() {
      $(this).remove(); // Supprime l'élément après la disparition
    });
}





// function onerror_correct(fileurl, cond) { // not onerrorfunction
  // let errmess = "ERROR IMAGE == " +fileurl+ " ( "+cond+")"; socket_def, \""+fileurl+"\"ault.emit("up-error", errmess);
// }


$(document).ready(function() {

  // AUTOSTART INIT
  socket_default.emit("up-folders_pls", param_set_fix); //EMIT normal
  socket_default.emit("up-load_all_folders", param_set_fix); //EMIT // all
  console.log(param_set_fix +" + \n"+ param_go_fix +" + \n"+ to_nsp) 
  // up-folders_files -> serv => up_folder_files_data -> here
  socket_default.emit("up-folders_files", param_set_fix, param_go_fix, to_nsp); // EMIT up-folders_files
  // alert("up-folders_plz -- go ok");

  ///
  /// UPLOADER
  ///
  var uploader = new SocketIOFileUpload(socket_default);
  uploader.listenOnInput(
    document.getElementById("siofu_input")
  );

  const param_ok=param_set_fix
  
  const form = document.getElementById("form");
  form.addEventListener("submit", submitForm);
  function submitForm(e) {
      e.preventDefault();
  }

  // HASH UNIQUE ID
  String.prototype.hashCode = function () {
    if (Array.prototype.reduce) {
        return this.split("").reduce(function (a, b) {
        a = (a << 5) - a + b.charCodeAt(0);
        return a & a;
        }, 0);
    }
    var hash = 0;
    if (this.length === 0) return hash;
    for (var i = 0; i < this.length; i++) {
        var character = this.charCodeAt(i);
        hash = (hash << 5) - hash + character;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
  };


  socket_default.on("upload.start", function(data, exist) {
    // alert(exist)
    if (exist == 'exist_already') {
      let data_filename = data.data_name
      popup("EXISTE Déjà == "+data_filename, 3000, 400, 'orangered')
      return
    }
    let file_name = data.data_name;
    // alert(data.data_id);
    const progressDisplay = $("<div class='progress_display' id='num_"+data.data_id+"'><div class='file_name_progress'>"+file_name+"</div><br><div class='file_name_pourcent'>Avancée : "+data.percentage+"%</div></div>");
    $("#files").prepend(progressDisplay); 
  })

  socket_default.on("upload.progress", function(data) {
    console.log(data)
    // let file_name = data.data_name;
    const currfile = $("#num_"+data.data_id);
    currfile.find(".file_name_pourcent").html(`
        <div class='progress-container'>
            <div class='progress-bar' style='width: ${Math.floor(data.percentage)}%;'>`+data.percentage+`</div>
        </div>
    `);
    if (data.percentage >= 100) {
      currfile.remove();
    }
  })




  var compteur = 0

  socket_default.on("upload_end", function (data_filename, context, set, go) {





    console.log("--- upload_end --- " + context)

    let datafu_set_folder = param_set_fix;
    let datafu_go_folder  = param_go_fix;

    //// SET + GO == same here -- pseudoROOMS
    if ( (param_set_fix == set) && (param_go_fix == go) ) {
      console.log("OK MEME ORIGINE MEME SOUS_DOSSIER")
    } else {
      console.warn("PAS LA BONNE ROOM / SOUS_DOSSIER !")
      return
    }

    // alert("Here set == "+datafu_set_folder+" serv to ==> "+set+"\nHere go" +datafu_go_folder+" serv to ==> "+go)

    let hashme=data_filename.hashCode();
    
    var compteur = 1;
    let fileurl_fix ="/"+is_set+"/"+param_go_fix+"/"+data_filename;
    let data_folder = param_go_fix;
    var safe_fileurl = data_folder + "/" +param_go_fix+"/"+data_filename;
    let is_data_old_name = (typeof data_old_name !== 'undefined') ? "data-old_filename='"+data_old_name+"' " : " "; 
    

    if ( $(".column").length == 0 ) {
      // alert('init col')
      let folder_number     = 0;
      let className         = param_go_fix;
      const protocol        = window.location.protocol;
      const host            = window.location.host;
      const current_host    = protocol+"//"+host;

      $("#here_folders").append("<div class='grp_buttons' data-count='0'> \
        <div id='cc_links' data-set-folder='"+datafu_set_folder+"'> \
            <a class='go_url_href' href='"+current_host+"/cc?set="+datafu_set_folder+"&go="+datafu_go_folder+"&live=oui' target='chouquette'>"+datafu_go_folder+"</a> \
            <a class='go_url_href href_loop' href='"+current_host+"/cc?set="+datafu_set_folder+"&go="+datafu_go_folder+"&loop=yes&live=oui' target='chouquette'>( loop )</a> \
            <button class='img_up_chooser_button mv_btn open_modal' data-modal_open='mv_modal' data-set='"+datafu_set_folder+"' data-folder='"+datafu_go_folder+"'>Déplacer</button> \
            <button class='img_up_namebatch_btn open_modal namebatch_modal' data-modal_open='namebatch_modal' data-set='"+datafu_set_folder+"' data-folder='"+datafu_go_folder+"'>Renommer</button> \
          </div> \
        </div> \
        <div class='column ok' data-folder='"+className+"' />");

    } else {
        // alert('init col EXISTS')
        // $("#here_folders").find("div").remove();
        // datafix.forEach(ls_folder_function_each);
    }

    if (!is_set) {
      var is_set=param_set_fix
    }

    if ($("[hashme='"+hashme+"']").length < 1) {


      if (context != "dwl_file") {
        popup("FINIS == "+data_filename, 3000, 400, 'yellowgreen')
      }
      
      // alert(fileurl)
      let fileurl_fix ="/"+is_set+"/"+param_go_fix+"/"+data_filename;
      
      //// createButton //////////////////
      function createButton(hashme, data_filename, fileurl, isSolo) {
        // alert(data_filename + "d -- d"+ fileurl)
        let fileurl_fix =fileurl;
        // let data_filename_fix = data_filename;
        // let safe_fileurl= data_filename_fix.replace(/(.*)\/client\//g, "");

        let data_filename_fix = data_filename.replace(/(.*)\/sets\//g, "\/");
        const cond = "upload_end + FINIS"
        const buttonRM = "<button class='rm_image' data-filename='"+data_filename_fix+"' onclick='socket_default.emit(\"up_server_rm_img\", \"" + data_filename_fix + "\", \"" + data_filename_fix + "\", \"" + fileurl_fix + "\", \"SUPPRIME\")'>x</button>"
        const buttonHTML = "<div class=\"aa button_wrapper\" data-fileurl='"+fileurl_fix+"' data-filename='" + data_filename_fix + "'>" + buttonRM+ 
          "<button class='add_button' hashme='" + hashme + "' class='complete' data-folder='_up' data-filename='" + data_filename_fix + "' data-fileurl='" + fileurl_fix + "' \
          onclick='socket_default.emit(\"up-add-neww\", \"_up\", \"" + data_filename_fix + "\", \"" + fileurl_fix + "\", \"from_up_page\"); \
          pushit(this)'> \
          <img \
              onerror='socket_default.emit(\"up-error\", \"ERROR IMAGE == " +fileurl_fix+ " ( "+cond+" )\", \""+fileurl+"\");' \
              onload='this.className=\"loaded\"' \
              src='" + fileurl_fix + "' title=" + data_filename_fix + "> \
          </button></div>";
  
          var data_folder = param_set_fix;
          var safe_fileurl = data_folder + "/" +param_go_fix+"/"+data_filename;
          let compteur=0
          let data_filename_safe =   accentsTidy(data_filename) // NOT HERE --> TODO SERVER SIDE
          // let fileurl_fix = safe_fileurl
          let is_data_old_name = (typeof data_old_name !== 'undefined') ? "data-old_filename='"+data_old_name+"' " : " "; 
          // let fileurl = datafix
          // const buttonRM = "<button class='rm_image' data-filename='"+data_filename+"' onclick='socket_default.emit(\"up_server_rm_img\", \"" + data_filename + "\", \"" + data_filename + "\", \"" + fileurl + "\", \"SUPPRIME\")'>x</button>"
          const elem_column = "<div data-hash='"+hashme+"' class='colone_imgs fix "+data_folder+"' data-filename='"+data_filename+"'>" + buttonRM + "<button class='img' \
                                    data-folder='"+data_folder+"' \
                                    data-filename='"+data_filename+"' \
                                    data-fileurl='"+safe_fileurl+"' > \
                                    <img class='ghost' src='"+safe_fileurl+"' \
                                        onerror='socket_default.emit(\"up-error\", \"ERROR IMAGE == " +safe_fileurl+ " ( "+cond+" )\", \""+fileurl+"\");'> \
                                    <div data-src='"+safe_fileurl+"' alt='"+data_filename+"' title='"+data_filename+"' style='background:url("+fileurl+")'></div> \
                                </button> \
                                <input class='img_up_chooser' type='checkbox' \
                                    data-folder='"+data_folder+"' \
                                    data-filename='"+data_filename+"' \
                                    data-fileurl='"+safe_fileurl+"' > \
                                <span class='number_compteur'>"+compteur+"</span>  \
                                <form id='lsup_change_name'> \
                                  <input class='lsinput up' value='"+data_filename_safe+"' data-fileurl='"+safe_fileurl+"' data-folder='"+data_folder+"' pattern='[_\+\.a-zA-Z0-9À-Ÿà-ÿ]{1,}'/> \
                                  <button type='submit' class='submit_lsinput rename_btn' value='renommer'>Renommer </button> \
                                </form> \
                            </div>"
             

          // DEPRECIATED ( context Solo / Broadcast/any )
          /// ON AJOUTE AUX CLIENTS SI leur Set+Go correspond...
          //// Si client1->serveur->clients ont le meme Set+Go
          //// TODO équivalent des rooms !!

          // if (isSolo) {
          //   // $("._up").prepend(buttonHTML);
          //   $("#folders").prepend(buttonHTML);                
          //   if ($("#here_folders > [hashme='"+hashme+"']").length < 1) {                          
          //     $("#here_folders").find(".column").append(elem_column);
          //   } else {
          //     // alert("kkk 2")  
          //     /// LIST DWL --> HERE
          //   }
          // } else {
          //   // $("#folders").prepend(buttonHTML);
          //   $("#folders").prepend(buttonHTML);
          //   // alert('isSolo not')
          // }
          
          // alert(hashme)

          if ($("#folders > [hashme='"+hashme+"']").length < 1) {  
            $("#folders").prepend(buttonHTML);   
          } 
          if ($("#here_folders > [hashme='"+hashme+"']").length < 1) {                          
            $("#here_folders").find(".column").append(elem_column);
          } else {
            alert('déja envoyée (existe)')
            popup("déja envoyée (existe)", 1000, 100, "red")
          }


      } //// createButton
    
      // DEPRECIATED ( context Solo / Broadcast/any )
      // Utilisation de la fonction
      // if (context == "solo") {
        createButton(hashme, data_filename, fileurl_fix, true); // TRUE == isSolo 
        // alert( "context == solo" )
      // } else {
        // createButton(hashme, data_filename, fileurl_fix, false); // FALSE != isSolo
        // alert( "context != solo" )
      // }
      //// createButton //////////////////



      setTimeout(function () {
            
        SortData('.colone_imgs','data-filename','asc')

        const imgs_buttons_re = $("button.img");
        imgs_buttons_re.each(function() {

          $(".input_checkbox_re").prop("checked", false);
          $(this).on("click", function() {
            const input_checkbox_re = $(this).parent().find(".img_up_chooser");
            
            //// BUG QFIX after tinysort
            if ( ($(input_checkbox_re).prop("checked") >= 0) && (!$(input_checkbox_re).hasClass("toggle_fix_checked")) ) {
              // alert("-0")
              $(input_checkbox_re).prop("checked", true);
              $(input_checkbox_re).addClass("toggle_fix_checked");
            } else {
              $(input_checkbox_re).removeClass("toggle_fix_checked");
              $(input_checkbox_re).prop("checked", false);
            }

            if ($(input_checkbox_re).prop("checked")) {
                $(input_checkbox_re).prop("checked", true);
                // alert( $(input_checkbox_re).data("filename") + "is unchecked" );

              }
            else {
              // alert( $(input_checkbox_re).data("filename") + "is checked" );

              $(input_checkbox_re).prop("checked", false);

            }
          });
        });
      
        
      },800);


      
            

    } else { // HASHME
      // alert(data_filename)
      // alert('hash exists')
      // NOT FIRST SHOOT
    } // hashme

    // const overlay = document.querySelector(".overlay");
    closeModal();
    popup("TELECHARGEMENTS FINIS", 2000, 100, "yellow")

    
    // $("#here_folders_title").remove()
    // $("#here_folders_title.empty").remove()

    // QFIX RELOAD PAGE ==> DISPLAY #here_folders !
    // window.location.href = window.location.href; 


    
  });




  socket_default.on("up_re_namebatch_done", function (data_old_name, data_filename, fileurl, condition) {
    popup("RENOMMAGE TERMINÉ -- la page va recharger !", 2000, 100, "cyan")
    
    const currentUrl = new URL(window.location.href.split('#')[0]);
    // window.history.pushState({ path: currentUrl.href }, '', currentUrl.href);
    // window.location.href = currentUrl.href.replace(); // Recharge la page avec la nouvelle URL  
    setTimeout(function () {
      window.location.href = currentUrl.href.replace(); // Recharge la page avec la nouvelle URL
    },3000);

    
  });


  /// POPUPS from serveur
  socket_default.on("go_server_popup", function (message, time, ctime, bkg, cond, set, go) {
    
    //// SET + GO == same here -- pseudoROOMS
    if ( (param_set_fix == set) && (param_go_fix == go) ) {
      popup(message, time, ctime, bkg)

      // if (cond == "REGEX_MATCH") {
      //   popup(message, time, ctime, bkg)
      //   // alert(fileurl)
      //   return
      // }
      if (cond == "CLOSE_MODAL") {
        // popup(message, time, ctime, bkg)
        closeModal();
        // alert(fileurl)
        // return
      }



    } /// SAME ROOM

  }); // go_server_popup


  socket_default.on("go_server_rm_img", function (data_old_name, data_filename, fileurl, condition, set, go) {

    //// SET + GO == same here -- pseudoROOMS
    if ( (param_set_fix == set) && (param_go_fix == go) ) {

      // alert("OK GO")

      let condition_fix = (condition!="") ? condition : "SUPPRIME";
      let data_filename_name = data_filename.split('/').pop(); // Obtenir le nom de fichier (basename)


      // alert(data_old_name)
      //// RENAME
      if (condition_fix == "exists") {
        let cible = $("#here_folders").find("button[data-filename='"+data_filename_name+"']");      

        var condition_fix_mess = "EXISTS !"
        cible.append("<span class='hidden rm_confirm "+condition_fix+"'>"+condition_fix_mess+"</span>")

        popup("EXISTE DEJA",1000,200,"cyan")

        alert("existe déjà, touver un autre nom !")
        /// ... RENAME END HERE !!!
        return
      } 
      
      //// ... NORMAL == REMOVE !!!
      // alert(condition_fix)
      // let data_old_name = (data_old_name!="") ? data_old_name : data_filename;
      // $("._up").find("button[data-filename='"+data_filename+"']").remove()
      // $("#here_folders").find("button[data-filename='"+data_filename_name+"']").remove()
      // $("#folders").find(".button_wrapper[data-filename='"+data_filename+"']").remove()
      
      // QFIX add "/"
      // alert("data_filename == "+data_filename)
      let buttonwrapper_folders = $("#folders").find(".button_wrapper[data-filename='/"+data_filename+"']")
      let buttonwrapper_folders_alt = $("#folders").find(".button_wrapper[data-filename='"+data_filename+"']")
      // let count_check_alert = buttonwrapper_folders.length
      // let count_check_alert_alt = buttonwrapper_folders_alt.length
      // alert(count_check_alert+" \n=== data_filename = "+data_filename+"\ncount_check_alert_alt == "+count_check_alert_alt)
      // $("#folders").find(".button_wrapper[data-filename='"+data_filename+"']").remove()
      $(buttonwrapper_folders).remove();
      $(buttonwrapper_folders_alt).remove();

      // $("#here_folders").find(".button_wrapper[data-filename='"+data_filename_name+"']").remove()
      $("#here_folders").find(".colone_imgs[data-filename='"+data_filename_name+"']").remove()
      $("#here_folders").find(".colone_imgs[data-filename='"+data_old_name+"']").remove()
      $("#here_folders").find("div[data-filename='"+data_old_name+"']").append("<span class='rm_confirm "+condition_fix+"'>"+condition_fix+"</span>")




      if (condition_fix != "SUPPRIME") {
        var data_folder = param_set_fix;
        var safe_fileurl = data_folder + "/" +param_go_fix+"/"+data_filename;
        let compteur=0
        let data_filename_safe =   accentsTidy(data_filename)

        let hashme=data_filename_safe.hashCode();

        // let fileurl = datafix
        const cond = "go_server_rm_img + SUPPRIMER"
        const buttonRM = "<button class='rm_image' data-filename='"+data_filename+"' onclick='socket_default.emit(\"up_server_rm_img\", \"" + data_filename + "\", \"" + data_filename + "\", \"" + fileurl + "\", \"SUPPRIME\")'>x</button>"
        const elem_column = "<div data-hash='"+hashme+"' data-old_filename='"+data_old_name+"'  class='colone_imgs fix "+data_folder+"' data-filename='"+data_filename+"'>" + buttonRM + "<button class='img' \
                                    data-folder='"+data_folder+"' \
                                  data-filename='"+data_filename+"' \
                                  data-fileurl='"+safe_fileurl+"' > \
                                  <img class='ghost' src='"+safe_fileurl+"' \
                                        onerror='socket_default.emit(\"up-error\", \"ERROR IMAGE == " +fileurl+ " ( "+cond+" )\", \""+fileurl+"\");'> \
                                  <div data-src='"+safe_fileurl+"' alt='"+data_filename+"' title='"+data_filename+"' style='background:url("+fileurl+")'></div> \
                              </button> \
                              <input class='img_up_chooser' type='checkbox' \
                                  data-folder='"+data_folder+"' \
                                  data-filename='"+data_filename+"' \
                                  data-fileurl='"+safe_fileurl+"' > \
                              <span class='number_compteur'>"+compteur+"</span>  \
                              <form id='lsup_change_name'> \
                                <input class='lsinput up' value='"+data_filename_safe+"' data-fileurl='"+safe_fileurl+"' data-folder='"+data_folder+"' pattern='[_\+\.a-zA-Z0-9À-Ÿà-ÿ]{1,}'/> \
                                <button type='submit' class='submit_lsinput rename_btn' value='renommer'>Renommer </button> \
                              </form> \
                          </div>"
          
        $("#here_folders").find(".column").append(elem_column);
        // data-old_filename

        // alert(condition_fix)
        const expr = condition_fix;
        switch (expr) {
          case 'rename':
            var condition_fix_mess = "Renommée ! <br> <span class='trunc_wrap'>"+data_old_name+"</span> <b>-> <span class='trunc_wrap'>"+data_filename+"</span></b>"
            break;
          case 'SUPPRIME':
            console.log('Supprimée');
            break;
          default:
            console.log(`Sorry, we are out of ${expr}.`);
        }

        $("#here_folders").find("div[data-old_filename='"+data_old_name+"']").append("<span class='hidden rm_confirm "+condition_fix+"'>"+condition_fix_mess+"</span>")
        
        let rm_mess = $("#here_folders").find("div[data-old_filename='"+data_old_name+"']").find(".rm_confirm")
        let delay_timeout = 50;
        var delay_hide = 1000;
        var delay_fade_out = 400;
        

        setTimeout(function () {
          rm_mess.hide().removeClass("hidden").delay(200).fadeIn(300).delay(delay_hide).fadeOut(delay_fade_out, function() {
              $(this).remove(); 
          });
        },delay_timeout);



        

        let total_delay = Number(delay_timeout + delay_hide + delay_fade_out);
        // alert("total_delay == "+total_delay)

        // Function to sort Data
        setTimeout(function () {
          SortData('.colone_imgs','data-filename','asc')
        },1000);

        // let count_here_folders = $("#here_folders").find(".button_wrapper").length;
        // let count_folders = $("#folders").find(".button_wrapper").length;
        // if (count_here_folders <= 0) {
        //   alert("count_here_folders vide")
        // }
        // if (count_folders <= 0) {
        //   alert("count_folders vide")
        // }

      }
    } //// SET + GO == same here


  }); //// go_server_rm_img



  // alert($('#ls_link').attr("href"));
  
  // alert($('#ls_link').attr("href"));
  if (param_go) {
    var set_and_go = param_set+"&go="+param_go;
  } else {
    var set_and_go = param_set;
  }

  $('#ls_link').attr("href","/ls?set="+set_and_go);
  $('#cc_link').attr("href","/cc?set="+set_and_go+"&loop=oui");
  // $("#cc_link").attr("href", "/cc?set="+param_ok)
  $("#cc_link").attr("href", "/cc?set="+set_and_go+"&loop=oui&live=oui").attr("target", "CC")
  // $("#cc_loop").attr("href", "/cc?set="+param_ok+"&go=_up&loop=yes")
  // $("#ls_link").attr("href", "/ls?set="+param_ok)

  
  /// EMIT
  $("#dwl_list_submit").on("click", function() {
    // const dwlContent = $("#dwl_list").val(); 
    const dwlContent = $("#dwl_list").val().split('\n').filter(line => line.trim() !== '').join('\n'); 
    socket_default.emit("up_dwl_list", dwlContent, param_set, param_go); // EMIT
  });
  

  // if  (typeof data_old_name !== 'undefined') {
  //   $("#here_folders").find("div[data-old_filename='"+data_old_name+"']").removeAttr("data-old_filename")

  // }



      

  

  SortData('.colone_imgs','data-filename','asc')
  // tinysort('.column.ok',{selector:'.colone_imgs',attr:'data-filename'});





// sessionStorage.SessionName = "SessionData"
// sessionStorage.getItem("SessionName")
// sessionStorage.setItem("SessionName","SessionData");


}); // READY






var t=0
socket_default.on("up-folders", function(type, data_folder, data_filename, set_is) { // data_filename == result
    
  // alert("up-folders")
  
  // let hashme=data_filename.hashCode();
    if (param_set_fix == set_is ) {
    // if ($("[hashme='"+hashme+"']").length < 1) {
    // alert("upppp -- up-folders type = "+type+" - hash = "+hashme)
    
    // SI le serveur n'est pas chargé durant l'ajout de fichiers (OFF)
    // alors les fichiers non renomés/filtrés/regex/match...
    // vont poser pbl... du coup on va les corriger (coté serveur)
    // QF ==== MV RENAME 4
    // ICI on les rename ... coté client 
    // le temps que le serveur fasse le boulot
    console.warn("up-folders type = "+type)
    let fileurl ="/"+data_folder+"/";     
    // let data_filename_fix = data_filename;
    let safe_fileurl= data_filename.replace(/(.*)\/sets\//g, "");
    // console.log(data_filename)
    // hashme='"+hashme+"' 
    // alert(safe_fileurl)

    let data_filename_safe = accentsTidy(data_filename)
    let hashme=data_filename_safe.hashCode();


    let data_filename_fix = data_filename.replace(/(.*)\/sets\//g, "\/");
    // socket.on("up_server_rm_img", function (data_old_name_1, data_filename_2, fileurl_3, context_4) {
    
    const cond = "up-folders";
    const buttonRM = "<button class='rm_image' data-filename='"+data_filename_fix+"' onclick='socket_default.emit(\"up_server_rm_img\", \"" + data_filename_fix + "\", \"" + data_filename_fix + "\", \"" + safe_fileurl + "\", \"SUPPRIME\")'>x</button>" // 2
    const buttonHTML = "<div class=\"bb button_wrapper\" data-fileurl='"+safe_fileurl+"' data-filename='" + data_filename_fix + "'>" + buttonRM + 
                  "<button class='add_button' hashme='"+hashme+"' data-folder='_up' data-filename='"+data_filename_fix+"' data-fileurl='"+fileurl+"' \
                  onclick='socket_default.emit(\"up-add-neww\", \"_up\", \""+safe_fileurl+"\", \""+safe_fileurl+"\", \"from_up_page\"); \
                            pushit(this)'> \
                            <img \
                                onerror='socket_default.emit(\"up-error\", \"ERROR IMAGE == " +data_filename+ " ( "+cond+" )\", \""+data_filename+"\");' \
                                onload='this.className=\"loaded\"' \
                                src='"+safe_fileurl+"'></button></div>"
    
    let up_images=(buttonHTML)
    $("#folders").prepend(up_images)

    setTimeout(function () {
      SortData('.colone_imgs','data-filename','asc')
    },1000);

    

    // if ($("#here_folders").length == 0) {  
  
      let compteur=0
      // let fileurl = datafix


      const elem_column = "<div data-hash='"+hashme+" class='colone_imgs fa "+data_folder+"' data-filename='"+data_filename+"'>" + buttonRM + "<button class='img' \
                                  data-folder='"+data_folder+"' \
                                  data-filename='"+data_filename+"' \
                                  data-fileurl='"+safe_fileurl+"' > \
                                  <img class='ghost' src='"+safe_fileurl+"'> \
                                  <div data-src='"+safe_fileurl+"' alt='"+data_filename+"' title='"+data_filename+"' style='background:url("+fileurl+")'></div> \
                              </button> \
                              <input class='img_up_chooser' type='checkbox' \
                                  data-folder='"+data_folder+"' \
                                  data-filename='"+data_filename+"' \
                                  data-fileurl='"+safe_fileurl+"' > \
                              <span class='number_compteur'>"+compteur+"</span>  \
                              <form id='lsup_change_name'> \
                                <input class='lsinput up' value='"+data_filename_safe+"' data-fileurl='"+safe_fileurl+"' data-folder='"+data_folder+"' pattern='[_\+\.a-zA-Z0-9À-Ÿà-ÿ]{1,}'/> \
                                <button type='submit' class='submit_lsinput rename_btn' value='renommer'>Renommer </button> \
                              </form> \
                          </div>"
                         
                          

      if ($("[hashme='"+hashme+"']").length < 1) {                          
        $("#here_folders").find(".column").append(elem_column);
      } else {
        // alert("kkk")  
      }

          
    // }
    


    if (t<1) {
      t++;
      $("#folders").before('<h1 id="old_files" class="big second">Derniers ajouts ( par date d\'envoi ) :</h1> \
        <input type="checkbox" id="toggle_folders_up"><label for="toggle_folders_up"></label>')


      // Ajouter un gestionnaire d'événements pour la case à cocher
      $("#toggle_folders_up").on("change", function() {
        localStorage.setItem("toggle_folders_up", this.checked); // Enregistre l'état dans localStorage
        // alert("changed")
      });
    

      const toggleState = localStorage.getItem("toggle_folders_up") === 'true'; // Récupère l'état
      $("#toggle_folders_up").prop("checked", toggleState); // Restaure l'état de la case à cocher
      
    
    
    }
  // } // hashme
  } else {
      // alert(param_set_fix+" != ( !!!! ) "+set_is)
  }




}) // up-folders



socket_default.on('load_all_folders', function (result, set_folder) { // GET_F OLDERS
  // arrayOfFiles.forEach(function (get_folder_files) {
  // console.log(result);

  let count_opt=0
  let is_active_up = ("_up" == param_go) ? ' selected' : ''
  const dropdown = $("<select id='resultDropdown'></select>");
  
  // dropdown.append(`<option value='_up' class='`+is_active_up+`'`+is_active_up+`>_up (${count_opt})</option>`);  

  var nouveau_dejacree = false

  result.forEach(item => {
    count_opt++

    // if (item.fixname_folder != "_up") {
      // name (path),  index,  fixname_folder (name)
      let is_active_go = (item.fixname_folder == param_go) ? ' selected' : ''
      let is_active_sep = (item.fixname_folder == param_go) ? '<--- ' : ''
      // alert(is_active_go)
      /// SI NOUVEAU EXISTE ne pas afficher
      if (item.fixname_folder == "NOUVEAU") {
        // alert("NOUVEAU folder exists")
        // alert(item.fixname_folder)
        $("body").addClass("nouveau_exists")
      }
      dropdown.append(`<option value='${item.fixname_folder}' class='sel${is_active_go}' ${is_active_go}>${item.fixname_folder} ··· (${count_opt}) ${is_active_sep}</option>`);
    // }
  });

  /// SI NOUVEAU EXISTE ne pas afficher
  /// SI NOUVEAU Dossier en cours
  let body_nouveau_exists = $("body").hasClass("nouveau_exists");
  if ( (param_go_fix != "NOUVEAU") && (!body_nouveau_exists) ) {
    dropdown.append(`<option value='NOUVEAU'>--- NOUVEAU ---</option>`); 
  }

  $("#dropdownContainer").find("#empty_select").remove()
  $("#dropdownContainer").append(dropdown);
  $("#dropdownContainer").append("<span id='help_dropdown'><<--- Changer de sous-dossier ( <span class='txt_clr go'>sub_go</span> )</span>")


  $("#resultDropdown").on('change', function() {

    let selectedValue_destination = $("#resultDropdown").val()
    if ( selectedValue_destination == "NOUVEAU" ) { 
      // alert(selectedValue_destination)
      let newfolder_fix ="/"+param_set_fix+"/NOUVEAU/";
      socket_default.emit("up_server_newfolder_set", newfolder_fix)
      // var timer = 100
    // } else {
      // var timer = 0
    }

    // setTimeout(function () {
      const selectedValue = $(this).val();
      const currentUrl = new URL(window.location.href.split('#')[0]);
      currentUrl.searchParams.set('go', selectedValue); // Ajoute ou met à jour le paramètre 'param'
      window.history.pushState({ path: currentUrl.href }, '', currentUrl.href);
      window.location.href = currentUrl.href.replace(); // Recharge la page avec la nouvelle URL
    // },timer);

    

  });


  const curr_selected = $("#dropdownContainer").find(".selected")
  // const curr_selvalue = curr_selected.attr("value")
  const prev_selected = curr_selected.prev("option").attr("value")
  // const prev_selvalue = prev_selected.attr("value")
  const next_selected = curr_selected.next("option").attr("value")
  // alert(next_selected)

  const first_sel = $("#dropdownContainer").find("option").first().attr("value");
  const last_sele = $("#dropdownContainer").find("option").last().attr("value");

  const isnext_select = (next_selected === undefined) ? "loop" : "exists"
  const loop_next = (isnext_select == "exists") ? next_selected : first_sel;
  
  const isprev_select = (prev_selected === undefined) ? "loop" : "exists"
  const loop_prev = (isprev_select == "exists") ? prev_selected : last_sele;

  let div_go_next = "<div id='go_next' class='gogo_nextprev' data-loop='"+isnext_select+"'><a href='/up?set="+param_set_fix+"&go="+loop_next+"' title='"+loop_next+"'>suiv.</a></div>"
  let div_go_prev = "<div id='go_prev' class='gogo_nextprev' data-loop='"+isprev_select+"'><a href='/up?set="+param_set_fix+"&go="+loop_prev+"' title='"+loop_prev+"'>prec.</a></div>"
  let div_curpath = $("#actual_folder").clone();
  $("body").append("<div id='nav_folders'>"+ div_go_prev+ "<div id='div_curpath'></div>" + div_go_next +"</div>")
  $("#div_curpath").html(div_curpath)


}); // Soket on -- load_all_folders



    
 


var u=0;
/// TODO DEPRACIATED ? in use ?
socket_default.on('up_folder_files_data', function (datatype, data, directoryPath_error) {
  
  
  // alert(data)    
  
  if (data.length > 0) {
    let datafix=data.toString().split(',');
  
    function ls_folder_function_each(item, index, arr) {
      let data_tofix=arr[index];
      // RM /Sets/
      // alert(item)
      let datafix=item.toString()
                            .replace(/^(.+)sets\//g, "");

      let data_filename=datafix.replace(/^(.+)\/([^\/]+)$/, "$2");
      let data_folder=datafix.replace(/^(.+)\/([^\/]+)$/, "$1"); 

      let data_filename_fix_go = data_folder + "/" + data_filename;
      let data_filename_fix_go_newname = data_folder + "/" + data_filename;
      // alert(data_filename_fix_go);

      let safe_fileurl = data_filename_fix_go.replace(/(.*)\/client\//g, "");
      const buttonRM = "<button class='rm_image' data-filename='"+data_filename_fix_go+"' onclick='socket_default.emit(\"up_server_rm_img\", \"" + "/" + data_filename_fix_go + "\", \"" + safe_fileurl + "\", \"" + safe_fileurl + "\", \"SUPPRIME\")'>x</button>" // 3

      if (datatype == "popup") {
        popup(data_filename, 500, 0, "yellowgreen")
      }

      // let safe_fileurl_newname = data_filename_fix_go_newname.replace(/(.*)\/clien
      /// ! NE PAS DE MANIPULATION DE NOMS ICI : coté serveur cf. up-folders_files
      // let data_filename_safe = accentsTidy(data_filename)
      let data_filename_safe = data_filename;
      let fileurl = datafix
      let hashme=data_filename.hashCode();

      const elem_column = "<div data-hash='"+hashme+"' class='colone_imgs ba "+data_folder+"' data-filename='"+data_filename+"'>" + buttonRM + "<button class='img' \
                                  data-folder='"+data_folder+"' \
                                  data-filename='"+data_filename+"' \
                                  data-fileurl='"+fileurl+"' > \
                                  <img class='ghost' src='"+fileurl+"'> \
                                  <div data-src='"+fileurl+"' alt='"+data_filename+"' title='"+data_filename+"' style='background:url("+fileurl+")'></div> \
                              </button> \
                              <input class='img_up_chooser' type='checkbox' \
                                  data-folder='"+data_folder+"' \
                                  data-filename='"+data_filename+"' \
                                  data-fileurl='"+fileurl+"' > \
                              <span class='number_compteur'>"+compteur+"</span>  \
                              <form id='lsup_change_name'> \
                                <input class='lsinput up' value='"+data_filename_safe+"' data-fileurl='"+safe_fileurl+"' data-folder='"+data_folder+"' pattern='[_\+\.a-zA-Z0-9À-Ÿà-ÿ]{1,}'/> \
                                <button type='submit' class='submit_lsinput rename_btn' value='renommer'>Renommer </button> \
                              </form> \
                          </div>"
                          

      // alert("here_folders + up_folder_files_data")
      
      if ($("[hashme='"+hashme+"']").length < 1) {                          
        $("#here_folders").find(".column").append(elem_column);
      } else {
        // alert("kkk 1")  
      }
      
      compteur++;

    } // function -- ls_folder_function_each

    // ls_folder_function_each(datafix);
    datafix.forEach(ls_folder_function_each);

    if(datafix=="") {
           $("#here_folders_title").before("<div id='empty_imgs' class='empty'>Dossier vide !</div>")
    } 

    if ( $(".column").length == 0 ) {
      let folder_number     = 0;
      let datafu_set_folder = param_set_fix;
      let datafu_go_folder  = param_go_fix;
      let className         = param_go_fix;
      const protocol = window.location.protocol;
      const host = window.location.host;
      const current_host=protocol+"//"+host;

      $("#here_folders").append("<div class='grp_buttons' data-count='0'> \
          <div id='cc_links' data-set-folder='"+datafu_set_folder+"'> \
              <a class='go_url_href' href='"+current_host+"/cc?set="+datafu_set_folder+"&go="+datafu_go_folder+"&live=oui' target='chouquette'>"+datafu_go_folder+"</a> \
              <a class='go_url_href href_loop' href='"+current_host+"/cc?set="+datafu_set_folder+"&go="+datafu_go_folder+"&loop=yes&live=oui' target='chouquette'>( loop )</a> \
              <button class='img_up_chooser_button mv_btn open_modal' data-modal_open='mv_modal' data-set='"+datafu_set_folder+"' data-folder='"+datafu_go_folder+"'>Déplacer</button> \
              <button class='img_up_namebatch_btn open_modal namebatch_modal' data-modal_open='namebatch_modal' data-set='"+datafu_set_folder+"' data-folder='"+datafu_go_folder+"'>Renommer</button> \
          </div> \
      </div> \
      <div class='column ok' data-folder='"+className+"' />");

      var compteur = 1;
      datafix.forEach(ls_folder_function_each);

    } else {

        $("#here_folders").find("div").remove();
        datafix.forEach(ls_folder_function_each);

    } // ? column

    if (directoryPath_error=="oui") {
        $("html").addClass("error");
        $("#title_ls").append("<div>( ERROR, set n'existe pas )</div>")
    }

    // CLOSE .column ok + form#lsup_change_name
    $("#here_folders").append("</div>");

    // CLIQUER SUR les boutton.img => checkbox -> toggle
    const imgs_buttons = $("button.img");
    imgs_buttons.each(function() {
      $(this).on("click", function() {
        const input_checkbox = $(this).parent().find(".img_up_chooser");

        if ($(input_checkbox).prop("checked")) {
            // alert( $(input_checkbox).data("filename") + "is checked" );
            $(input_checkbox).prop("checked", false);
        }
        else {
          // alert( $(input_checkbox).data("filename") + "is unchecked" );
          $(input_checkbox).prop("checked", true);
        }
      });
    }); // imgs_buttons

  

    $("#here_folders").on("submit", "#lsup_change_name", function(e) {
      e.preventDefault();
      const inputElement = $(this).find(".lsinput.up"); // Récupère l'élément input
      const inputValue = inputElement.val(); // Récupère la valeur de l'input
      const fileurl = inputElement.data("fileurl"); // Récupère la valeur de data_data_filename_fix_go
      // const safeFileUrl = inputElement.data("safe_fileurl"); // Récupère la valeur de data_safe_fileurl
      const datafolder = inputElement.data("folder"); // Récupère la valeur de data_safe_fileurl_newname
      const fileurl_newname = datafolder + "/" + inputElement.val();

      // alert("Valeur = data_filename : " + inputValue + "\n" +
      //       "fileurl : " + fileurl + "\n" +
      //       "data-folder : " + datafolder + "\n" + 
      //       "fileurl_newname => " + fileurl_newname.toString() + "\n" +
      //       "param_go_fix : " +param_go_fix 
      //     ); // Affiche les valeurs

      let data_filename_val = inputValue.replace("%","_");
      let data_filename_encode = encodeURI(data_filename_val)
      let data_filename_no_accent = accentsTidy(data_filename_encode);
      let data_filename = data_filename_no_accent;

      let data_old_name = fileurl.split('/').pop(); // Obtenir le nom de fichier (basename)
                                                  // data_old_name, data_filename, fileurl, fileurl_newname, param_go_fix, context
                                                  // data_old_name, data_filename, fileurl, fileurl_newname, param_go_fix, context
                                                  
      socket_default.emit("up_server_rename_img", data_old_name, data_filename, fileurl, fileurl_newname.toString(), param_go_fix, param_set_fix, "y")

      
    }); //here_folders
  
  }

  // NEW FOLDER --- SET
  $("#newfolderContainer_set").on("submit", "#newfolder_form_set", function(e) {
    e.preventDefault();
    let newfolder_form_val_set = $("#newfolder_name_set").val();
    let newfolder_fix ="/"+newfolder_form_val_set+"/";
    // alert("newfolder_form_val_set == "+newfolder_form_val_set);
    // alert("newfolder_fix = "+newfolder_fix)
    
    if (newfolder_form_val_set != ""){
      // alert(newfolder_fix)
      socket_default.emit("up_server_newfolder_set", newfolder_fix);
      // RELOAD page -> new folder
      const selectedValue = newfolder_form_val_set;
      const currentUrl = new URL(window.location.href);
      currentUrl.searchParams.set('set', selectedValue); // Ajoute ou met à jour le paramètre 'go'
      currentUrl.searchParams.set('go', "_up"); // Suppression du paramètre "set"
      
      window.history.pushState({ path: currentUrl.href }, '', currentUrl.href);
      window.location.href = currentUrl.href; // Recharge la page avec la nouvelle URL
    } else {
      alert("Indiquer un nom de dossier")
    }

  }); /// newfolderContainer


  // NEW FOLDER --- GO
  $("#newfolderContainer").on("submit", "#newfolder_form", function(e) {
    e.preventDefault();
    let newfolder_form_val = $("#newfolder_name").val();
    let newfolder_fix ="/"+param_set_fix+"/"+newfolder_form_val;
    // alert("newfolder_form_val == "+newfolder_form_val);
    // alert("newfolder_fix = "+newfolder_fix)

    if (newfolder_form_val != ""){
      socket_default.emit("up_server_newfolder", newfolder_fix);
      // RELOAD page -> new folder
      const selectedValue = newfolder_form_val;
      const currentUrl = new URL(window.location.href);
      currentUrl.searchParams.set('go', selectedValue); // Ajoute ou met à jour le paramètre 'param'
      window.history.pushState({ path: currentUrl.href }, '', currentUrl.href);
      window.location.href = currentUrl.href; // Recharge la page avec la nouvelle URL
    } else {
      alert("Indiquer un nom de dossier")
    }

  }); /// newfolderContainer


  $("#renamefolderContainer").on("submit", "#renamefolder_form", function(e) {
    e.preventDefault();
    let newfolder_form_val = $("#renamefolder_name").val();
    let newfolder_fix ="/"+param_set_fix+"/"+newfolder_form_val+"/";
    let final_olddir ="/"+param_set_fix+"/"+param_go_fix+"/"; 
    // console.log("newfolder_fix == "+newfolder_fix);
    // console.log("final_olddir == "+final_olddir)

    if (newfolder_form_val != ""){
      socket_default.emit("up_server_renamefolder", newfolder_fix, final_olddir);

      const selectedValue = newfolder_form_val;
      const currentUrl = new URL(window.location.href);
      currentUrl.searchParams.set('go', selectedValue); // Ajoute ou met à jour le paramètre 'param'
      window.history.pushState({ path: currentUrl.href }, '', currentUrl.href);
      window.location.href = currentUrl.href; // Recharge la page avec la nouvelle URL
    } else {
      alert("Indiquer un nom de dossier")
    }

  }); /// renamefolderContainer



  // alert("root _up_folder_files_data")

  /// ACTION -- modal_open    
  /// OPEN MODULAR popup based on ... data-modal_open
  $(".open_modal[data-modal_open]").each(function() {
    $(this).on("click", function() {
      const modal_open = $(this).attr("data-modal_open");
      console.log("openModal_pls")
      openModal_pls(modal_open);
    });
    // Appel de la fonction openModal_pls uniquement lors du clic
  }); // each [data-modal_open]

  // openModalBtn.on("click", function() {
  //   openModal_pls(modal_open);
  // });


  




}); // SOCKET // socket_default.on('up-folders'




/// openModal_pls
/// SOURCE inspired -- https://www.freecodecamp.org/news/how-to-build-a-modal-with-javascript/
const openModal_pls = function openModal(modal_open) {

  // alert("function openModal == " +modal_open)

  if (modal_open == "mv_modal") {
    const checkedInputs = $("input.img_up_chooser:checked");

    if (checkedInputs.length >= 1) {
      console.log("open modular");
    } else {
      alert("cocher au moins 1 image ci-dessous !")
      return
    } // IF checkedInputs.length >= 1) {

    $(".modal[data-modal_open='mv_modal']").each(function() {
      // $(this).removeClass("hidden");
      const thiis = $(this)

      if ( $(this).find("#resultDropdown").length != 1 ) {
        // Cloner l'élément #resultDropdown et l'ajouter au modal
        $(thiis).find(".modal-content").html($("#resultDropdown").clone());

        $(thiis).find(".modal-content").find("#resultDropdown").attr("id", "resultDropdown_move");
        
        // $("#resultDropdown_move").after("<span id='help_resultDropdown_move'> ( <span class='txt_clr go'>sub_go</span> )</span>")
        $(thiis).find("#resultDropdown_move").before("<span id='help_resultDropdown_move_title'> Déplacer les images vers le <span class='txt_clr go'>sub_go</span> :</span><br>")
        $(thiis).find("#resultDropdown_move").after("<div id='resultDropdown_move_files_preview'></div>");


        setTimeout(function () {
        
          // Récupérer tous les inputs cochés
          const checkedInputs = $("input.img_up_chooser:checked");
          const fileUrls = [];

          // Parcourir chaque input coché et récupérer le data-fileurl
          checkedInputs.each(function() {
            const fileUrl = $(this).data("fileurl");
            const fileName = $(this).data("filename");
            fileUrls.push({ url: fileUrl, name: fileName });
            // alert(fileUrls);
          });

          fileUrls.forEach(function(data) {
            // alert(data.name);
            let exists = $("#resultDropdown_move_files_preview").find("[data-name='"+data.name+"']").length;
            if (exists == 0) {
            $("#resultDropdown_move_files_preview").append("<img data-name='"+data.name+"' src='"+data.url+"'>")
            }
          });

        
          $("#resultDropdown_move").on('change', function() {
            const selectedValue_destination = $(this).val();
            // alert("changed")
            img_up_chooser_move_action(selectedValue_destination, fileUrls)
          }); // FUN on change #resultDropdown_move

        },130);

      } // IF CLONE N'existe pas... -- $("#resultDropdown").length < 2
    

    }); // each [data-modal_open...]
  
  } // IF -- if (modal_open == "mv_modal") {




  
  //////// DWL
  if (modal_open == "dwl_list") {
    setTimeout(function () {
      $("#dwl_list").focus()
    },100);  
  }


  /////////// RENAME
  if (modal_open == "namebatch_modal") {
    const checkedInputs = $("input.img_up_chooser:checked");

    if (checkedInputs.length >= 1) {
      console.log("open modular");
    } else {
      alert("cocher au moins 1 image ci-dessous !")
      return
    } // IF checkedInputs.length >= 1) {

    $(".modal[data-modal_open='"+modal_open+"']").each(function() {
      $(this).find(".modal-content").html("<span id='help_namebatch_modal'>Prefix + Renommer :</span><br>")
      $(this).find("#help_namebatch_modal").after('<div id="help_namebatch_modal_form"> \
          <span action=""> \
            <label for="fprefix">Prefix_ :</label><br> \
            <input type="text" id="fprefix" name="fprefix"><br> \
            <label for="lname">Nom :</label><br> \
            <input type="text" id="lname" name="lname"> \
            <button id="sub_namebatch_modal" value="Submit">Envoyer</button> \
          </span>  \
        </div>');
      
        $(this).find("#help_namebatch_modal_form").after("<div id='help_namebatch_modal_preview'></div>");


      const fileUrls = [];
      // Parcourir chaque input coché et récupérer le data-fileurl
      checkedInputs.each(function() {
        const fileUrl = $(this).data("fileurl");
        const fileName = $(this).data("filename");
        fileUrls.push({ url: fileUrl, name: fileName });
      });

      /// PREVIEWS start auto
      fileUrls.forEach(function(data) {
        $("#help_namebatch_modal_preview").append("<img data-name='"+data.name+"' src='"+data.url+"'>")
      });

      /// CLIC GO
      $("#sub_namebatch_modal").on("click", function() {
        // alert("dddllll")
        // e.preventDefault();

        fcount = 0;
        fileUrls.forEach(function(data) {
          
          fcount ++;
          let fileurl = data.url;
          let data_name = data.name;
          const fprefix = $('#fprefix').val();
          const lname = $('#lname').val();

          console.warn(
            "\n fileurl == \t\t\t"+fileurl, 
            "\n param_set_fix ==\t"+param_set_fix, 
            "\n param_go_fix ==\t"+param_go_fix, 
            "\n data_name == \t"+data_name, 
            "\n fprefix == \t\t"+fprefix, 
            "\n lname == \t\t\t"+lname
          )

          socket_default.emit("up_re_namebatch", fileurl, param_set_fix, param_go_fix, data_name, fprefix, lname, fcount)
        });
      });


    }); // EACH -> THIS
  }


  /// GENERAL
  $("[data-modal_open='"+modal_open+"']").each(function() {
    $(this).removeClass("hidden");
  })
  $(overlay).removeClass("hidden")

} // FUN / const openModal



const overlay = document.querySelector(".overlay");


// ACTIVE closeModal
$(".btn-close").each( function() {
  $(this).on("click", function() {
    // const modal_toclose = $(this).attr("data-modal_close");
    // closeModal(modal_toclose);
    // alert("btn-close");
    setTimeout(function () {
      $(".modal").each(function() { 
        $(this).addClass("hidden"); 
      });
      $(overlay).addClass("hidden")
  },40);
  });
});


// closeModal
const closeModal = function closeModal() { 
  // alert("closeModal");
  setTimeout(function () {
    $(".modal").each(function() { 
      $(this).addClass("hidden"); 
    });
    $(overlay).addClass("hidden")
 },100);
} // closeModal


$(overlay).on("click", function() {
  closeModal(); // Appel de closeModal uniquement lors du clic
});

// KEYPRESS Escape -> closeModal
document.onkeydown = function(evt) {
  evt = evt || window.event;
  var isEscape = false;
  if ("key" in evt) {
      isEscape = (evt.key === "Escape" || evt.key === "Esc");
  } else {
      isEscape = (evt.keyCode === 27);
  }
  if (isEscape) {
      // alert("Escape");
      closeModal()
  }
};
//// END // ACTIVE closeModal









/// DEPLACER IMAGES COCHEES
function img_up_chooser_move_action(selectedValue_destination, fileUrls) {

  if ( selectedValue_destination == "NOUVEAU" ) {
    $("#resultDropdown_move")
        .after("<div wrap='new_folder_destination'> \
              <input name='new_folder_name'></input> \
              <button \
                    id='sub_folder_moveto' type='submit' class='submit_lsinput rename_btn moveto' value='sub_folder_moveto'>Nouveau</button> \
              </div>")
    $("[name='new_folder_name']").focus();

    $('#sub_folder_moveto').on("click", function() {
      let newfolder_form_val = ( $("[name='new_folder_name']").val() ) ? $("[name='new_folder_name']").val() : "_up";
      let newfolder_fix ="/"+param_set_fix+"/"+newfolder_form_val;
      // alert(newfolder_form_val)
      // alert(newfolder_fix)

      socket_default.emit("up_server_newfolder_set", newfolder_fix)

      setTimeout(function () {
    
        // Récupérer tous les inputs cochés
        const checkedInputs = $("input.img_up_chooser:checked");
        const fileUrls = [];

        // Parcourir chaque input coché et récupérer le data-fileurl
        checkedInputs.each(function() {
          const fileUrl = $(this).data("fileurl");
          const fileName = $(this).data("filename");
          fileUrls.push({ url: fileUrl, name: fileName });
          // alert(fileUrls);
        });
        
        let newfolder_fix_val ="/"+newfolder_form_val+"/";
        img_up_chooser_move_action(newfolder_fix_val, fileUrls)

      },500);


      let selectedValue = newfolder_form_val;
      setTimeout(function () {
        const currentUrl = new URL(window.location.href);
        currentUrl.searchParams.set('go', selectedValue); // Ajoute ou met à jour le paramètre 'param'
        window.history.pushState({ path: currentUrl.href }, '', currentUrl.href);
        window.location.href = currentUrl.href; // Recharge la page avec la nouvelle URL
      },1500);
    
    });
    return
  } // IF selectedValue_destination == NOUVEAU

  let param_set_ok = urlParams.get("set")

  // alert(selectedValue_destination)
  if (window.confirm("Déplacer vers ----> "+selectedValue_destination+" ?")) {

    // Ouvrir un popup modulaire pour chaque élément dans le tableau
    fileUrls.forEach(function(data) {

      // alert(data.name); // Affiche chaque valeur dans la console
      socket_default.emit("up_server_move_imgs", data.url, data.name, selectedValue_destination, param_set_ok)
      // alert(param_set_ok);

      // let data_url_fix = "/"+data.url
      const data_url_fix = param_set_ok +"/"+ data.name;
      // alert(data_url_fix)
                // socket.on("up_server_rm_img", function (data_old_name, data_filename, fileurl, context) {
                                            // data_old_name, data_filename, fileurl, context
      socket_default.emit("up_server_rm_img", data.name, data.name, data_url_fix, "SUPPRIME") // EMIT

      const modal = document.querySelector(".modal");
      const overlay = document.querySelector(".overlay");

      modal.classList.add("hidden");
      overlay.classList.add("hidden");
    });

  } // IF window.confirm



  
  
} ///// img_up_chooser_move_action




// socket_default.on("watchr_remove-image", function(data_folder, data_filename) { // data_filename == result
//   let hashme=data_filename.hashCode();
//   // alert("Remove -- watchr_remove-image "+hashme)
//   $("[hashme='"+hashme+"']:not(.complete)").remove()
// });



// $('#img_up_namebatch_btn').on("click", function() {

//   const checkedInputs = $("input.img_up_chooser:checked");

//   if (checkedInputs.length >= 1) {
//     console.log("open modular");
//   } else {
//     alert("cocher au moins 1 image ci-dessous !")
//     return
//   } // IF checkedInputs.length >= 1) {

// });




