// DISABLE UI on CC => cc.html
// ==> ADD .live => hide some UI texts


// ### GET PARAMS suite
const urlParams = new URLSearchParams(window.location.search);

// set_is
const param_set = urlParams.get("set")
if (param_set) {
  var param_set_fix = param_set.toString().replace("set=", "")
  console.warn("!!!!! PARAM SET (param_set_fix) == " + param_set_fix)
  // alert(param_set_fix)
  document.title = 'CC - ' + param_set_fix + " -- Chat-chouquette"
} else {
  var param_set_fix = "00_defaut"
  // alert(param_set_fix)
}


// RAND / DEMO ##############
const param_rand = urlParams.get("rand") ? urlParams.get("rand") : "norand";
const param_rand_fix = param_rand.toString().replace("rand=", "")
console.warn("!!!!! PARAM rand (param_rand_fix) == " + param_rand_fix)
// ###### suite plus bas (others params next)

// RAND ##############
// RAND (chosse a random startup sub-folder)
if (param_rand_fix == "norand") {
  var is_demo_rand = "no"
} else {
  var is_demo_rand = param_rand_fix;
}


// ########################
// ################# ???
// DEMO ##############
const is_demo = "yes"       // [yes]/no : Autorun (bypass LS) //TODO NOT set correctly
const is_demo_start = 0     // [0]-9999 : Autorun at folder number ?  //TODO 0_name -> + facile 
const is_demo_loop = "no"   // [no]/yes : at end loop same Folder (w/out reloading) //TODO


// COVER ##############
const cover_plz = "no" // [no]/yes : IMG de présentation // TODO CC-ask->serveur yes/no (+) defaut sets/xyz/cover.png
// var fileurl_cover_default = "/img/peche.jpg"
const fileurl_cover_default = "/sets/" + param_set + "/cover.png"
// alert(fileurl_cover_set)
const imgPreload = new Image();
imgPreload.src = fileurl_cover_default; // Précharge l'image
imgPreload.onload = function () {
  // alert("loaded")
  $('#images_container').append("<div class=\"image cover active\" style=\"background-image: url(\'" + fileurl_cover_default + "\'); animation-duration: " + original_speed + "ms;\">");
}
const data_filename_cover = "/img/chouquetteoko.png"

// CC Animation / Pauses ###########
var change_img_interval = 15 * 1000; // IMG change /// TODO IF CHANGED =/= (6) => NEED CSS CHANGE ANIMATION // TODO .env
var scenario_new_timeset = change_img_interval;
const didactic = "yes"     // [no]/yes : Pause_loop ? //TODO NOT set correctly
const show_uploads = "yes" // [no]/yes : _Up folder ? //TODO NOT set correctly

// PAUSES ( if didactic==yes ) #######
const first_pause = 120 * 1000;              /* First Pause */
const pause_duration = change_img_interval; 		        /* Durée totale de la pause */
const pause_change = 1 * 1000; /* Img change interval during pause */
const pause_every_interval = 30 * 60 * 1000; 		/* Next Pause */
// const pause_change_interval = 4 * 1000;     /* Img change interval during pause */
// TD REDO
// var max_loop = 2;

// SPEED ###################
const min_speed = "3500";                   // press o -- ralenti
const max_speed = "250";                    // press k -- accelere
const number_ms = 10;                       // ratio.interval +/- speed on press

// DEBUG ################
const debug = "no"
function onerror_function(fileurlhere) {
  if (debug == "yes") { alert(fileurlhere) }
}

// HELP Message
let helpmessage = "H == Help (this)<br> \
R == Randomize <br> \
⇩ == + Vite == K <br> \
⇧ == - Vite == O <br> \
⇦ == Prev <br> \
⇨ == Next <br> \
---<br>\
F == Fullscreen (or tap)<br> \
P == Pause <br> \
T == Kill <br> \
U == Ultra <br>\
---<br>\
C == Rouge <br>\
V == Vert <br>\
B == Blue <br>\
N == Noir <br>\
G == Gray <br>"




// ##################################
// ##################################
// ### GET PARAMS suite #############
// const param_loop = urlParams.get('loop');
const param_loop = urlParams.get("loop") ? urlParams.get("loop") : "noloop";
const param_loop_fix = param_loop.toString().replace("loop=", "")

console.warn("!!!!! PARAM LOOP (param_loop_fix) == " + param_loop_fix)
const param_go = urlParams.get("go") ? urlParams.get("go") : "nogo";
// alert("param_go == "+param_go)
const param_go_fix = param_go.toString().replace("go=", "")
console.warn("!!!!! PARAM GO (param_go_fix) == " + param_go_fix)

const param_live = urlParams.get("live") ? urlParams.get("live") : "non";

// ######################################
// ######## END GET PARAMS ##############
// ######################################

// ##################################################
// ######## USER LOCAL PARAMS (speed) ###############
// ##################################################
// IF localStorage : original_speed ( use it or use default_speed );
const default_speed = 450;
var original_speed = localStorage.getItem('original_speed') ? localStorage.getItem('original_speed') : default_speed;

if (!localStorage.getItem('original_interval_speed')) {
  var original_interval_speed = 460;
  localStorage.setItem('original_speed', 460);
}
if ((original_speed == "") || (original_speed == undefined)) {
  original_speed = 460;
  localStorage.setItem('original_speed', 460);
}
// ##################################################

// ######################################
// ######### IO CONNECTION ##############
// ###### + Send PARAM to Server ########
// ######################################
// # SET VAR/OPT/HANDSHAKE ==> SERV  == var set_is=socket.handshake.query.set_is
var ioo = io({ query: { set_is: param_set_fix } })
var socket = io.connect("/cc")
// #######################################


// ###################################################
// ###################################################
// ############## FUNCTIONS ##########################
// ###################################################
// ###################################################

// ###########################################
// ########################################### CC SCENARIOS / FCT
// ###########################################

// AUTOSTART SCENARIO  ############
$(document).ready(function () {

  // $('body').attr("data-current_num",) 

  console.log("ready!");
  scenario(change_img_interval);
  if (didactic == "yes") {
    setInterval(pause_loop, pause_every_interval)
    setTimeout(function () {
      pause_loop();
    }, first_pause);
  }

  // HELP #################
  alerte_pan(helpmessage)
  help_toggle(10 * 1000)

  socket.emit("cc-get_folder_files_extra", "Z_tests", "aaaa"); //EMIT

  if (param_live != "non") {
    $("html").addClass("live");
    $(".panels").remove()
  }


}); // ready


socket.on('cc-get_folder_files_extra_go', function (get_folder_files, folder_name, folder_num, dirname_plz) { // GET_FILES

  // alert("goooo")
  // alert(folder_name+","+ folder_num+","+ dirname_plz)
  let folder_name_exist = $("#images_container_extra").find(".image[data-folder='" + folder_name + "']").length
  console.warn("OK LOAD -- folder_name_exist ?= " + folder_name_exist)
  // $('body').attr("data-current_num", folder_num)
  // alert("pause 2")

  if (folder_name_exist == 0) { // IF Folder NOT already loaded... loading
    get_folder_files.forEach(function (filename) {
      let fileurl = filename.replace(dirname_plz + "/client/", "");
      // alert("fileurl f = "+fileurl)
      // let set_is="sets" // <<---- /sets/...
      // let regex="/^(.+)sets\//gm"
      let data_filename = fileurl.replace("/media/BC_01C/@NodeJS/nodechouquette/client/", "");

      $("#images_container_extra").append("<div class='image' data-folder='" + data_filename + "' data-fld_num='" + folder_num + "' data-fileurl='" + data_filename + "' style=\"background-image: url(\'" + data_filename + "\'), url(\'" + data_filename + "\') ; animation-duration: " + original_speed + "ms;\" alt=" + data_filename + " onerror='onerror_function(" + fileurl + ")'></div>");
    })
  }

  // scenario_extra(1000)
  // setInterval(scenario_extra, 5000);

});





let scenario_extra_firststart = 10 * 1000; // START (not looping)
let scenario_change_bg_interval = 4 * 1000; // CHG_BG (rotate)
let scenario_extra_duration = 20 * 1000; // SC_EXTRA (Full duration)
let scenario_extra_restart = 60 * 1000 * 45; // SC_EXTRA (loop interval)

// setTimeout(function () {
//   scenario_extra();
// }, scenario_extra_firststart);
setInterval(scenario_extra, scenario_extra_restart);


function change_bg_extra() {
  if ($("#images_container_extra").find(".image.active").length == 0) {
    $("#images_container_extra").find(".image").first().addClass("active")
  } else if ($("#images_container_extra").find(".image.active").next(".image").length == 0) {
    $("#images_container_extra").find(".image").first().addClass("active").nextAll().removeClass("active")
  } else {
    $("#images_container_extra").find(".image.active").first().next().addClass("active").prevAll().removeClass("active");
  }
}

function scenario_extra() {
  clearInterval(change_bg_extra);
  clearInterval(scenario_extra);

  if (!$("#images_container_extra").hasClass("forced")) {
    setInterval(change_bg_extra, scenario_change_bg_interval);
    $("#images_container_extra").addClass("showme");

    setTimeout(function () {
      $("#images_container_extra").removeClass("showme");
      clearInterval(change_bg_extra);
    }, scenario_extra_duration);
  }
}

// ###################################################
// ################################################### AUTRES
// ###################################################
function pause_loop(pause_every_interval) {
  if (didactic == "yes") {
    $("html").addClass("pause_loop")
    $(".active").addClass("pause_loop")
    setTimeout(function () {
      change_bg("next", "pop");
    }, pause_change);
    setTimeout(function () {
      // change_bg("prev"); 
      change_bg("prev", "pop");
    }, pause_change);
    setTimeout(function () {
      $("html").removeClass("pause_loop")
      // RETURN TO LAST ACTIVE THEN PAUSE_LOOP STARTED
      $(".image.pause_loop").removeClass("pause_loop").next().addClass("active").nextAll().removeClass("active").removeClass("ultra")
    }, pause_duration);
  }
}

// ###################################################
function scenario(change_img_interval) {
  clearInterval(change_img_setinterval);
  var change_img_setinterval = setInterval(change_bg, change_img_interval);
}




// ####################################################
// ####### FULLSCREEN + 3 Tap Pause ###################
// ######## #overlay central button ###################
// ####################################################
function fullscreen() {
  // alert("fs")
  if (!document.fullscreenElement) { // 1e Tap
    $('html').addClass("fullscreen");
    document.documentElement.requestFullscreen();
    alerte_me("tap_1", "tap_1", 3000);

  } else if ($('html').is(".taper")) { // 4e Tap -> tapause
    $('html').removeClass("taper").addClass("taperpause");
    $('html').addClass("forcepause"); // PAUSE totale
    $('body').addClass("tapause");    // PAUSE totale

  } else if ($('html').is(".taperpause")) { // 5e Tap -> tap
    change_bg("prev"); // QF prev then next help -> back to speed (no freez)
    pause_me(10000, "", "forced");
    $('html').addClass("forcepause");
    $('html').removeClass("taperpause taper").addClass("tap");
    $('html').removeClass("forcepause"); // PAUSE totale
    change_bg("next");  // QF prev then next help -> back to speed (no freez)
    alerte_me("tap_4", "tap_4", 3000);

  } else if ($('html').is(".tap")) { // 3e Tap -> taper
    change_bg("prev");
    $('html').addClass("forcepause").addClass("taper"); // RESET
    $('body').removeClass("tapause"); // PAUSE
    alerte_me("tap_3", "tap_3", 3000);

  } else { // 2e Tap (is fullscreen) => PAUSE slow
    change_bg("prev");
    $('html').addClass("taper tap forcepause").data("tap", "3"); // TAPER
    alerte_me("tap_2", "tap_2", 3000);
  }
}
// function fullscreen_exit() {
//     setTimeout(function () {
//             $('html').removeClass("fullscreen");
//             $("#images_container").attr("onclick","fullscreen()");
//             document.exitFullscreen();
//     }, 1500);
// }


// $("#images_container").attr("onclick","fullscreen()");



// ###################################################
function pause_me(pause_duration, scenario_new_timeset, context) {
  alerte_me("pause", 1000, "gray");
  if ($('html').is(".randomizing")) {

    console.log("-- OCCUPIED, cant active pause rigth now !")

  } else if ($('html').is(".forcepause") || context == "forced") {
    setTimeout(function () {
      let change_img_setinterval = change_img_interval;
      clearInterval(change_img_setinterval);
    }, 10);
  } else if ($('html').is(".pause_me")) {

    if (!$("html").hasClass("forcepause")) {
      console.log("-- NOT Pausing -> Stop pause : Restart Scenario")
      pause_me_stop(scenario_new_timeset);
      // console.log("-- end pause");     
      change_bg();
    } else {
      $("html").removeClass("forcepause");
    }

  } else {

    setTimeout(function () {
      let change_img_setinterval = change_img_interval;
      clearInterval(change_img_setinterval);
      // clearInterval(pause_loop_setinterval)
      if (!$("html").hasClass("forcepause")) {
        $("html").addClass("pause_me");
        alerte_me("Pause !", "1200", "orange");
        console.log("Paused = = = true");
      } else {
        setTimeout(function () {
          $("html").removeClass("pause_me");
          alerte_me("Pause forced !", "1200", "orangered");
          console.log("Paused = = = forced");
        }, 10);
      }
      // scenario(pause_change_interval);
      // alert(change_img_setinterval);
    }, 100);


    if (!$("html").hasClass("forcepause")) {

      if (pause_duration) {
        setTimeout(function () {
          pause_me_stop(scenario_new_timeset);
          console.log("-- STOP pause - not forced c1");
          change_bg();
        }, pause_duration);

      } else {

        setTimeout(function () {
          pause_me_stop(scenario_new_timeset);
          console.log("-- STOP pause - forced c2");
          //  change_bg(); 
        }, pause_duration);

      }
    }

  }
}

// ###################################################
function pause_me_stop(scenario_new_time) {
  $("html").removeClass("pause_me");
  // scenario(change_img_interval);
  // console.log("Paused = = = end = restart scenario");
  if (!$("html").hasClass("forcepause")) {
    alerte_me("End Pause !", "1200", "pink");
    console.log("-- end pause");
    scenario(scenario_new_time);
  } else {
    $("html").removeClass("pause_me");
    scenario(scenario_new_time);
  }

}


// ###################################################
// 'start_folder', demo_force_start, ls_state, this_folder, demo_next_folder, next_folder
// NOT IN USE ?? NO emit at the end
// ?? ->> start_folder -> cc-loadall ==> xxx
// socket.on('start_folder', function (start, ls_state, dirname, next_num, next) {
//   console.log(start, dirname, next);
//   var param_set_fix=param_set.toString().replace("set=","")
//   let set_is=param_set_fix;
//   $("html").attr("data-current_num", ls_state);
//   $("html").attr("data-current_folder", dirname);
//   $("html").attr("data-next", next);
//   $("html").attr("data-next_num", next_num);
//   $("body").attr("data-set_is",set_is);
//   socket.emit("cc-loadall", ls_state, dirname, next_num, next); //EMIT
// });

// ###################################################
/// SCENARIO change_bg
function change_bg(direction, option) {
  // alert("change_bg")
  if ($("body").hasClass("lock")) {
    console.warn("lock !")
    // alert("lock !")
    // $("body").removeClass("lock")
    return

  } else if (direction == "prev") { // #################### P R E V
    // alert("a")
    console.log("1 change_me direction == " + direction + " + option == " + option)
    var check_prevprev_img = $("#images_container").find(".active").first().prev(".image").prev(".image")

    if ($("html").hasClass("forcepause")) { // En mod pause forcée on recule de 1 (-1) 
      // ##### FORCEPAUSE
      // alert("b") 
      if ((check_prevprev_img.length < 1)) { // LAST
        // alert("c")
        $("#images_container").find(".image").last().prev().prev().prev().addClass("active").prevAll().removeClass("active").removeClass("ultra");

      } else {
        // alert("d")
        $("#images_container").find(".active").first().prev().addClass("active").nextAll(".image").removeClass("active").removeClass("ultra");

      }

    } else {
      if ((check_prevprev_img.length < 1)) { // FIRST -- prev
        // ##### NOT FORCEPAUSE
        // alert("e") 
        $("#images_container").find(".image").last().prev().prev().prev().addClass("active").prevAll().removeClass("active").removeClass("ultra");
      } else {
        // ##### NOT FORCEPAUSE -- NORMAL DEFAULT -- prev
        // alert("f") 
        $("#images_container").find(".active").first().prev().prev().addClass("active").nextAll(".image").removeClass("active").removeClass("active");
      }
    }


  } else if (direction == "next") { // #################### N E X T

    console.log("2 change_me direction == " + direction + " + option == " + option)

    // const imgPreload = new Image();
    // const fileurl = $("#images_container").find(".active").first().next(".image").attr("data-fileurl");
    // alert(fileurl);
    // imgPreload.src = fileurl; // Précharge l'image

    // const fileurl_1 = $("#images_container").find(".active").first().next(".image").next(".image").attr("data-fileurl");
    // imgPreload.src = fileurl_1; // Précharge l'image

    // imgPreload.onload = function() {

    $("#images_container").find(".active").first().next(".image").addClass("active").prevAll(".image").removeClass("active");
    // $("#images_container").find(".active").last().prevAll(".image").removeClass("active");

    // let image_to_load = $("#images_container").find(".active").first().next(".image");
    // Précharger l'image
    // const imgPreload = new Image();
    // imgPreload.src = image_to_load.attr("data-fileurl"); // Utilise l'URL de l'image

    // imgPreload.onload = function() {
    //   // alert(image_to_load.attr("data-fileurl"))
    //   // Une fois que l'image est préchargée, précharger l'image suivante
    //   let image_to_load_next = $("#images_container").find(".active").first().next(".image");
    //   const imgPreloadNext = new Image();
    //   imgPreloadNext.src = image_to_load_next.attr("data-fileurl"); // Utilise l'URL de l'image suivante
    //   // alert(image_to_load_next.attr("data-fileurl"))
    // };

    if ($("html").hasClass("forcepause")) { // En mod pause forcée on recule de 1 (-1)
      var check_nextnext_img = $("#images_container").find(".active").first().next(".image").next(".image").next(".image")
      console.log(check_nextnext_img.length)
      if ((check_nextnext_img.length < 1)) {
        change_bg("pop", "force_manual")
        // alert("next end")
        let current_num_next = (current_num == 0) ? 1 : Number(current_num) + 1;
        $('body').data("current_num", current_num_next)
        socket.emit("cc-get_folder_files", current_fld, current_num_next, show_uploads); //EMIT
      }
    }

    // };

  } else if ((option != "force_change" && option != "undefined") && ($('html').is(".pause_me")) || ($('html').is(".forcepause")) || ($('html').is(".paused"))) {
    // alert("2")
    console.log("change_me direction und == " + direction + " + option == " + option)
    console.log("change_me NOT ( en pause + not force_change )")

  } else {
    // alert("3")
    console.log("3 change_me direction == " + direction + " + option == " + option)
    var index = $("#images_container").find(".active");
    if (index.length == 0) {
      // alert("noindex")
      $("#images_container").find(".image").last().addClass("active");
    }
    // console.error(index)


    // console.log("change bg ! ==\n[ "+$(index).attr("data-fld_num")+" ] "+$(index).attr("data-folder")+"\n"+$(index).attr("alt"));
    // if (index.length == 0) {
    //     console.log("noin");
    // }

    if ($("body").hasClass("lock")) {
      return
    } else if (is_demo == "yes") { // DEFAULT
      var nextnear = $("#images_container").find(".active").first().next(".image").next(".image").next(".image");
    } else {
      var nextnear = $("#images_container").find(".active").first().next(".image").next(".image");
    }

  }





  if ((direction == "undefined") && (option != "force_change") && (option != "undefined") && ($('html').is(".pause_me")) || (($('html').is(".forcepause")) && (option != "force_manual")) || (($('html').is(".paused, pause_loop")) && (option != "force_manual"))) {
    // alert("4")
    console.log("change_me NOT ( not force_change )")
  } else if ((direction != "prev")) { // NOT PREV <--- need some speciales on forcepause (voir +haut) 
    // alert("5")
    if (is_demo == "yes") {
      var nextnear = $("#images_container").find(".active").first().next(".image").next(".image").next(".image");
    } else {
      var nextnear = $("#images_container").find(".active").first().next(".image").next(".image");
    }

    // alert("6")

    if ((option != "force_manual") && ((option != "force_change" && option != "undefined") && ($('html').is(".pause_me")) || ($('html').is(".forcepause")) || ($('html').is(".paused")))) {
      console.log("change_me NOT ( not force_change )")
      // alert("7")

    } else if (nextnear.length != 0) { // NOT DERNIERE IMG : Encore des img apres active <- default
      // alert("8")


      if ($("body").hasClass("lock")) {
        return
      }

      $("#images_container").find(".active").first().next(".image").addClass("active").prevAll().removeClass("active")
      // .prev().addClass("ultra");
      // $("#images_container").find(".active").prevAll().removeClass("active")
      // setTimeout(function () {
      //   $("#images_container").find(".ultra").removeClass(".ultra")
      // },change_img_interval + 100);
      // alerte_me("chg bg", 1000, "chartreuse");


    } else { // DERNIERES IMG

      if (is_demo == "yes" && is_demo_loop != "yes" && param_loop_fix != "yes") { // NOT LOOP

        console.warn("\n\n\n\nend NOT loop")
        console.warn("findumonde");
        // $('#images_container').find(".image").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active")

        // alerte_me("dernières images", 3000, "red")
        // alert("aaa")
        let is_up = $('body').attr("data-isup_number");
        let howmuch_number = $('body').attr("data-howmuch_number");
        var current_fld = $('body').attr("data-current_fld");
        let current_num = $('body').attr("data-current_num")
        let current_num_next = (current_num == 0) ? -1 : Number(current_num);
        // alert(current_num+" =/= "+current_num_next)

        if (current_num_next >= howmuch_number) { // DER des DER
          console.warn("\n\n\n\nend DER des DER")
          // alert("toomu")
          socket.emit("cc-get_folder_files", "", -1, show_uploads);
          console.warn("toomuch == " + current_num_next + " > " + howmuch_number)
          $('body').attr("data-current_num", "-1");
          // .addClass("reload_from_zero"); // TODO CHECK
          // alert("tm")
          // $('#images_container').find(".image").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active")




        } else if (current_num_next == is_up) { // UP

          console.warn("\n\n\n\nend NOT loop UP")
          // alert("nextis up")
          console.warn("is_up ==" + is_up + "current_num_next ==" + current_num_next)
          let current_num_next_fix = Number(current_num_next);
          // alert("current_num_next_fix == "+current_num_next_fix)

          if (current_num_next_fix > howmuch_number) { // UP Der des DERS
            console.warn("\n\n\n\nend NOT loop UP + DER DES DER")
            // alert("nextup fix toom")
            socket.emit("cc-get_folder_files", "", -1, show_uploads);
            console.warn("toomuch == " + current_num_next_fix + " > " + howmuch_number)
            $('body').attr("data-current_num", "-1");
            // .addClass("reload_from_zero"); // TODO CHECK

          } else { // UP default

            let current_num_next_fix = Number(current_num_next) + 0;
            $('body').attr("data-current_num", current_num_next_fix);
            socket.emit("cc-get_folder_files", current_fld, current_num_next_fix, show_uploads); //EMIT
          }
          // alert("current_num_next_fix == "+current_num_next_fix)


        } else { // DEFAULT NotLOOP

          // prevent loading if next_right_arrow_key pressed (wait, prevent bypass folder)
          if (!$("html").hasClass("loading_next")) {
            $("html").addClass("loading_next")
            let current_num_next_fix = Number(current_num_next);
            console.warn("\n\n\n\nend NOT loop DEFAULT")
            socket.emit("cc-get_folder_files", current_fld, current_num_next, show_uploads); //EMIT
            $('body').attr("data-current_num", current_num_next_fix)
            // alert("ooo " +current_num_next_fix) 
          }

          setTimeout(function () {
            $("html").removeClass("loading_next")
          }, 2000);

          // alert("t1")

        }

      } else if (is_demo_loop == "yes" || param_loop_fix == "yes") { // DEMO loop

        console.warn("\n\n\n\nend YES loop")
        // alert(param_loop_fix)
        // FONCTION play from start
        $('#images_container').find(".image").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active")
        console.warn("L O O P mode")

      } else { // NOT DEMO + N + derniere img


        socket.emit("cc-get_folder_files", current_fld, 0, show_uploads); //EMIT
        // alert("end no no no")
        console.log("__is_demo - fin du monde : ajouter des images !")
        $("#images_container").find(".active").removeClass("active")
        // $("#images_container").find(".ultra").removeClass("ultra")
        setTimeout(function () {
          $("#images_container").find(".image").last().addClass("active")
        }, 10);
        // alert('t4')

      }
    } // DERNIERES IMG nextnear ...
  } // if ( (option != "force_change"...

  $("html").removeClass("reseting");
  // let index = $("#images_container").find(".active");
  // if (index.length == 0) {
  //   // alert("noindex")
  //   setTimeout(function () {
  //     $("#images_container").find(".image").last().addClass("active");
  //   }, 10);
  // }
}





// #########################################################
// #########################################################
// ####################### SOCKETS #########################
// #########################################################
// #########################################################
// TODO FIX Keypress next (right arrow) sometime not working
// socket.on('say_hello', function (data) {
//   console.warn("\n\nSERVER RESTART == SAY_HELLO == "+data+"\n\n")
// });
// socket.on('watchr_add-image', function (data) {
//   console.warn("\nNEW IMG == "+data+"\n\n")
// });



// #########################################################
socket.on("serv-neww-file", function (data_folder, data_filename, fileurl, context, action) {  // clicked from up
  console.warn("\n\nclicked img == " + data_filename + "\n\n");
  // alert(fileurl)
  // let hashme=data_filename.hashCode();
  // alert("new")

  // Précharger l'image avant de l'ajouter au DOM
  // const imgPreload = new Image();
  // imgPreload.src = fileurl; // Précharge l'image

  if (context == "from_up_page") {
    // // $('#images_container').find(".active").first().after(theimg); 
    // // $('#images_container').find(".active").first().nextAll().removeClass("active");
    // $('#images_container').find(".active").first().before(theimg); 
    // $('#images_container').find(".active").first().nextAll().removeClass("active");

    // $('#images_container').find(".active").first().prev().before(theimg); 
    // $('#images_container').find(".active").first().prev().nextAll().removeClass("active");
    // $('#images_container').find(".active").first().prev().prevAll().removeClass("active");



    // imgPreload.onload = function() {
    let context_fix = (context) ? context : "no_context";
    let theimg = ("<div data-hash=\"\" class=\"image clicked active " + context_fix + "\" data-folder=\"" + data_folder + "\" data-fileurl=\"" + fileurl + "\" \
                              style=\"background-image: url(\'"+ fileurl + "\'); animation-duration: " + original_speed + "ms;\" \
                              alt=\""+ data_filename + "\" onerror='onerror_function(\"" + fileurl + "\")'></div>");
    $('#images_container').find(".active").first().before(theimg);

    $('#images_container').find(".active").first().before(theimg);
    $('#images_container').find(".active").first().nextAll().removeClass("active");
    $('#images_container').find(".active").first().prevAll().removeClass("active");

    // };

  } else if (context == "piano") {
    // alert("piano")


    let context_fix = (context) ? context : "no_context";
    let theimg = ("<div data-hash=\"\" class=\"image clicked active " + context_fix + "\" data-folder=\"" + data_folder + "\" data-fileurl=\"" + fileurl + "\" \
                            style=\"background-image: url(\'"+ fileurl + "\'); animation-duration: " + original_speed + "ms;\" \
                            alt=\""+ data_filename + "\" onerror='onerror_function(\"" + fileurl + "\")'></div>");
    // $('#images_container').find(".active").first().before(theimg); 
    $('#images_container').append(theimg); // ... Append directly
    // };


    if (action == "unpress") {
      // alert("unpress")

      $(".piano").each(function () {
        $(this).remove();
      })

    } else {
      // alert("piano no")
      // alert(theimg)
      $('#images_container').find(".image").last().after(theimg);
      // $('#images_container').find(".active").last().prevAll().removeClass("active");
    }
  } else if ($('#images_container').find(".image").length < 1) { // IF img BUT NOT ACTIVE


    // imgPreload.onload = function() {
    let context_fix = (context) ? context : "no_context";
    let theimg = ("<div data-hash=\"\" class=\"image clicked active " + context_fix + "\" data-folder=\"" + data_folder + "\" data-fileurl=\"" + fileurl + "\" \
                              style=\"background-image: url(\'"+ fileurl + "\'); animation-duration: " + original_speed + "ms;\" \
                              alt=\""+ data_filename + "\" onerror='onerror_function(\"" + fileurl + "\")'></div>");
    // $('#images_container').find(".active").first().before(theimg); 
    $('#images_container').append(theimg); // ... Append directly
    // };

    // } else if ( ( ($('#images_container').find(".active").next()).length == 1 ) && ( $("div[data-hash='"+hashme+"']").length < 1 ) ) {
  } else if ((($('#images_container').find(".active").next()).length == 1)) {

    // imgPreload.onload = function() {
    let context_fix = (context) ? context : "no_context";
    let theimg = ("<div data-hash=\"\" class=\"image clicked active " + context_fix + "\" data-folder=\"" + data_folder + "\" data-fileurl=\"" + fileurl + "\" \
                              style=\"background-image: url(\'"+ fileurl + "\'); animation-duration: " + original_speed + "ms;\" \
                              alt=\""+ data_filename + "\" onerror='onerror_function(\"" + fileurl + "\")'></div>");
    $('#images_container').find(".active").first().before(theimg);
    $('#images_container').find(".active").first().nextAll().removeClass("active");
    $('#images_container').find(".active").first().prevAll().removeClass("active");

    // };

    // $('#images_container').find(".active").first().before(theimg); 
    // $('#images_container').find(".active").last().prevAll().removeClass("active");
    // alert("1") 
    // NEW

    // } else if ( ( ( $('#images_container').find(".active").next().next()).length == 1 ) && ( $("div[data-hash='"+hashme+"']").length < 5 ) ) {


    //   // imgPreload.onload = function() {
    //     let context_fix = (context) ? context : "no_context";
    //     let theimg=("<div data-hash=\""+hashme+"\" class=\"image clicked active "+context_fix+"\" data-folder=\""+data_folder+"\" data-fileurl=\""+fileurl+"\" \
    //                           style=\"background-image: url(\'"+fileurl+"\'); animation-duration: "+original_speed+"ms;\" \
    //                           alt=\""+data_filename+"\" onerror='onerror_function(\""+fileurl+"\")'></div>");
    //     $('#images_container').find(".active").next().next().after(theimg); // ... if ACTIVE exists, to do it next to it
    //     $('#images_container').find(".active").last().prevAll().removeClass("active");

    // };
    // alert("2")
  } else {


    // imgPreload.onload = function() {
    let context_fix = (context) ? context : "no_context";
    let theimg = ("<div data-hash=\"\" class=\"image clicked active " + context_fix + "\" data-folder=\"" + data_folder + "\" data-fileurl=\"" + fileurl + "\" \
                              style=\"background-image: url(\'"+ fileurl + "\'); animation-duration: " + original_speed + "ms;\" \
                              alt=\""+ data_filename + "\" onerror='onerror_function(\"" + fileurl + "\")'></div>");
    $('#images_container').find(".active").before(theimg); // ... if ACTIVE exists, to do it next to it
    $('#images_container').find(".active").last().prev().nextAll().removeClass("active");

    // };
    // alert("3")
  }

  // if ( $("div[data-hash='"+hashme+"']").length >= 5 ) {

  //   $("#images_container").find("div.clicked[data-hash='"+hashme+"']").last().prevAll("div.clicked[data-hash='"+hashme+"']").remove();
  //     $("#images_container").find("div.clicked[data-hash='"+hashme+"']").last().addClass("active")
  //     // alert("4")
  // }
  // if (context=="watcher_sent") {
  //   $("#images_container").find("div[data-hash='"+hashme+"']").last().addClass("watcher_sent");
  // }   

  var item_count = $('.image').length;
  for (i = 0; i < item_count; i++) {
    $('.image').eq(i).css('z-index', item_count - i);
  }
  if ($('#images_container').find(".active").length < 1) {
    $('#images_container').find(".image").first().addClass("active");
  }
  $("html").removeClass("reseting");
});

// #########################################################
// socket.on("watchr_remove-image", function (data_filename) { 
//   let hashme=data_filename.hashCode();
//   $('#images_container').find("div[data-hash='"+hashme+"']").remove();
// });



//// LS load_all_folders -> Server : ls-load_all_folders -> here
// socket.on("cc-load_all_folders", function () {
socket.on('cc-load_all_folders', function (get_folder_files, folder_name, folder_num, dirname_plz, set_folder, isfinalone, cond) { // GET_FILES
  let is_set = urlParams.get("set")
  let is_go= urlParams.get("set")

  // alert("set_folder == " + set_folder + " \nis_set == " + is_set)
  
  if ( $("#images_container").attr("data-load_all_folders") != "done" ) { // LOCK
    if  (set_folder == is_set)  { // -- pseudoROOMS
      get_folder_files.forEach(function (get_folder_files) {
        // alert(get_folder_files);
        let set_is = set_folder;
        // console.warn(set_is);
        let fileurl = "sets"+get_folder_files;
        let data_filename = fileurl.split('/').pop(); // Récupère le nom de fichier
    
        console.log(fileurl + " --- data_filename --- " +data_filename);
        $("#images_container").append("<div class='image' data-folder='" + folder_name + "' data-fld_num='" + folder_num + "' data-fileurl='" + fileurl + "' style=\"background-image: url(\'" + fileurl + "\'); animation-duration: " + original_speed + "ms;\" alt=" + data_filename + " onerror='onerror_function(" + fileurl + ")'></div>");
      });
      /// LOCKER
      if ( isfinalone == "oui_finaleone") {
        $("#images_container").attr("data-load_all_folders", "done")
        // alert("set_folder == " + set_folder + " \nis_set == " + is_set)
      }
    }
  } else{
    if ( (isfinalone == "oui_finaleone") && (cond == "button") ) {
      alert("already done")
    }
  }
  
});

// #########################################################
socket.on("cc-reset_all", function () {
  $('#images_container').find(".image").remove();
  $("html").addClass("reseting");
});

// #########################################################
socket.on('cc-rm_this_group', function (go_folder, how, set_folder) {
  // alert("cc-rm_this_group")
  $("#images_container").find(".image[data-folder='" + go_folder + "']").remove()
  $("#images_container").find(".image[data-folder='/" + set_folder + "/" + go_folder + "']").remove()
  alerte_me("remove group", 2000, "blue");
});

socket.on('serv-img-rm', function (fileurl) {
  $("#images_container").find(".image[data-fileurl='" + fileurl + "']").first().prev(".image").addClass("before_remove");

  setTimeout(function () {
    $("#images_container").find(".image[data-fileurl='" + fileurl + "']").remove();
    $("#images_container").find(".image").first().removeClass("active");

    if ($('#images_container').find(".active").length < 1) {
      $('#images_container').find(".before_remove").addClass("active").removeClass("before_remove");
    }
  }, 400);

  // $("#images_container").find(".image[data-folder='/"+set_folder+"/"+go_folder+"']").remove()
  // alerte_me("remove group", 2000, "blue");
});

socket.on('serv-clear_all_cc', function () {
  $("#images_container").find(".image[data-folder='texteditor']").first().prev(".image").addClass("before_remove");

  setTimeout(function () {
    $("#images_container").find(".image[data-folder='texteditor']").remove();
    $("#images_container").find(".image").first().removeClass("active");

    if ($('#images_container').find(".active").length < 1) {
      $('#images_container').find(".before_remove").addClass("active").removeClass("before_remove");
    }
  }, 400);
});

// #########################################################
socket.on('cc-new_speed_socket', function (new_speed) {
  // console.warn("\n\new_speed (from ls) == "+new_speed+"\n\n");
  alerte_me("newspeed", 1000, "yellow")
  alert("waited")

  let newspeed = $("body").attr("new_speed")

  if (newspeed != "") {
    $("body").attr("new_speed_new", new_speed)
  } else {

    $("body").attr("new_speed", new_speed)
    localStorage.setItem('original_speed', new_speed);
    $(".image").each(function () {
      $(this).css("animation-duration", new_speed + "ms").addClass("manual_speed");
    })

    setTimeout(function() {
      let newspeednew = $("body").attr("new_speed_new")

      if (newspeed != newspeednew) {
        localStorage.setItem('original_speed', new_speed);
        $(".image").each(function () {
          $(this).css("animation-duration", new_speed + "ms").addClass("manual_speed");
        })
      }

      $("body").attr("new_speed", "")
      alert("waited")
    }, 2000); // Attendre 2 secondes

  };
});

// #########################################################
socket.on('cc-killme', function (what) {
  alerte_me("killme " + what, 1000, "orange")
  console.log("killme " + what)
  kill_me(what);
});
// #########################################################
socket.on('cc-reload', function (new_speed) {
  alerte_me("reload", 1000, "orangered")
  window.location.reload();
});
// #########################################################
socket.on('cc-randme', function (new_speed) {
  alerte_me("randme", 1000, "orangered")
  rand_me();
  // alert("re")
});
// #########################################################
socket.on('cc-restart', function (what, num) {
  // alert("re")
  alerte_me("Restart", 1000, "orangered")
  restart(what, num);
});
// #########################################################
socket.on('ls-change_bg', function (todo, option) {
  change_bg(todo, option);
});
// #########################################################
socket.on('ls-pause_me', function (state) {
  // pause_me(pause_duration, "", state);
  pause_mutliple()
  console.warn("pause __ from ls")
});
socket.on('ls-lock_me', function (state) {
  $("body").toggleClass("lock");
  // pause_mutliple("lock");
  // alert("lock 1")
  console.warn("lock __ from ls")
});
// #########################################################
socket.on('cc-demo_load_others', function (reload_end_scenario, message) {
  demo_load_others(reload_end_scenario, message)
});

// #########################################################
socket.on('cc_style_change', function (type) {
  $("html").removeClass("rouge blue vert gray revert");
  change_style(type);
  console.log("cc type == " + type)
});
// #########################################################
socket.on('cc_goeffect', function (type) {
  // if ( $(".effect_"+type).length > 0 ) {
  //   $(".image").removeClass("effect_"+type+""+type);
  //   $(".image").removeClass("effect_"+type);
  //   // $(".effect_"+type).addClass("effect_"+type+""+type);
  //   $(".image").addClass("effect_"+type);
  // } else {

  if ($(".effect_" + type).length > 0) {
    $(".image").addClass("effect_" + type + "" + type);
    setTimeout(function () {
      $(".image").removeClass("effect_" + type + "" + type);
      $(".image").removeClass("effect_" + type);
    }, 2000);

  } else {

    $(".image").removeClass("effect_" + type + "" + type);
    $(".image").removeClass("effect_" + type);
    setTimeout(function () {
      $(".image").addClass("effect_" + type);
    }, 200);
  }

  // }


  // change_style(type);
  // alert("cc_goeffect")
  console.log("cc effect == " + type)
});


// #########################################################  
socket.on('newstyle', function () {
  newstyle();
});


// #########################################################  
socket.on('cc-extra_set', function () {
  $("html").addClass("live");


  let param_live = urlParams.get("live") ? urlParams.get("live") : "oui";
  let nolive = window.location.search.replace(/(\&|\?)live=(oui|non)/g, "")
  let setlivefix = (window.location.search) ? "&live=" + param_live : "?live=" + param_live;

  var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + nolive + setlivefix;
  window.history.pushState({ path: newurl }, '', newurl);

  // window.location.search = urlParams;
  change_bg_extra();
  setTimeout(function () {
    $("#images_container_extra").toggleClass("showme").toggleClass("forced");
  }, 200);

  // alert("extra_set")
});


///////////
socket.on('ls-point', function (id) {
  // alert("cc : " +id)
  back_point_re(id);
})

socket.on("cc_point_exist_ask", function (askurl, action) {

  // alert("from cc = /" +askurl+ " -- "+ action);
  let ask_image = $('#images_container').find(".image[data-fileurl='sets/" + askurl + "']");

  // alert(ask_image.length);
  if (ask_image.length < 1) {
    socket.emit("cc-point_response", askurl, "not");
    // alert("less");
  } else {
    // alert("cc_point_exist_ask = " +action)
    if (action == "backback") {
      back_point_re("back", askurl, "to_go");
      return
    }
    back_point_re(action, askurl, "to_go");
    socket.emit("cc-point_response", askurl, "exists");
  }
});

function back_point_re(id, askurl, to_go) {
  // alert(id)
  // alert("back_point_re == "+ id)

  let cible = (to_go != "") ? $('#images_container').find("[data-fileurl='sets/" + askurl + "']") : $('#images_container').find(".active");
  // let cible = $('#images_container').find("[data-fileurl='sets/" + askurl + "']");

  // alert(to_go)

  if (id == "point") {

    /// ADD RE (save last point.ed)
    let a = $('#images_container').find(".active")
    let p = $('#images_container').find(".point")
    if (p.length > 0) {
      // if (p != a) {
        // CL
        $(p).prevAll().removeClass("ret")
        $(p).nextAll().removeClass("ret")  
        $(p).addClass("ret").removeClass("point");
      // } else {
        // $(a).addClass("re");
        // CL
        // $(a).prevAll().removeClass("re")
        // $(a).nextAll().removeClass("re")  
      // }
      // alert("p")
    }

    /// NEW CIBLE --> new point.ed
    $(cible).addClass("point");
    // CL
    $(cible).prevAll().removeClass("point")
    $(cible).nextAll().removeClass("point")



  } else if (id == "rereturn") {

      let r = $('#images_container').find(".ret")
      let l = r.length

      if (l > 0) {
        // alert("rre")
        $(r).addClass("active");
        $(r).attr()
        $(r).prevAll().removeClass("active");
        $(r).nextAll().removeClass("active");
        // alert("r")
      }


  } else if (id == "back") { // GO

    // let a = $('#images_container').find(".active")
    let p = $('#images_container').find(".point")
    let l = p.length

    if (l > 0) {
      $(p).addClass("active")
      $(p).nextAll().removeClass("active");
      $(p).prevAll().removeClass("active");
    }



  }

  console.warn($(".active").length
)

}


// #########################################################
// #########################################################
// #########################################################
function change_style(type) { // ### revert
  if (type == "reset") {
    $("html").removeClass("rouge blue vert gray revert multi");
  } else {
    // $("html").removeClass("rouge blue vert gray revert");
    $("html").toggleClass(type);
  }
}
// #########################################################

function newstyle() { // ### revert
    $("html").toggleClass("newstyle");
}
// #########################################################

function demo_load_others(reload_end_scenario, message) {
  // TODO GET FIRST FOLDER NAME
  const list_first_path_name = $('body').attr("data-first_path");
  $('body').attr("data-current_fld", list_first_path_name);
  $('body').data("current_num")

  if ($("body").is(".reload_from_zero")) { // NOT first time
    console.log("NOT reload_from_zero : all is loaded ! keep Running !!!")
    alerte_me("NOT reload_from_zero : all is loaded ! keep Running !!!", 1000, "black")

  } else if (reload_end_scenario == "reload_in_loop") {

    console.log(reload_end_scenario + " --R0-- message " + message)
    alerte_me("reload_in_loop", 1000, "black")

    // FONCTION play from start
    $('#images_container').find(".image").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active")

  } else {
    // MOINS2     // reload_from_zero First <- body is NOT .reload_from_zero

    let current_num = $('body').attr("data-current_num")
    let current_num_next = Number(current_num) + 1;
    $('body').attr("data-current_num", current_num_next); // TODO CHECK
    // alert("pause 3")

    // load_files_cc("reload_from_zero")
    console.log(reload_end_scenario + " --R1-- message " + message)
    alerte_me("reload_from_zero", 1000, "black")

  }
}
// #########################################################
// #########################################################
// #########################################################


// alert(param_rand_fix)
// alert(is_demo_rand)
// alert(is_demo)


function load_files_cc(state, params) { // DEFAULT

  // alert(is_demo_rand)
  // alert(params)
  if (is_demo == "yes") {
    // S T A R T  _____ L O A D   FILES    H E R E
    if (params) {

      console.warn("*** params *** \n" + params)
      socket.emit("cc-get_folder_files", params, "params", show_uploads); //EMIT

      // alert("params");
      $('body').attr("data-current_num", "rand").addClass("is_demo_rand");

    } else if (!params && (!$('body').hasClass("is_demo_rand")) ) {

      let start_num = (is_demo_rand=="oui") ? "rand" : -1;
      // alert(start_num)
      /// REDO --- qfix RAND PARAM
      socket.emit("cc-get_folder_files", "first_shoot", start_num, show_uploads); //EMIT
      $('body').addClass("is_demo_rand")
      // alert("no params");

    } else if ((is_demo_rand == "oui") && (!$('body').hasClass("is_demo_rand"))) {

      console.warn("randmee")
      socket.emit("cc-get_folder_files", "first_shoot", "rand", show_uploads); //EMIT

      $('body').attr("data-current_num", "rand").addClass("is_demo_rand");
      // data-current_num <= will be fixed by server sending via
      // cc-demorand
      console.warn("\n\Rand first\n\n")

    } else if (state == "reload_from_zero") {      // LOAD Folders not loaded (demo start)

      console.warn("fshoot")
      // FIX RAND RE_LOAD 01 ON END BUT ALREADY LOADED (on first by start_rand : cf. cc-demorand)
      let startrand = $('body').attr("data-start_rand") ? $('body').attr("data-start_rand") : "not_rand";
      console.warn("startrand == " + startrand)
      if ((startrand == "0") || (startrand == "no needed")) {
        // alert(param_rand)
        socket.emit("cc-get_folder_files", "first_shoot", -1, show_uploads); //EMIT
      } else {
        // alert(param_rand)
        socket.emit("cc-get_folder_files", "first_shoot", -1, show_uploads); //EMIT
      }
      // MOINS2
      $('body').attr("data-current_num", "-1").addClass("reload_from_zero");
      console.warn("\n\nreload_from_zero _0 \n\n")


    } else {                              // DEMO (not) ( default )

      socket.emit("cc-get_folder_files", "", Number(is_demo_start) - 1, show_uploads); //EMIT
      $('body').attr("data-current_num", Number(is_demo_start) - 1);
      // alert("p1")
      if (cover_plz == "yes") { // COVER
        $("#images_container").prepend("<div class='image' data-fileurl='" + fileurl_cover_default + "' style=\"background-image: url(\'" + fileurl_cover_default + "\');\" alt=" + data_filename_cover + " onerror='onerror_function(" + fileurl_cover_default + ")'></div>");
        $("#images_container").css("background", "none")
      }
    }

  } // is_demo
} // folder_files

// #########################################################
// ||
// ||
// \/
// #########################################################

// S - cc-get_folder_files STARTUP 
socket.on('cc-folder_files', function (get_folder_files, folder_name, folder_num, dirname_plz) { // GET_FILES
  
  // alert(folder_name+","+ folder_num+","+ dirname_plz)
  let folder_name_exist = $("#images_container").find(".image[data-folder='" + folder_name + "']").length
  // alert("OK LOAD -- folder_name_exist ?= "+folder_name_exist)
  folder_num = Number(folder_num);
  $('body').attr("data-current_num", folder_num);

  // alert("pause 2")

  // alert(folder_num);

  // if (folder_name_exist==0) { // IF Folder NOT already loaded... loading
  if (folder_name_exist < 1) { // IF Folder NOT already loaded... loading
    // alert("DEBUG == ALREADY LOADED")

    get_folder_files.forEach(function (filename) {
      let fileurl = filename.replace(dirname_plz + "/client/", "");
      // alert("fileurl f = "+fileurl)
      let set_is = "sets" // <<---- /sets/...
      let regex = "/^(.+)" + set_is + "\//gm"
      let data_filename = fileurl.replace(regex, "");
      $("#images_container").append("<div class='image' data-folder='" + folder_name + "' data-fld_num='" + folder_num + "' data-fileurl='" + fileurl + "' style=\"background-image: url(\'" + fileurl + "\'); animation-duration: " + original_speed + "ms;\" alt=" + data_filename + " onerror='onerror_function(" + fileurl + ")'></div>");
    })

    setTimeout(function () {
      $('body').attr("data-current_fld", folder_name);
      let current_num = $('body').data("current_num")
      let current_num_fix = Number(current_num)
      console.warn("current_num_fix = " + current_num_fix + " --- folder_name = " + folder_name)

      if (current_num_fix == 0) { // FIRST

        // alert("LOOOOD current_num_fix==0")

        $("#images_container").find(".image").first().addClass("active");
        if (cover_plz == "yes") { // COVER
          // ?t="+new Date().getTime()+"
          $("#images_container").find(".image").first().after("<div class='image' data-folder='" + folder_name + "' data-fld_num='" + folder_num + "' data-fileurl='" + fileurl_cover_default + "' style=\"background-image: url(\'" + fileurl_cover_default + "\'); animation-duration: " + original_speed + "ms;\" alt=" + data_filename_cover + " onerror='onerror_function(" + fileurl_cover_default + ")'></div>");
        }

      } else if ((is_demo_start != "no") && ($('body').attr("data-first_shoot") != "1")) {  // NUMBER / DEMO

        // alert("LOOOOD DEFAULT")

        $('body').attr("data-first_shoot", "1");
        $("#images_container").find(".image").first().addClass("active");

      } else {

        // alert("fail")

        // $('body').attr("data-first_shoot", "1");
        $('body').data("current_num", -1)
        // socket.emit("cc-get_folder_files", "", -1, show_uploads);
        change_bg("next");
        $("#images_container").find(".active").last().after("<div class='image' data-folder='" + folder_name + "' data-fld_num='" + folder_num + "' data-fileurl='" + fileurl_cover_default + "' style=\"background-image: url(\'" + fileurl_cover_default + "\'); animation-duration: " + original_speed + "ms;\" alt=" + data_filename_cover + " onerror='onerror_function(" + fileurl_cover_default + ")'></div>");
        $('#images_container').find(".image").last(".active").prevAll(".image").removeClass("ultra").removeClass("active");
        change_bg("next");

      }

      // $('body').attr("data-current_num", current_num_fix);

    }, 600);

  } else {                 // IF Folder already loaded... NOT loading -> play from start

    console.warn("\n\nSTOP LOADING -- .image[data-folder='" + folder_name + "'] exists\n\n")
    // FONCTION play from start
    $('#images_container').find(".image[data-folder='" + folder_name + "']").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active")
    $("html").removeClass("reseting");

    // alert(folder_name)

    let current_num = ($('body').attr("data-current_num")) ? $('body').attr("data-current_num") : -1;
    // let current_num_next = (folder_name == "_up") ? Number(current_num) + 1 : Number(current_num);
    // $("body").addClass("ALREADY_LOADED");
    let current_num_next = Number(current_num);
    let alredaycount = $("body").attr("data-already");


    if (folder_name == "_up") {
      $(".image[data-folder='_up']").first().addClass("active");
      // return;
    }
    if (!alredaycount || alredaycount == "undefined") {
      $("body").attr("data-already", 1);
      socket.emit("cc-get_folder_files", "", current_num_next, show_uploads);
      $('body').data("current_num", -1);

    } else {
      // alert("b")
      let alredaycount_next = Number(alredaycount) + 1;
      $("body").attr("data-already", alredaycount_next);
    }
    if (Number($("body").attr("data-already")) > 2) {
      // alert("already 2")
      // socket.emit("cc-get_folder_files", "", current_num_next, show_uploads);
      $('#images_container').find(".image").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active")
    }

    // change_bg("next");
    // alert(alredaycount);
    // console.warn("toomuch == "+current_num_next+" > "+howmuch_number)
  }
}) // cc_folder_files


// #########################################################
/// FIX To get first RAND num from serveur
// in load_files_cc()
socket.on('cc-demorand', function (num, status) {
  console.log("{{ cc-demorand ( " + status + " ) == " + num)
  if (status == "howmuch_list") {
    $('body').attr("data-howmuch_number", num);
  } else if (status == "isup_number") {
    $('body').attr("data-isup_number", num);
  } else if (status == "list_first_path_name") {
    let num_fix = num.replace(/(.*)\/sets\//g, "");
    $('body').attr("data-first_path", num_fix);
    $('body').attr("data-current_fld", num_fix);
    // alert(num)
  } else {
    let num_rand = num;
    $('body').attr("data-start_rand", num_rand).attr("data-current_num", num_rand).addClass("is_demo_rand");
  }
});

// #########################################################
// PARAM = first  / go
let params = new URLSearchParams(document.location.search) ? new URLSearchParams(document.location.search) : "first";
// alert(params)
if (params == "first") {
  load_files_cc("first") // C load_files_cc() ==> S cc-get_folder_files
} else {
  // const param_go = urlParams.get("go") ? urlParams.get("go") : "nogo";
  // const param_go_fix=param_loop.toString().replace("loop=","")

  let param_go = params.get("go") ? params.get("go") : "nogo";
  // alert(param_go)
  switch (param_go) {
    case "nogo":
      load_files_cc("first") // C load_files_cc() ==> S cc-get_folder_files
      break;
    default:
      // let params_fix=params.toString().replace("go=","")
      load_files_cc("first", param_go_fix) // C load_files_cc() ==> S cc-get_folder_files
  } //SWITCH
}








// #########################################################
// #########################################################

socket.on('cc-loadall_pre_cleanup', function (go_folder, how, set_folder) {
  // alert(how)
  if (((".image.cc-subfolder.from_ls.buttall[data-folder='" + go_folder + "']").length >= 0) ||
    ((".image.cc-subfolder.from_ls.buttall[data-folder='/" + set_folder + "/" + go_folder + "']").length >= 0)) {

    console.warn("ls-loadall => cc-loadall_pre_cleanup == PRE RM Already Loaded")
    $("#images_container").find(".image.cc-subfolder.from_ls.buttall[data-folder='" + go_folder + "']").remove()
    $("#images_container").find(".image.cc-subfolder.from_ls.buttall[data-folder='" + set_folder + "/" + go_folder + "']").remove()

  }
});

// #########################################################
// USE by ls-loadall ("old" == not standardized)
socket.on('cc-subfolder', function (datatype, data, how) {
  // alert("h1 = "+how)

  // how 1: default_next 2: at_the_end
  localStorage.getItem('original_speed', max_speed);
  let datafix = (how == "default_next_next") ? data.toString().split(',').reverse() : data.toString().split(',');
  datafix.forEach(cleanup_filenames);

  function cleanup_filenames(item, index, arr, howis) {
    // alert("h2 = "+how)
    // alert("item = "+item+" \nindex -- "+index+"\n arr(==file_list) --\nhowis -- "+howis)
    let set_is = param_set_fix;
    let data_tofix = arr[index];
    let regex = "/^(.+)" + set_is + "\//g"
    let datafix = data_tofix.toString().replace(regex, "");
    let data_filename = datafix.replace(/^(.+)\/([^\/]+)$/, "$2");
    let data_folder = datafix.replace(/^(.+)\/([^\/]+)$/, "$1");
    // $("body").attr("data-set_is",set_is);
    /// CLASS (de sous dossier)
    // let not_regex_fold_name=set_is
    // let fileurl ="/"+set_is+"/"+data_folder+"/"+data_filename;
    let fileurl = item;
    // alert(fileurl)
    // alert("fileurl d = "+fileurl)
    /// ##TD Fix_getset_speed

    let div_image_element = "<div class='image cc-subfolder from_ls buttall' \
                              data-folder='"+ data_folder + "' \
                              data-fileurl='"+ fileurl + "' \
                              style=\"background-image: url(\'"+ fileurl + "?t=" + new Date().getTime() + "\'); animation-duration: " + original_speed + "ms;\" \
                              alt="+ data_filename + " onerror='onerror_function(" + fileurl + ")'></div>"
    // alert("00")
    if ($('#images_container').find(".active").length < 1) {
      // alert("0")
      $("#images_container").append(div_image_element);
      $('#images_container').find(".image").first().addClass("active");
      // scenario(change_img_interval);

    } else if (how == "default_next_next") {
      // alert("h3 = "+how)
      $("#images_container").find(".active").last().after().after().after(div_image_element);

    } else if (how == "at_the_end") // IF ONE or more .active already exists
    {
      // alert("2")
      $("#images_container").append(div_image_element);
    } else // IF ONE or more .active already exists
    {
      // alert("1")
      $("#images_container").find(".active").last().after(div_image_element);
    }
  }
});

















// const vid = document.querySelector('#images_container');
// const pipBtn = document.querySelector('#pip-btn');
// pipBtn.addEventListener('click', () => {
//   vid.requestPictureInPicture();
// });





// #########################################################
// #########################################################
// ######################################################### GLOBAL FUNCITONS
// #########################################################
// #########################################################

// ###################################################
function alerte_me(message, time, color) {
  $("html").addClass("alerte");
  $("#num").html(message).css("background", color);
  if (time != 0) {
    setTimeout(function () {
      $("html").removeClass("alerte");
      $("#num").css("background", "#000");
    }, time);
  }
}
function alerte_pan(message) {
  $("#alerte_pan").html(message);
}




function pause_mutliple() {
  if ((!$('html').is(".pause_me")) && (!$('html').is(".forcepause"))) {
    $("html").addClass("forcepause")
    pause_me(pause_duration, "", "forced");
  } else if ($("body").hasClass("tapause")) {
    $("html").removeClass("forcepause")
    $('body').removeClass("tapause");    // PAUSE totale
  } else if ($("html").hasClass("forcepause")) {
    // $("html").removeClass("forcepause")
    $('body').addClass("tapause");    // PAUSE totale
    // alerte_me("p stop -- ", 1000, "red");
  }
}
$("#overlay").attr("onclick", "pause_mutliple()");

// #########################################################
function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}


// #########################################################
// HASH UNIQUE ID
String.prototype.hashCode = function () {
  if (Array.prototype.reduce) {
    return this.split("").reduce(function (a, b) {
      a = (a << 5) - a + b.charCodeAt(0);
      return a & a;
    }, 0);
  }
  var hash = 0;
  if (this.length === 0) return hash;
  for (var i = 0; i < this.length; i++) {
    var character = this.charCodeAt(i);
    hash = (hash << 5) - hash + character;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
};

// #########################################################
/// IS IFRAME ?
function inIframe() {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}
if (!inIframe()) {
  window.name = "cchouquette";
}




// #########################################################
function rand_me() {
  $('#images_container>.image').tsort({ order: 'rand' }, { attr: 'data-fileurl' });
  // FONCTION play from start
  $('#images_container').find(".image").first().addClass("active").nextAll(".image").removeClass("ultra").removeClass("active");
  // $("#images_container").find(".image").first().nextAll(".image").removeClass("ultra").removeClass("active");

}

// #########################################################
function kill_me(what) {
  if (what == "next") {
    $('#images_container').find(".active").last().next().remove();
    // alert(0)
    console.warn("Kille_me == " + what)
  } else if (what == "prev") {
    $('#images_container').find(".active").last().prev().remove();
    // alert(1)
    console.warn("Kille_me == " + what)
  }
};
// #########################################################
function restart(what, num_is) { //TODO is working ??
  let num = (param_rand=="oui") ? "rand" : num_is;
  $("html").addClass("reseting");
  console.warn("restart === " + what + " --- " + num)
  // alert(("restart === "+what+" --- "+num))
  // MOINS2
  var current_fld = $('body').attr("data-current_fld");
  socket.emit("cc-get_folder_files", current_fld, num, show_uploads, what); //EMIT

  $('body').attr("data-current_num", "-1").addClass("reload_from_zero"); // TODO CHECK
  console.warn("\n\nLS __ Restart -- reload_from_zero\n\n")
};
// #########################################################


function help_toggle(time) {
  if ($('body').hasClass("no_help")) {
    setTimeout(function () {
      $('body').removeClass("no_help");
      $('body').addClass("showsides");
    }, 10);
  } else {
    setTimeout(function () {
      $('body').addClass("no_help");
      $('body').removeClass("showsides");
    }, 10);
  }
  setTimeout(function () {
    $('body').removeClass("showsides");
    $('body').addClass("no_help");
  }, time);
};

// #########################################################
function plusvite(option) {

  var original_speed = localStorage.getItem('original_speed');
  var new_speed = Number(original_speed) - Number(number_ms);

  if ((new_speed < min_speed) || (option == "nolimit")) {

    if (new_speed < 0 || new_speed == 0) {
      console.log("\nNew_Speed +\nCANT BE LESS of 0 !");
      alerte_me("new_speed min == 0 !", 1000, "red");
      localStorage.setItem('original_speed', 10);
    } else {

      console.log("\nNew_Speed +\n" + new_speed + "\n");
      alerte_me(new_speed, 1000, "black");
      localStorage.setItem('original_speed', new_speed);
    }

  } else {
    
    console.log("\nTooMuch_Slow (< " + min_speed + ")\n" + max_speed + "\n");
    alerte_me("!!!! " + max_speed, 1000, "red");
    var new_speed = max_speed;
    localStorage.setItem('original_speed', max_speed);
  }

  ///// SYSTEM TEMPON -- on vérifie si une newspeednew vitesse a été demandée
  let newspeed = $("body").attr("new_speed")

  /// UNE newspeed a été demandée, traitement en cours, on stocke la nouvelle : new_speed_new
  if ( !(newspeed === undefined) ) {

    $("body").attr("new_speed_new", new_speed)

  } else {

    // Settimeout check
    let witeime = 500;

    /// LOCK
    $("body").attr("new_speed", new_speed)

    //// EXEC newspeed
    localStorage.setItem('original_speed', new_speed);
    $(".image").each(function () {
      $(this).css("animation-duration", new_speed + "ms").addClass("manual_speed");
    })

    /// PUIS on attend (tant que new_speed existe dans Body)
    setTimeout(function() {
      let newspeed = $("body").attr("new_speed")
      let newspeednew = $("body").attr("new_speed_new")

      /// LOCK Max speed limite
      if (newspeednew <= min_speed) {
        
        /// SEULEMENT au bout du temps imparti, on modifie l'animeCSS
        /// SI les deux valeurs sont diff on choisi newspeednew
        if (newspeed != newspeednew) {
          localStorage.setItem('original_speed', newspeednew);
          $(".image").each(function () {
            $(this).css("animation-duration", newspeednew + "ms").addClass("manual_speed");
          })
        }

      } else {
        console.log("plusvite -- trop vite")
      }

      /// UNLOCK
      $("body").removeAttr("new_speed")
      /// EMIT 
      socket.emit("new_speed_socket", newspeednew); //EMIT

    }, witeime); 

  };

}








function moinsvite(option) {
  var original_speed = localStorage.getItem('original_speed');
  var new_speed = Number(original_speed) + Number(number_ms);

  if (new_speed < min_speed) {
    console.log("\nNew_Speed +\n" + new_speed + "\n");
    alerte_me(new_speed, 1000, "black");
    localStorage.setItem('original_speed', new_speed);
  } else {
    console.log("\nTooMuch_Slow (< " + min_speed + ")\n" + min_speed + "\n");
    alerte_me("!!!! " + new_speed, 1000, "red");
    var new_speed = min_speed;
    localStorage.setItem('original_speed', min_speed);
  }

  ///// SYSTEM TEMPON -- on vérifie si une newspeednew vitesse a été demandée
  let newspeed = $("body").attr("new_speed")

  /// UNE newspeed a été demandée, traitement en cours, on stocke la nouvelle : new_speed_new
  if ( !(newspeed === undefined) ) {

    $("body").attr("new_speed_new", new_speed)

  } else {

    // Settimeout check
    let witeime = 500;

    /// LOCK
    $("body").attr("new_speed", new_speed)

    //// EXEC newspeed
    localStorage.setItem('original_speed', new_speed);
    $(".image").each(function () {
      $(this).css("animation-duration", new_speed + "ms").addClass("manual_speed");
    })

    /// PUIS on attend (tant que new_speed existe dans Body)
    setTimeout(function() {
      let newspeed = $("body").attr("new_speed")
      let newspeednew = $("body").attr("new_speed_new")

      /// LOCK Max speed limite
      if (newspeednew >= min_speed) {
        
        /// SEULEMENT au bout du temps imparti, on modifie l'animeCSS
        /// SI les deux valeurs sont diff on choisi newspeednew
        if (newspeed != newspeednew) {
          localStorage.setItem('original_speed', newspeednew);
          $(".image").each(function () {
            $(this).css("animation-duration", newspeednew + "ms").addClass("manual_speed");
          })
        }

      } else {
        console.log("plusvite -- trop vite")
      }

      /// UNLOCK
      $("body").removeAttr("new_speed")
      /// EMIT 
      socket.emit("new_speed_socket", newspeednew); //EMIT

    }, witeime); 

  };

}



// #########################################################
// #########################################################
// ######################################################### KEYPRESS
// #########################################################
// #########################################################

$(document).keypress(function (e) {
  // ######################################################### F
  if (e.key == "f") {
    // alert("KEY F")
    if (!document.fullscreenElement) {
      document.documentElement.requestFullscreen();
      $('html').addClass("fullscreen");
    }
    if (document.fullscreenElement) {
      if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
        $('html').addClass("fullscreen");
        $("#wrap_container").attr("onclick", "none()");
      } else {
        document.exitFullscreen();
        $('html').removeClass("fullscreen");
        $("#wrap_container").attr("onclick", "fullscreen()");
      }
    }
  }


  // ######################################################### R
  if (e.key == "r") {
    rand_me();
  }




  // ######################################################### T
  if (e.key == "t") {
    kill_me();
  }


  // ######################################################### H
  if (e.key == "h") {
    help_toggle();
  }

  // ######################################################### B
  // if (e.key == "b") { 
  //   $("body").toggleClass("blur");
  // }

  // ######################################################### G
  if (e.key == "g") {
    change_style("gray"); // # gray
  }
  if (e.key == "n") {
    change_style("revert"); // # noir
  }
  if (e.key == "b") {
    change_style("blue"); // # bleu
  }
  if (e.key == "v") {
    change_style("vert"); // # vert
  }
  if (e.key == "c") {
    change_style("rouge"); // # vert
  }
  if (e.key == "m") {
    change_style("multi"); // # vert
  }

  // ######################################################### U
  if (e.key == "u") {
    // requestPictureInPicture();
    if ($('body').is(".ultra")) {
      alerte_me("RMV _ultra", 3000, "black");
    } else {
      alerte_me("ADD _ultra", 3000, "black");
    }
    $("body").toggleClass("ultra");
  }

  // ######################################################### P
  if ((e.key == "p") || (e.key == " ")) {
    alerte_me("p", 1000, "red");
    e.preventDefault()
    pause_mutliple()
  }

  // ######################################################### K
  if (e.key == "k") { // +vite
    plusvite();
  }
  // ######################################################### O
  if (e.key == "o") { //-vite
    moinsvite()
  }
}); // DOC keypress




// ######################################################### 
// ######################################################### 
// ######################################################### Left / Right - Top / Bottom arrows
document.onkeydown = function (event) {
  switch (event.keyCode) {
    case 37:
      change_bg("prev", "force_change"); // direction
      // alert("gauche")
      break;
    case 38:
      // alert('Up key pressed');
      moinsvite()
      break;
    case 39:
      // alert('Right key pressed'); // arrow
      change_bg("next", "force_change"); // direction
      break;
    case 40:
      // alert('Down key pressed');
      plusvite()
      break;
  }
};

// #########################################################
// #########################################################
// #########################################################

